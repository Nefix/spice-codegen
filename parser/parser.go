package parser

import (
	"fmt"
	"io"

	"github.com/alecthomas/participle"
	"github.com/alecthomas/participle/lexer"
	"github.com/alecthomas/participle/lexer/regex"
	"github.com/iancoleman/strcase"
)

type Spice struct {
	Entries []*Entry `@@*`
}

type Entry struct {
	Enum       *Enum       `@@`
	Flags      *Flags      `| @@`
	Struct     *Struct     `| @@ ";"`
	Definition *Definition `| @@`
	Channel    *Channel    `| @@`
	Protocol   *Protocol   `| @@`
	Message    *Message    `| @@`
	Type       *Type       `| @@`
	Variable   *Variable   `| @@`
}

type Attribute struct {
	Name   string   `"@" @Ident`
	Values []string `( "(" @Ident ")" )*`
}

type Type struct {
	Name       string       `"typedef" @Ident`
	Type       string       `@Ident`
	Attributes []*Attribute `( @@ )* ";"`
}

type ArraySize struct {
	Int        *int            `"[" (@Int`
	ImageSize  *ArrayImageSize `| @@`
	Identifier string          `| @Ident )? "]"`
	// TODO: Implement bytes size
	// TODO: Implement cstring
}

type ArrayImageSize struct {
	Size   int    `"image_size" "(" @Int`
	Width  string `"," @Ident`
	Height string `"," @Ident ")"`
}

type Variable struct {
	Type       string       `@Ident`
	Pointer    bool         `@( "*" )?`
	Name       string       `@Ident`
	Size       *ArraySize   `( @@ )?`
	Attributes []*Attribute `( @@ )* ";"`
}

type Definition struct {
	Name  string `"define" @Ident`
	Type  string `@Ident`
	Value int    `"=" @Int ";"`
}

type Struct struct {
	Name       string       `"struct" @Ident`
	Fields     []*Field     `"{" ( @@ )* "}"`
	Attributes []*Attribute `( @@ )*`
}

type Field struct {
	Switch     *Switch   `@@`
	Struct     *Struct   `| @@`
	StructName string    `@Ident ";"`
	Variable   *Variable `| @@`
}

type Enum struct {
	Type       string          `( @"enum8" | @"enum16" | @"enum32" )!`
	Name       string          `@Ident`
	Items      []*EnumFlagItem `"{" ( @@ )* "}"`
	Attributes []*Attribute    `( @@ )* ";"`
}

type Flags struct {
	Type       string          `( @"flags8" | @"flags16" | @"flags32" )`
	Name       string          `@Ident`
	Items      []*EnumFlagItem `"{" ( @@ )* "}"`
	Attributes []*Attribute    `( @@ )* ";"`
}

type EnumFlagItem struct {
	Name       string       `@( Int | Ident | "_" )*`
	Value      int          `( "=" @Int )?`
	Attributes []*Attribute `( @@ )* ( "," )?`
}

type Message struct {
	Name       string       `"message" @Ident`
	Fields     []*Field     `"{" ( @@ )* "}"`
	Attributes []*Attribute `( @@ )* ";"`
}

type Switch struct {
	Switch     string         `"switch" "(" @Ident ")" "{"`
	Cases      []*SwitchCase  `( @@ )*`
	Default    *SwitchDefault `( @@ )?`
	Name       string         `"}" @Ident`
	Attributes []*Attribute   `( @@ )* ";"`
}

type SwitchCase struct {
	False     string      `Case (@"!")?`
	Case      string      `@Ident ":"`
	Variables []*Variable `( @@ )*`
}

type SwitchDefault struct {
	Variables []*Variable `Default ":" ( @@ )+`
}

type Channel struct {
	Name       string         `"channel" @Ident`
	Parent     string         `( ":" @Ident )? "{"`
	Sides      []*ChannelSide `( @@ )*`
	Attributes []*Attribute   `"}" ( @@ )* ";"`
}

type ChannelSide struct {
	Side     string            `@( "server" | "client" ) ":"`
	Messages []*ChannelMessage `( @@ )*`
}

type ChannelMessage struct {
	Fields     []*Field     `( "message" "{" ( @@ )* "}"`
	Type       string       `| @Ident )`
	Attributes []*Attribute `( @@ )*`
	Name       string       `@Ident`
	Value      int          `( "=" @Int )? ";"`
}

type Protocol struct {
	Name     string             `"protocol" @Ident`
	Channels []*ProtocolChannel `"{" ( @@ )* "}" ";"`
}

type ProtocolChannel struct {
	Type string `@Ident`
	Name string `@Ident`
	ID   int    `( "=" @Int )? ";"`
}

func Parse(r io.Reader) (*Spice, error) {
	l := lexer.Must(regex.New(`
		SinglelineComment = \/\/[^\n\r]*(\n\r)?
		MultilineComment = (?s)/\*.*?\*/

		Case = case
		Default = default

		Ident = [[:alpha:]][[:alpha:]|[:digit:]|_|\.]*
		String = "[^"]*"
		Int = [[:digit:]]+

		Punct = [[:punct:]]
		Whitespace = \s+
	`))

	parser := participle.MustBuild(&Spice{}, participle.Lexer(l), participle.Elide("SinglelineComment", "MultilineComment", "Whitespace"))

	spice := &Spice{}
	if err := parser.Parse(r, spice); err != nil {
		return nil, fmt.Errorf("parse: %w", err)
	}

	return spice, nil
}

func (f *Field) Type() string {
	if f.Switch != nil {
		return "switch"
	}

	if f.Struct != nil {
		return "struct"
	}

	if f.Variable != nil {
		return "variable"
	}

	panic("unknown field type")
}

func (f *Field) Name() string {
	var s string
	switch f.Type() {
	case "switch":
		s = f.Switch.Name

	case "struct":
		if f.StructName != "" {
			s = f.StructName
			break
		}

		s = f.Struct.Name

	case "variable":
		s = f.Variable.Name

	default:
		panic("unknown field name")
	}

	return strcase.ToCamel(s)
}

func (f *Field) SetName(n string) {
	switch f.Type() {
	case "switch":
		f.Switch.Name = n

	case "struct":
		if f.StructName != "" {
			f.StructName = n
			break
		}

		f.Struct.Name = n

	case "variable":
		f.Variable.Name = n

	default:
		panic("unknown field name")
	}

}
