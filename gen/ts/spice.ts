// @ts-ignore
export class Fixed284 {
  value: number

  static messageSize = 4

  constructor(value: number) {
    this.value = value
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): Fixed284 {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const value = view.getInt32(offset, true)
    offset += 4
    return new Fixed284(value)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(Fixed284.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setInt32(offset, this.value, true)
    offset += 4

    return buf
  }
}
export class StreamIdT {
  value: number

  static messageSize = 4

  constructor(value: number) {
    this.value = value
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): StreamIdT {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const value = view.getUint32(offset, true)
    offset += 4
    return new StreamIdT(value)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(StreamIdT.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.value, true)
    offset += 4

    return buf
  }
}
export class Point {
  x: number
  y: number

  static messageSize = 8

  constructor(x: number, y: number) {
    this.x = x
    this.y = y
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): Point {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const x = view.getInt32(offset, true)
    offset += 4
    const y = view.getInt32(offset, true)
    offset += 4
    return new Point(x, y)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(Point.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setInt32(offset, this.x, true)
    offset += 4
    view.setInt32(offset, this.y, true)
    offset += 4

    return buf
  }
}
export class Point16 {
  x: number
  y: number

  static messageSize = 4

  constructor(x: number, y: number) {
    this.x = x
    this.y = y
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): Point16 {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const x = view.getInt16(offset, true)
    offset += 2
    const y = view.getInt16(offset, true)
    offset += 2
    return new Point16(x, y)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(Point16.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setInt16(offset, this.x, true)
    offset += 2
    view.setInt16(offset, this.y, true)
    offset += 2

    return buf
  }
}
export class PointFix {
  x: Fixed284
  y: Fixed284

  static messageSize = 8

  constructor(x: Fixed284, y: Fixed284) {
    this.x = x
    this.y = y
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): PointFix {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const x = Fixed284.unmarshal(buf, offset)
    offset += Fixed284.messageSize
    const y = Fixed284.unmarshal(buf, offset)
    offset += Fixed284.messageSize
    return new PointFix(x, y)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(PointFix.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    slice.set(new Uint8Array(this.x.marshal()), offset)
    offset += Fixed284.messageSize
    slice.set(new Uint8Array(this.y.marshal()), offset)
    offset += Fixed284.messageSize

    return buf
  }
}
export class Rect {
  top: number
  left: number
  bottom: number
  right: number

  static messageSize = 16

  constructor(top: number, left: number, bottom: number, right: number) {
    this.top = top
    this.left = left
    this.bottom = bottom
    this.right = right
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): Rect {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const top = view.getInt32(offset, true)
    offset += 4
    const left = view.getInt32(offset, true)
    offset += 4
    const bottom = view.getInt32(offset, true)
    offset += 4
    const right = view.getInt32(offset, true)
    offset += 4
    return new Rect(top, left, bottom, right)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(Rect.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setInt32(offset, this.top, true)
    offset += 4
    view.setInt32(offset, this.left, true)
    offset += 4
    view.setInt32(offset, this.bottom, true)
    offset += 4
    view.setInt32(offset, this.right, true)
    offset += 4

    return buf
  }
}
export class Transform {
  t00: number
  t01: number
  t02: number
  t10: number
  t11: number
  t12: number

  static messageSize = 24

  constructor(t00: number, t01: number, t02: number, t10: number, t11: number, t12: number) {
    this.t00 = t00
    this.t01 = t01
    this.t02 = t02
    this.t10 = t10
    this.t11 = t11
    this.t12 = t12
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): Transform {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const t00 = view.getUint32(offset, true)
    offset += 4
    const t01 = view.getUint32(offset, true)
    offset += 4
    const t02 = view.getUint32(offset, true)
    offset += 4
    const t10 = view.getUint32(offset, true)
    offset += 4
    const t11 = view.getUint32(offset, true)
    offset += 4
    const t12 = view.getUint32(offset, true)
    offset += 4
    return new Transform(t00, t01, t02, t10, t11, t12)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(Transform.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.t00, true)
    offset += 4
    view.setUint32(offset, this.t01, true)
    offset += 4
    view.setUint32(offset, this.t02, true)
    offset += 4
    view.setUint32(offset, this.t10, true)
    offset += 4
    view.setUint32(offset, this.t11, true)
    offset += 4
    view.setUint32(offset, this.t12, true)
    offset += 4

    return buf
  }
}
export enum LinkErr {
  LinkErrOk = 0,
  LinkErrError = 1,
  LinkErrInvalidMagic = 2,
  LinkErrInvalidData = 3,
  LinkErrVersionMismatch = 4,
  LinkErrNeedSecured = 5,
  LinkErrNeedUnsecured = 6,
  LinkErrPermissionDenied = 7,
  LinkErrBadConnectionId = 8,
  LinkErrChannelNotAvailable = 9,
}
export enum WarnCode {
  WarnCodeWarnGeneral = 0,
}
export enum InfoCode {
  InfoCodeInfoGeneral = 0,
}
export enum MigrateFlags {
  Empty,
  MigrateFlagsNeedFlush = 1 << 0,
  MigrateFlagsNeedDataTransfer = 1 << 1,
}
export enum CompositeFlags {
  Empty,
  CompositeFlagsOp0 = 1 << 0,
  CompositeFlagsOp1 = 1 << 1,
  CompositeFlagsOp2 = 1 << 2,
  CompositeFlagsOp3 = 1 << 3,
  CompositeFlagsOp4 = 1 << 4,
  CompositeFlagsOp5 = 1 << 5,
  CompositeFlagsOp6 = 1 << 6,
  CompositeFlagsOp7 = 1 << 7,
  CompositeFlagsSrcFilter0 = 1 << 8,
  CompositeFlagsSrcFilter1 = 1 << 9,
  CompositeFlagsSrcFilter2 = 1 << 10,
  CompositeFlagsMaskFilter0 = 1 << 11,
  CompositeFlagsMaskFitler1 = 1 << 12,
  CompositeFlagsMaskFilter2 = 1 << 13,
  CompositeFlagsSrcRepeat0 = 1 << 14,
  CompositeFlagsSrcRepeat1 = 1 << 15,
  CompositeFlagsMaskRepeat0 = 1 << 16,
  CompositeFlagsMaskRepeat1 = 1 << 17,
  CompositeFlagsComponentAlpha = 1 << 18,
  CompositeFlagsHasMask = 1 << 19,
  CompositeFlagsHasSrcTransform = 1 << 20,
  CompositeFlagsHasMaskTransform = 1 << 21,
  CompositeFlagsSourceOpaque = 1 << 22,
  CompositeFlagsMaskOpaque = 1 << 23,
  CompositeFlagsDestOpaque = 1 << 24,
}
export enum NotifySeverity {
  NotifySeverityInfo = 0,
  NotifySeverityWarn = 1,
  NotifySeverityError = 2,
}
export enum NotifyVisibility {
  NotifyVisibilityLow = 0,
  NotifyVisibilityMedium = 1,
  NotifyVisibilityHigh = 2,
}
export enum MouseMode {
  Empty,
  MouseModeServer = 1 << 0,
  MouseModeClient = 1 << 1,
}
export class Empty {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): Empty {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new Empty()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(Empty.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export class Data {
  data: number[]

  constructor(data: number[]) {
    this.data = data
  }

  static unmarshal(buf: ArrayBuffer, expectedSize: number, offset?: number): Data {
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    let data: number[] = []
    for (let i = 0; i < expectedSize - offset; i++) {
    const uint8 = view.getUint8(offset)
    offset += 1
    data.push(uint8)
    }
    return new Data(data)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(0) // TODO: This should be the ininite data size
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export enum DataCompressionType {
  DataCompressionTypeNone = 0,
  DataCompressionTypeLz4 = 1,
}
export class EmptyStructure {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): EmptyStructure {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new EmptyStructure()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(EmptyStructure.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export class CompressedData {
  type: DataCompressionType
  empty: EmptyStructure | undefined
  uncompressedSize: number | undefined
  compressedData: ArrayBuffer | undefined

  get messageSize() {
    let size = 0
    size += 1
    switch (this.type) {
      case DataCompressionType.DataCompressionTypeNone:
        size += 0
        break
      default:
        size += 4
        break
    }
    return size
  }

  constructor(type: DataCompressionType, empty: EmptyStructure | undefined, uncompressedSize: number | undefined, compressedData: ArrayBuffer | undefined) {
    this.type = type
    this.empty = empty
    this.uncompressedSize = uncompressedSize
    this.compressedData = compressedData
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): CompressedData {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const type = view.getUint8(offset)
    offset += 1
    let empty
    let uncompressedSize
    switch (type) {
      case DataCompressionType.DataCompressionTypeNone:
    const empty = EmptyStructure.unmarshal(buf, offset)
    offset += EmptyStructure.messageSize
        break
      default:
    uncompressedSize = view.getUint32(offset, true)
    offset += 4
        break
    }
    const compressedData = buf.slice(origOffset)
    return new CompressedData(type, empty, uncompressedSize, compressedData)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint8(offset, this.type)
    offset += 1
    switch (this.type) {
      case DataCompressionType.DataCompressionTypeNone:
    slice.set(new Uint8Array(this.empty!.marshal()), offset)
    offset += EmptyStructure.messageSize
        break
      default:
    view.setUint32(offset, this.uncompressedSize!, true)
    offset += 4
        break
    }

    return buf
  }
}
export class ChannelWait {
  channelType: number
  channelId: number
  messageSerial: bigint

  static messageSize = 10

  constructor(channelType: number, channelId: number, messageSerial: bigint) {
    this.channelType = channelType
    this.channelId = channelId
    this.messageSerial = messageSerial
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): ChannelWait {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const channelType = view.getUint8(offset)
    offset += 1
    const channelId = view.getUint8(offset)
    offset += 1
    const messageSerial = view.getBigUint64(offset, true)
    offset += 8
    return new ChannelWait(channelType, channelId, messageSerial)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(ChannelWait.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint8(offset, this.channelType)
    offset += 1
    view.setUint8(offset, this.channelId)
    offset += 1
    view.setBigUint64(offset, this.messageSerial, true)
    offset += 8

    return buf
  }
}
export class MsgMigrate {
  flags: MigrateFlags

  static messageSize = 4

  constructor(flags: MigrateFlags) {
    this.flags = flags
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgMigrate {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const flags = view.getUint32(offset, true)
    offset += 4
    return new MsgMigrate(flags)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgMigrate.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.flags, true)
    offset += 4

    return buf
  }
}
export class MsgMigrateData {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgMigrateData {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new MsgMigrateData()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgMigrateData.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export class MsgSetAck {
  generation: number
  window: number

  static messageSize = 8

  constructor(generation: number, window: number) {
    this.generation = generation
    this.window = window
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgSetAck {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const generation = view.getUint32(offset, true)
    offset += 4
    const window = view.getUint32(offset, true)
    offset += 4
    return new MsgSetAck(generation, window)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgSetAck.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.generation, true)
    offset += 4
    view.setUint32(offset, this.window, true)
    offset += 4

    return buf
  }
}
export class MsgPing {
  id: number
  timestamp: bigint
  data: ArrayBuffer | undefined

  get messageSize() {
    let size = 0
    size += 4
    size += 8
    return size
  }

  constructor(id: number, timestamp: bigint, data: ArrayBuffer | undefined) {
    this.id = id
    this.timestamp = timestamp
    this.data = data
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): MsgPing {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const id = view.getUint32(offset, true)
    offset += 4
    const timestamp = view.getBigUint64(offset, true)
    offset += 8
    const data = buf.slice(origOffset)
    return new MsgPing(id, timestamp, data)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.id, true)
    offset += 4
    view.setBigUint64(offset, this.timestamp, true)
    offset += 8

    return buf
  }
}
export class MsgWaitForChannels {
  waitCount: number
  waitList: ChannelWait[]

  get messageSize() {
    let size = 0
    size += 1
    size += 10 * this.waitCount
    return size
  }

  constructor(waitCount: number, waitList: ChannelWait[]) {
    this.waitCount = waitCount
    this.waitList = waitList
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): MsgWaitForChannels {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const waitCount = view.getUint8(offset)
    offset += 1
    let waitList: ChannelWait[] = []
    for (let i = 0; i < waitCount; i++) {
    const channelWait = ChannelWait.unmarshal(buf, offset)
    offset += ChannelWait.messageSize
    waitList.push(channelWait)
    }
    return new MsgWaitForChannels(waitCount, waitList)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint8(offset, this.waitCount)
    offset += 1
    this.waitList.forEach((channelWait) => {
    slice.set(new Uint8Array(channelWait.marshal()), offset)
    offset += ChannelWait.messageSize
    })

    return buf
  }
}
export class MsgDisconnecting {
  timeStamp: bigint
  reason: LinkErr

  static messageSize = 12

  constructor(timeStamp: bigint, reason: LinkErr) {
    this.timeStamp = timeStamp
    this.reason = reason
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgDisconnecting {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const timeStamp = view.getBigUint64(offset, true)
    offset += 8
    const reason = view.getUint32(offset, true)
    offset += 4
    return new MsgDisconnecting(timeStamp, reason)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgDisconnecting.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setBigUint64(offset, this.timeStamp, true)
    offset += 8
    view.setUint32(offset, this.reason, true)
    offset += 4

    return buf
  }
}
export class MsgNotify {
  timeStamp: bigint
  severity: NotifySeverity
  visibilty: NotifyVisibility
  what: number
  messageLen: number
  message: number[]

  get messageSize() {
    let size = 0
    size += 8
    size += 4
    size += 4
    size += 4
    size += 4
    size += 1 * this.messageLen
    return size
  }

  constructor(timeStamp: bigint, severity: NotifySeverity, visibilty: NotifyVisibility, what: number, messageLen: number, message: number[]) {
    this.timeStamp = timeStamp
    this.severity = severity
    this.visibilty = visibilty
    this.what = what
    this.messageLen = messageLen
    this.message = message
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): MsgNotify {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const timeStamp = view.getBigUint64(offset, true)
    offset += 8
    const severity = view.getUint32(offset, true)
    offset += 4
    const visibilty = view.getUint32(offset, true)
    offset += 4
    const what = view.getUint32(offset, true)
    offset += 4
    const messageLen = view.getUint32(offset, true)
    offset += 4
    let message: number[] = []
    for (let i = 0; i < messageLen; i++) {
    const uint8 = view.getUint8(offset)
    offset += 1
    message.push(uint8)
    }
    return new MsgNotify(timeStamp, severity, visibilty, what, messageLen, message)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setBigUint64(offset, this.timeStamp, true)
    offset += 8
    view.setUint32(offset, this.severity, true)
    offset += 4
    view.setUint32(offset, this.visibilty, true)
    offset += 4
    view.setUint32(offset, this.what, true)
    offset += 4
    view.setUint32(offset, this.messageLen, true)
    offset += 4
    this.message.forEach((uint8) => {
    view.setUint8(offset, uint8)
    offset += 1
    })

    return buf
  }
}
export class MsgList {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgList {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new MsgList()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgList.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export class MsgBaseLast {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgBaseLast {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new MsgBaseLast()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgBaseLast.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export enum Msg {
  MsgMigrate = 1,
  MsgMigrateData = 2,
  MsgSetAck = 3,
  MsgPing = 4,
  MsgWaitForChannels = 5,
  MsgDisconnecting = 6,
  MsgNotify = 7,
  MsgList = 8,
  MsgBaseLast = 100,
}
export class MsgcAckSync {
  generation: number

  static messageSize = 4

  constructor(generation: number) {
    this.generation = generation
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgcAckSync {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const generation = view.getUint32(offset, true)
    offset += 4
    return new MsgcAckSync(generation)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgcAckSync.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.generation, true)
    offset += 4

    return buf
  }
}
export class MsgcAck {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgcAck {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new MsgcAck()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgcAck.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export class MsgcPong {
  id: number
  timestamp: bigint

  static messageSize = 12

  constructor(id: number, timestamp: bigint) {
    this.id = id
    this.timestamp = timestamp
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgcPong {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const id = view.getUint32(offset, true)
    offset += 4
    const timestamp = view.getBigUint64(offset, true)
    offset += 8
    return new MsgcPong(id, timestamp)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgcPong.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.id, true)
    offset += 4
    view.setBigUint64(offset, this.timestamp, true)
    offset += 8

    return buf
  }
}
export class MsgcMigrateFlushMark {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgcMigrateFlushMark {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new MsgcMigrateFlushMark()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgcMigrateFlushMark.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export class MsgcMigrateData {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgcMigrateData {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new MsgcMigrateData()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgcMigrateData.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export class MsgcDisconnecting {
  timeStamp: bigint
  reason: LinkErr

  static messageSize = 12

  constructor(timeStamp: bigint, reason: LinkErr) {
    this.timeStamp = timeStamp
    this.reason = reason
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgcDisconnecting {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const timeStamp = view.getBigUint64(offset, true)
    offset += 8
    const reason = view.getUint32(offset, true)
    offset += 4
    return new MsgcDisconnecting(timeStamp, reason)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgcDisconnecting.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setBigUint64(offset, this.timeStamp, true)
    offset += 8
    view.setUint32(offset, this.reason, true)
    offset += 4

    return buf
  }
}
export enum Msgc {
  MsgcAckSync = 1,
  MsgcAck = 2,
  MsgcPong = 3,
  MsgcMigrateFlushMark = 4,
  MsgcMigrateData = 5,
  MsgcDisconnecting = 6,
}
export class ChannelId {
  type: number
  id: number

  static messageSize = 2

  constructor(type: number, id: number) {
    this.type = type
    this.id = id
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): ChannelId {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const type = view.getUint8(offset)
    offset += 1
    const id = view.getUint8(offset)
    offset += 1
    return new ChannelId(type, id)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(ChannelId.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint8(offset, this.type)
    offset += 1
    view.setUint8(offset, this.id)
    offset += 1

    return buf
  }
}
export class DstInfo {
  port: number
  sport: number
  hostSize: number
  hostData: number[] | undefined
  certSubjectSize: number
  certSubjectData: number[] | undefined

  static messageSize = 20

  constructor(port: number, sport: number, hostSize: number, hostData: number[] | undefined, certSubjectSize: number, certSubjectData: number[] | undefined) {
    this.port = port
    this.sport = sport
    this.hostSize = hostSize
    this.hostData = hostData
    this.certSubjectSize = certSubjectSize
    this.certSubjectData = certSubjectData
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): DstInfo {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const port = view.getUint16(offset, true)
    offset += 2
    const sport = view.getUint16(offset, true)
    offset += 2
    const hostSize = view.getUint32(offset, true)
    offset += 4
    const hostDataPtr = view.getUint32(offset, true)
    offset += 4
    let hostData: number | undefined = undefined
    if (hostDataPtr !== 0) {
        hostData = number.unmarshal(buf, Number(hostDataPtr))
    }
    const certSubjectSize = view.getUint32(offset, true)
    offset += 4
    const certSubjectDataPtr = view.getUint32(offset, true)
    offset += 4
    let certSubjectData: number | undefined = undefined
    if (certSubjectDataPtr !== 0) {
        certSubjectData = number.unmarshal(buf, Number(certSubjectDataPtr))
    }
    return new DstInfo(port, sport, hostSize, hostData, certSubjectSize, certSubjectData)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(DstInfo.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint16(offset, this.port, true)
    offset += 2
    view.setUint16(offset, this.sport, true)
    offset += 2
    view.setUint32(offset, this.hostSize, true)
    offset += 4
    view.setUint32(offset, 0, true) // TODO: This!
    offset += 8
    view.setUint32(offset, this.certSubjectSize, true)
    offset += 4
    view.setUint32(offset, 0, true) // TODO: This!
    offset += 8

    return buf
  }
}
export class MsgMainMigrateBegin {
  dstInfo: DstInfo

  static messageSize = 20

  constructor(dstInfo: DstInfo) {
    this.dstInfo = dstInfo
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgMainMigrateBegin {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const dstInfo = DstInfo.unmarshal(buf, offset)
    offset += DstInfo.messageSize
    return new MsgMainMigrateBegin(dstInfo)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgMainMigrateBegin.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    slice.set(new Uint8Array(this.dstInfo.marshal()), offset)
    offset += DstInfo.messageSize

    return buf
  }
}
export class MsgMainMigrateCancel {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgMainMigrateCancel {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new MsgMainMigrateCancel()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgMainMigrateCancel.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export class MsgMainInit {
  sessionId: number
  displayChannelsHint: number
  supportedMouseModes: number
  currentMouseMode: number
  agentConnected: number
  agentTokens: number
  multiMediaTime: number
  ramHint: number

  static messageSize = 32

  constructor(sessionId: number, displayChannelsHint: number, supportedMouseModes: number, currentMouseMode: number, agentConnected: number, agentTokens: number, multiMediaTime: number, ramHint: number) {
    this.sessionId = sessionId
    this.displayChannelsHint = displayChannelsHint
    this.supportedMouseModes = supportedMouseModes
    this.currentMouseMode = currentMouseMode
    this.agentConnected = agentConnected
    this.agentTokens = agentTokens
    this.multiMediaTime = multiMediaTime
    this.ramHint = ramHint
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgMainInit {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const sessionId = view.getUint32(offset, true)
    offset += 4
    const displayChannelsHint = view.getUint32(offset, true)
    offset += 4
    const supportedMouseModes = view.getUint32(offset, true)
    offset += 4
    const currentMouseMode = view.getUint32(offset, true)
    offset += 4
    const agentConnected = view.getUint32(offset, true)
    offset += 4
    const agentTokens = view.getUint32(offset, true)
    offset += 4
    const multiMediaTime = view.getUint32(offset, true)
    offset += 4
    const ramHint = view.getUint32(offset, true)
    offset += 4
    return new MsgMainInit(sessionId, displayChannelsHint, supportedMouseModes, currentMouseMode, agentConnected, agentTokens, multiMediaTime, ramHint)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgMainInit.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.sessionId, true)
    offset += 4
    view.setUint32(offset, this.displayChannelsHint, true)
    offset += 4
    view.setUint32(offset, this.supportedMouseModes, true)
    offset += 4
    view.setUint32(offset, this.currentMouseMode, true)
    offset += 4
    view.setUint32(offset, this.agentConnected, true)
    offset += 4
    view.setUint32(offset, this.agentTokens, true)
    offset += 4
    view.setUint32(offset, this.multiMediaTime, true)
    offset += 4
    view.setUint32(offset, this.ramHint, true)
    offset += 4

    return buf
  }
}
export class MsgMainChannelsList {
  numOfChannels: number
  channels: ChannelId[]

  get messageSize() {
    let size = 0
    size += 4
    size += 2 * this.numOfChannels
    return size
  }

  constructor(numOfChannels: number, channels: ChannelId[]) {
    this.numOfChannels = numOfChannels
    this.channels = channels
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): MsgMainChannelsList {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const numOfChannels = view.getUint32(offset, true)
    offset += 4
    let channels: ChannelId[] = []
    for (let i = 0; i < numOfChannels; i++) {
    const channelId = ChannelId.unmarshal(buf, offset)
    offset += ChannelId.messageSize
    channels.push(channelId)
    }
    return new MsgMainChannelsList(numOfChannels, channels)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.numOfChannels, true)
    offset += 4
    this.channels.forEach((channelId) => {
    slice.set(new Uint8Array(channelId.marshal()), offset)
    offset += ChannelId.messageSize
    })

    return buf
  }
}
export class MsgMainMouseMode {
  supportedModes: MouseMode
  currentMode: MouseMode

  static messageSize = 4

  constructor(supportedModes: MouseMode, currentMode: MouseMode) {
    this.supportedModes = supportedModes
    this.currentMode = currentMode
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgMainMouseMode {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const supportedModes = view.getUint16(offset, true)
    offset += 2
    const currentMode = view.getUint16(offset, true)
    offset += 2
    return new MsgMainMouseMode(supportedModes, currentMode)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgMainMouseMode.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint16(offset, this.supportedModes, true)
    offset += 2
    view.setUint16(offset, this.currentMode, true)
    offset += 2

    return buf
  }
}
export class MsgMainMultiMediaTime {
  time: number

  static messageSize = 4

  constructor(time: number) {
    this.time = time
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgMainMultiMediaTime {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const time = view.getUint32(offset, true)
    offset += 4
    return new MsgMainMultiMediaTime(time)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgMainMultiMediaTime.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.time, true)
    offset += 4

    return buf
  }
}
export class MsgMainAgentConnected {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgMainAgentConnected {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new MsgMainAgentConnected()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgMainAgentConnected.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export class MsgMainAgentDisconnected {
  errorCode: LinkErr

  static messageSize = 4

  constructor(errorCode: LinkErr) {
    this.errorCode = errorCode
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgMainAgentDisconnected {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const errorCode = view.getUint32(offset, true)
    offset += 4
    return new MsgMainAgentDisconnected(errorCode)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgMainAgentDisconnected.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.errorCode, true)
    offset += 4

    return buf
  }
}
export class MsgMainAgentData {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgMainAgentData {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new MsgMainAgentData()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgMainAgentData.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export class MsgMainAgentToken {
  numTokens: number

  static messageSize = 4

  constructor(numTokens: number) {
    this.numTokens = numTokens
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgMainAgentToken {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const numTokens = view.getUint32(offset, true)
    offset += 4
    return new MsgMainAgentToken(numTokens)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgMainAgentToken.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.numTokens, true)
    offset += 4

    return buf
  }
}
export class MsgMainMigrateSwitchHost {
  port: number
  sport: number
  hostSize: number
  hostData: number[] | undefined
  certSubjectSize: number
  certSubjectData: number[] | undefined

  static messageSize = 20

  constructor(port: number, sport: number, hostSize: number, hostData: number[] | undefined, certSubjectSize: number, certSubjectData: number[] | undefined) {
    this.port = port
    this.sport = sport
    this.hostSize = hostSize
    this.hostData = hostData
    this.certSubjectSize = certSubjectSize
    this.certSubjectData = certSubjectData
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgMainMigrateSwitchHost {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const port = view.getUint16(offset, true)
    offset += 2
    const sport = view.getUint16(offset, true)
    offset += 2
    const hostSize = view.getUint32(offset, true)
    offset += 4
    const hostDataPtr = view.getUint32(offset, true)
    offset += 4
    let hostData: number | undefined = undefined
    if (hostDataPtr !== 0) {
        hostData = number.unmarshal(buf, Number(hostDataPtr))
    }
    const certSubjectSize = view.getUint32(offset, true)
    offset += 4
    const certSubjectDataPtr = view.getUint32(offset, true)
    offset += 4
    let certSubjectData: number | undefined = undefined
    if (certSubjectDataPtr !== 0) {
        certSubjectData = number.unmarshal(buf, Number(certSubjectDataPtr))
    }
    return new MsgMainMigrateSwitchHost(port, sport, hostSize, hostData, certSubjectSize, certSubjectData)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgMainMigrateSwitchHost.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint16(offset, this.port, true)
    offset += 2
    view.setUint16(offset, this.sport, true)
    offset += 2
    view.setUint32(offset, this.hostSize, true)
    offset += 4
    view.setUint32(offset, 0, true) // TODO: This!
    offset += 8
    view.setUint32(offset, this.certSubjectSize, true)
    offset += 4
    view.setUint32(offset, 0, true) // TODO: This!
    offset += 8

    return buf
  }
}
export class MsgMainMigrateEnd {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgMainMigrateEnd {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new MsgMainMigrateEnd()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgMainMigrateEnd.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export class MsgMainName {
  nameLen: number
  name: number[]

  get messageSize() {
    let size = 0
    size += 4
    size += 1 * this.nameLen
    return size
  }

  constructor(nameLen: number, name: number[]) {
    this.nameLen = nameLen
    this.name = name
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): MsgMainName {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const nameLen = view.getUint32(offset, true)
    offset += 4
    let name: number[] = []
    for (let i = 0; i < nameLen; i++) {
    const uint8 = view.getUint8(offset)
    offset += 1
    name.push(uint8)
    }
    return new MsgMainName(nameLen, name)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.nameLen, true)
    offset += 4
    this.name.forEach((uint8) => {
    view.setUint8(offset, uint8)
    offset += 1
    })

    return buf
  }
}
export class MsgMainUuid {
  uuid: number[]

  static messageSize = 16

  constructor(uuid: number[]) {
    this.uuid = uuid
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgMainUuid {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    let uuid: number[] = []
    for (let i = 0; i < 16; i++) {
    const uint8 = view.getUint8(offset)
    offset += 1
    uuid.push(uint8)
    }
    return new MsgMainUuid(uuid)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgMainUuid.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    this.uuid.forEach((uint8) => {
    view.setUint8(offset, uint8)
    offset += 1
    })

    return buf
  }
}
export class MsgMainAgentConnectedTokens {
  numTokens: number

  static messageSize = 4

  constructor(numTokens: number) {
    this.numTokens = numTokens
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgMainAgentConnectedTokens {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const numTokens = view.getUint32(offset, true)
    offset += 4
    return new MsgMainAgentConnectedTokens(numTokens)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgMainAgentConnectedTokens.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.numTokens, true)
    offset += 4

    return buf
  }
}
export class MsgMainMigrateBeginSeamless {
  dstInfo: DstInfo
  srcMigVersion: number

  static messageSize = 24

  constructor(dstInfo: DstInfo, srcMigVersion: number) {
    this.dstInfo = dstInfo
    this.srcMigVersion = srcMigVersion
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgMainMigrateBeginSeamless {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const dstInfo = DstInfo.unmarshal(buf, offset)
    offset += DstInfo.messageSize
    const srcMigVersion = view.getUint32(offset, true)
    offset += 4
    return new MsgMainMigrateBeginSeamless(dstInfo, srcMigVersion)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgMainMigrateBeginSeamless.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    slice.set(new Uint8Array(this.dstInfo.marshal()), offset)
    offset += DstInfo.messageSize
    view.setUint32(offset, this.srcMigVersion, true)
    offset += 4

    return buf
  }
}
export class MsgMainMigrateDstSeamlessAck {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgMainMigrateDstSeamlessAck {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new MsgMainMigrateDstSeamlessAck()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgMainMigrateDstSeamlessAck.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export class MsgMainMigrateDstSeamlessNack {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgMainMigrateDstSeamlessNack {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new MsgMainMigrateDstSeamlessNack()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgMainMigrateDstSeamlessNack.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export enum MsgMain {
  MsgMainMigrateBegin = 101,
  MsgMainMigrateCancel = 102,
  MsgMainInit = 103,
  MsgMainChannelsList = 104,
  MsgMainMouseMode = 105,
  MsgMainMultiMediaTime = 106,
  MsgMainAgentConnected = 107,
  MsgMainAgentDisconnected = 108,
  MsgMainAgentData = 109,
  MsgMainAgentToken = 110,
  MsgMainMigrateSwitchHost = 111,
  MsgMainMigrateEnd = 112,
  MsgMainName = 113,
  MsgMainUuid = 114,
  MsgMainAgentConnectedTokens = 115,
  MsgMainMigrateBeginSeamless = 116,
  MsgMainMigrateDstSeamlessAck = 117,
  MsgMainMigrateDstSeamlessNack = 118,
}
export class MsgcMainClientInfo {
  cacheSize: bigint

  static messageSize = 8

  constructor(cacheSize: bigint) {
    this.cacheSize = cacheSize
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgcMainClientInfo {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const cacheSize = view.getBigUint64(offset, true)
    offset += 8
    return new MsgcMainClientInfo(cacheSize)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgcMainClientInfo.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setBigUint64(offset, this.cacheSize, true)
    offset += 8

    return buf
  }
}
export class MsgcMainMigrateConnected {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgcMainMigrateConnected {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new MsgcMainMigrateConnected()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgcMainMigrateConnected.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export class MsgcMainMigrateConnectError {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgcMainMigrateConnectError {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new MsgcMainMigrateConnectError()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgcMainMigrateConnectError.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export class MsgcMainAttachChannels {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgcMainAttachChannels {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new MsgcMainAttachChannels()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgcMainAttachChannels.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export class MsgcMainMouseModeRequest {
  mode: MouseMode

  static messageSize = 2

  constructor(mode: MouseMode) {
    this.mode = mode
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgcMainMouseModeRequest {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const mode = view.getUint16(offset, true)
    offset += 2
    return new MsgcMainMouseModeRequest(mode)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgcMainMouseModeRequest.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint16(offset, this.mode, true)
    offset += 2

    return buf
  }
}
export class MsgcMainAgentStart {
  numTokens: number

  static messageSize = 4

  constructor(numTokens: number) {
    this.numTokens = numTokens
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgcMainAgentStart {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const numTokens = view.getUint32(offset, true)
    offset += 4
    return new MsgcMainAgentStart(numTokens)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgcMainAgentStart.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.numTokens, true)
    offset += 4

    return buf
  }
}
export class MsgcMainAgentData {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgcMainAgentData {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new MsgcMainAgentData()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgcMainAgentData.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export class MsgcMainAgentToken {
  numTokens: number

  static messageSize = 4

  constructor(numTokens: number) {
    this.numTokens = numTokens
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgcMainAgentToken {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const numTokens = view.getUint32(offset, true)
    offset += 4
    return new MsgcMainAgentToken(numTokens)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgcMainAgentToken.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.numTokens, true)
    offset += 4

    return buf
  }
}
export class MsgcMainMigrateEnd {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgcMainMigrateEnd {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new MsgcMainMigrateEnd()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgcMainMigrateEnd.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export class MsgcMainMigrateDstDoSeamless {
  srcVersion: number

  static messageSize = 4

  constructor(srcVersion: number) {
    this.srcVersion = srcVersion
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgcMainMigrateDstDoSeamless {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const srcVersion = view.getUint32(offset, true)
    offset += 4
    return new MsgcMainMigrateDstDoSeamless(srcVersion)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgcMainMigrateDstDoSeamless.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.srcVersion, true)
    offset += 4

    return buf
  }
}
export class MsgcMainMigrateConnectedSeamless {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgcMainMigrateConnectedSeamless {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new MsgcMainMigrateConnectedSeamless()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgcMainMigrateConnectedSeamless.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export class MsgcMainQualityIndicator {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgcMainQualityIndicator {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new MsgcMainQualityIndicator()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgcMainQualityIndicator.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export enum MsgcMain {
  MsgcMainClientInfo = 101,
  MsgcMainMigrateConnected = 102,
  MsgcMainMigrateConnectError = 103,
  MsgcMainAttachChannels = 104,
  MsgcMainMouseModeRequest = 105,
  MsgcMainAgentStart = 106,
  MsgcMainAgentData = 107,
  MsgcMainAgentToken = 108,
  MsgcMainMigrateEnd = 109,
  MsgcMainMigrateDstDoSeamless = 110,
  MsgcMainMigrateConnectedSeamless = 111,
  MsgcMainQualityIndicator = 112,
}
export enum ClipType {
  ClipTypeNone = 0,
  ClipTypeRects = 1,
}
export enum PathFlags {
  Empty,
  PathFlagsBegin = 1 << 0,
  PathFlagsEnd = 1 << 1,
  PathFlagsClose = 1 << 3,
  PathFlagsBezier = 1 << 4,
}
export enum VideoCodecType {
  VideoCodecTypeMjpeg = 1,
  VideoCodecTypeVp8 = 2,
  VideoCodecTypeH264 = 3,
  VideoCodecTypeVp9 = 4,
  VideoCodecTypeH265 = 5,
}
export enum StreamFlags {
  Empty,
  StreamFlagsTopDown = 1 << 0,
}
export enum BrushType {
  BrushTypeNone = 0,
  BrushTypeSolid = 1,
  BrushTypePattern = 2,
}
export enum MaskFlags {
  Empty,
  MaskFlagsInvers = 1 << 0,
}
export enum ImageType {
  ImageTypeBitmap = 0,
  ImageTypeQuic = 1,
  ImageTypeReserved = 2,
  ImageTypeLzPlt = 100,
  ImageTypeLzRgb = 101,
  ImageTypeGlzRgb = 102,
  ImageTypeFromCache = 103,
  ImageTypeSurface = 104,
  ImageTypeJpeg = 105,
  ImageTypeFromCacheLossless = 106,
  ImageTypeZlibGlzRgb = 107,
  ImageTypeJpegAlpha = 108,
  ImageTypeLz4 = 109,
}
export enum ImageCompression {
  ImageCompressionInvalid = 0,
  ImageCompressionOff = 1,
  ImageCompressionAutoGlz = 2,
  ImageCompressionAutoLz = 3,
  ImageCompressionQuic = 4,
  ImageCompressionGlz = 5,
  ImageCompressionLz = 6,
  ImageCompressionLz4 = 7,
}
export enum ImageFlags {
  Empty,
  ImageFlagsCacheMe = 1 << 0,
  ImageFlagsHighBitsSet = 1 << 1,
  ImageFlagsCacheReplaceMe = 1 << 2,
}
export enum BitmapFmt {
  BitmapFmtInvalid = 0,
  BitmapFmt1BitLe = 1,
  BitmapFmt1BitBe = 2,
  BitmapFmt4BitLe = 3,
  BitmapFmt4BitBe = 4,
  BitmapFmt8Bit = 5,
  BitmapFmt16Bit = 6,
  BitmapFmt24Bit = 7,
  BitmapFmt32Bit = 8,
  BitmapFmtRgba = 9,
  BitmapFmt8BitA = 10,
}
export enum BitmapFlags {
  Empty,
  BitmapFlagsPalCacheMe = 1 << 0,
  BitmapFlagsPalFromCache = 1 << 1,
  BitmapFlagsTopDown = 1 << 2,
}
export enum JpegAlphaFlags {
  Empty,
  JpegAlphaFlagsTopDown = 1 << 0,
}
export enum ImageScaleMode {
  ImageScaleModeInterpolate = 0,
  ImageScaleModeNearest = 1,
}
export enum Ropd {
  Empty,
  RopdInversSrc = 1 << 0,
  RopdInversBrush = 1 << 1,
  RopdInversDest = 1 << 2,
  RopdOpPut = 1 << 3,
  RopdOpOr = 1 << 4,
  RopdOpAnd = 1 << 5,
  RopdOpXor = 1 << 6,
  RopdOpBlackness = 1 << 7,
  RopdOpWhiteness = 1 << 8,
  RopdOpInvers = 1 << 9,
  RopdInversRes = 1 << 10,
}
export enum LineFlags {
  Empty,
  LineFlagsStyled = 1 << 3,
  LineFlagsStartWithGap = 1 << 2,
}
export enum StringFlags {
  Empty,
  StringFlagsRasterA1 = 1 << 0,
  StringFlagsRasterA4 = 1 << 1,
  StringFlagsRasterA8 = 1 << 2,
  StringFlagsRasterTopDown = 1 << 3,
}
export enum SurfaceFlags {
  Empty,
  SurfaceFlagsPrimary = 1 << 0,
  SurfaceFlagsStreamingMode = 1 << 1,
}
export enum SurfaceFmt {
  SurfaceFmtInvalid = 0,
  SurfaceFmt1A = 1,
  SurfaceFmt8A = 8,
  SurfaceFmt16555 = 16,
  SurfaceFmt16565 = 80,
  SurfaceFmt32Xrgb = 32,
  SurfaceFmt32Argb = 96,
}
export enum AlphaFlags {
  Empty,
  AlphaFlagsDestHasAlpha = 1 << 0,
  AlphaFlagsSrcSurfaceHasAlpha = 1 << 1,
}
export enum ResourceType {
  ResourceTypeInvalid = 0,
  ResourceTypePixmap = 1,
}
export class ClipRects {
  numRects: number
  rects: Rect[]

  get messageSize() {
    let size = 0
    size += 4
    size += 16 * this.numRects
    return size
  }

  constructor(numRects: number, rects: Rect[]) {
    this.numRects = numRects
    this.rects = rects
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): ClipRects {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const numRects = view.getUint32(offset, true)
    offset += 4
    let rects: Rect[] = []
    for (let i = 0; i < numRects; i++) {
    const rect = Rect.unmarshal(buf, offset)
    offset += Rect.messageSize
    rects.push(rect)
    }
    return new ClipRects(numRects, rects)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.numRects, true)
    offset += 4
    this.rects.forEach((rect) => {
    slice.set(new Uint8Array(rect.marshal()), offset)
    offset += Rect.messageSize
    })

    return buf
  }
}
export class PathSegment {
  flags: PathFlags
  count: number
  points: PointFix[]

  get messageSize() {
    let size = 0
    size += 1
    size += 4
    size += 8 * this.count
    return size
  }

  constructor(flags: PathFlags, count: number, points: PointFix[]) {
    this.flags = flags
    this.count = count
    this.points = points
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): PathSegment {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const flags = view.getUint8(offset)
    offset += 1
    const count = view.getUint32(offset, true)
    offset += 4
    let points: PointFix[] = []
    for (let i = 0; i < count; i++) {
    const pointFix = PointFix.unmarshal(buf, offset)
    offset += PointFix.messageSize
    points.push(pointFix)
    }
    return new PathSegment(flags, count, points)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint8(offset, this.flags)
    offset += 1
    view.setUint32(offset, this.count, true)
    offset += 4
    this.points.forEach((pointFix) => {
    slice.set(new Uint8Array(pointFix.marshal()), offset)
    offset += PointFix.messageSize
    })

    return buf
  }
}
export class Path {
  numSegments: number
  segments: PathSegment[]

  get messageSize() {
    let size = 0
    size += 4
    size += -1 * this.numSegments
    return size
  }

  constructor(numSegments: number, segments: PathSegment[]) {
    this.numSegments = numSegments
    this.segments = segments
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): Path {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const numSegments = view.getUint32(offset, true)
    offset += 4
    let segments: PathSegment[] = []
    for (let i = 0; i < numSegments; i++) {
    const pathSegment = PathSegment.unmarshal(buf, offset)
    offset += pathSegment.messageSize
    segments.push(pathSegment)
    }
    return new Path(numSegments, segments)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.numSegments, true)
    offset += 4
    this.segments.forEach((pathSegment) => {
    slice.set(new Uint8Array(pathSegment.marshal()), offset)
    offset += pathSegment.messageSize
    })

    return buf
  }
}
export class Clip {
  type: ClipType
  rects: ClipRects | undefined

  get messageSize() {
    let size = 0
    size += 1
    switch (this.type) {
      case ClipType.ClipTypeRects:
        size += this.rects!.messageSize
        break
    }
    return size
  }

  constructor(type: ClipType, rects: ClipRects | undefined) {
    this.type = type
    this.rects = rects
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): Clip {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const type = view.getUint8(offset)
    offset += 1
    let rects
    switch (type) {
      case ClipType.ClipTypeRects:
    rects = ClipRects.unmarshal(buf, offset)
    offset += rects.messageSize
        break
    }
    return new Clip(type, rects)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint8(offset, this.type)
    offset += 1
    switch (this.type) {
      case ClipType.ClipTypeRects:
    slice.set(new Uint8Array(this.rects!.marshal()), offset)
    offset += this.rects!.messageSize
        break
    }

    return buf
  }
}
export class DisplayBase {
  surfaceId: number
  box: Rect
  clip: Clip

  get messageSize() {
    let size = 0
    size += 4
    size += 16
    size += this.clip.messageSize
    return size
  }

  constructor(surfaceId: number, box: Rect, clip: Clip) {
    this.surfaceId = surfaceId
    this.box = box
    this.clip = clip
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): DisplayBase {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const surfaceId = view.getUint32(offset, true)
    offset += 4
    const box = Rect.unmarshal(buf, offset)
    offset += Rect.messageSize
    const clip = Clip.unmarshal(buf, offset)
    offset += clip.messageSize
    return new DisplayBase(surfaceId, box, clip)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.surfaceId, true)
    offset += 4
    slice.set(new Uint8Array(this.box.marshal()), offset)
    offset += Rect.messageSize
    slice.set(new Uint8Array(this.clip.marshal()), offset)
    offset += this.clip.messageSize

    return buf
  }
}
export class ResourceID {
  type: number
  id: bigint

  static messageSize = 9

  constructor(type: number, id: bigint) {
    this.type = type
    this.id = id
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): ResourceID {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const type = view.getUint8(offset)
    offset += 1
    const id = view.getBigUint64(offset, true)
    offset += 8
    return new ResourceID(type, id)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(ResourceID.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint8(offset, this.type)
    offset += 1
    view.setBigUint64(offset, this.id, true)
    offset += 8

    return buf
  }
}
export class WaitForChannel {
  channelType: number
  channelId: number
  messageSerial: bigint

  static messageSize = 10

  constructor(channelType: number, channelId: number, messageSerial: bigint) {
    this.channelType = channelType
    this.channelId = channelId
    this.messageSerial = messageSerial
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): WaitForChannel {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const channelType = view.getUint8(offset)
    offset += 1
    const channelId = view.getUint8(offset)
    offset += 1
    const messageSerial = view.getBigUint64(offset, true)
    offset += 8
    return new WaitForChannel(channelType, channelId, messageSerial)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(WaitForChannel.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint8(offset, this.channelType)
    offset += 1
    view.setUint8(offset, this.channelId)
    offset += 1
    view.setBigUint64(offset, this.messageSerial, true)
    offset += 8

    return buf
  }
}
export class Palette {
  unique: bigint
  numEnts: number
  ents: number[]

  get messageSize() {
    let size = 0
    size += 8
    size += 2
    size += 4 * this.numEnts
    return size
  }

  constructor(unique: bigint, numEnts: number, ents: number[]) {
    this.unique = unique
    this.numEnts = numEnts
    this.ents = ents
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): Palette {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const unique = view.getBigUint64(offset, true)
    offset += 8
    const numEnts = view.getUint16(offset, true)
    offset += 2
    let ents: number[] = []
    for (let i = 0; i < numEnts; i++) {
    const uint32 = view.getUint32(offset, true)
    offset += 4
    ents.push(uint32)
    }
    return new Palette(unique, numEnts, ents)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setBigUint64(offset, this.unique, true)
    offset += 8
    view.setUint16(offset, this.numEnts, true)
    offset += 2
    this.ents.forEach((uint32) => {
    view.setUint32(offset, uint32, true)
    offset += 4
    })

    return buf
  }
}
export class BitmapData {
  format: BitmapFmt
  flags: BitmapFlags
  x: number
  y: number
  stride: number
  paletteId: bigint | undefined
  palette: Palette | undefined
  data: ArrayBuffer | undefined

  get messageSize() {
    let size = 0
    size += 1
    size += 1
    size += 4
    size += 4
    size += 4
    switch (this.flags) {
      case BitmapFlags.BitmapFlagsPalFromCache:
        size += 8
        break
      default:
        size += 4
        break
    }
    return size
  }

  constructor(format: BitmapFmt, flags: BitmapFlags, x: number, y: number, stride: number, paletteId: bigint | undefined, palette: Palette | undefined, data: ArrayBuffer | undefined) {
    this.format = format
    this.flags = flags
    this.x = x
    this.y = y
    this.stride = stride
    this.paletteId = paletteId
    this.palette = palette
    this.data = data
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): BitmapData {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const format = view.getUint8(offset)
    offset += 1
    const flags = view.getUint8(offset)
    offset += 1
    const x = view.getUint32(offset, true)
    offset += 4
    const y = view.getUint32(offset, true)
    offset += 4
    const stride = view.getUint32(offset, true)
    offset += 4
    let paletteId
    let palette
    switch (flags) {
      case BitmapFlags.BitmapFlagsPalFromCache:
    paletteId = view.getBigUint64(offset, true)
    offset += 8
        break
      default:
    const palettePtr = view.getUint32(offset, true)
    offset += 4
    let palette: Palette | undefined = undefined
    if (palettePtr !== 0) {
        palette = Palette.unmarshal(buf, Number(palettePtr))
    }
        break
    }
    const data = buf.slice(offset)
    return new BitmapData(format, flags, x, y, stride, paletteId, palette, data)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint8(offset, this.format)
    offset += 1
    view.setUint8(offset, this.flags)
    offset += 1
    view.setUint32(offset, this.x, true)
    offset += 4
    view.setUint32(offset, this.y, true)
    offset += 4
    view.setUint32(offset, this.stride, true)
    offset += 4
    switch (this.flags) {
      case BitmapFlags.BitmapFlagsPalFromCache:
    view.setBigUint64(offset, this.paletteId!, true)
    offset += 8
        break
      default:
    view.setUint32(offset, 0, true) // TODO: This!
    offset += 8
        break
    }

    return buf
  }
}
export class BinaryData {
  dataSize: number
  data: ArrayBuffer | undefined

  get messageSize() {
    let size = 0
    size += 4
    return size
  }

  constructor(dataSize: number, data: ArrayBuffer | undefined) {
    this.dataSize = dataSize
    this.data = data
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): BinaryData {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const dataSize = view.getUint32(offset, true)
    offset += 4
    const data = buf.slice(offset)
    return new BinaryData(dataSize, data)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.dataSize, true)
    offset += 4

    return buf
  }
}
export class LZPLTData {
  flags: BitmapFlags
  dataSize: number
  paletteId: bigint | undefined
  palette: Palette | undefined
  data: ArrayBuffer | undefined

  get messageSize() {
    let size = 0
    size += 1
    size += 4
    switch (this.flags) {
      case BitmapFlags.BitmapFlagsPalFromCache:
        size += 8
        break
      default:
        size += 4
        break
    }
    return size
  }

  constructor(flags: BitmapFlags, dataSize: number, paletteId: bigint | undefined, palette: Palette | undefined, data: ArrayBuffer | undefined) {
    this.flags = flags
    this.dataSize = dataSize
    this.paletteId = paletteId
    this.palette = palette
    this.data = data
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): LZPLTData {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const flags = view.getUint8(offset)
    offset += 1
    const dataSize = view.getUint32(offset, true)
    offset += 4
    let paletteId
    let palette
    switch (flags) {
      case BitmapFlags.BitmapFlagsPalFromCache:
    paletteId = view.getBigUint64(offset, true)
    offset += 8
        break
      default:
    const palettePtr = view.getUint32(offset, true)
    offset += 4
    let palette: Palette | undefined = undefined
    if (palettePtr !== 0) {
        palette = Palette.unmarshal(buf, Number(palettePtr))
    }
        break
    }
    const data = buf.slice(offset)
    return new LZPLTData(flags, dataSize, paletteId, palette, data)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint8(offset, this.flags)
    offset += 1
    view.setUint32(offset, this.dataSize, true)
    offset += 4
    switch (this.flags) {
      case BitmapFlags.BitmapFlagsPalFromCache:
    view.setBigUint64(offset, this.paletteId!, true)
    offset += 8
        break
      default:
    view.setUint32(offset, 0, true) // TODO: This!
    offset += 8
        break
    }

    return buf
  }
}
export class ZlibGlzRGBData {
  glzDataSize: number
  dataSize: number
  data: ArrayBuffer | undefined

  get messageSize() {
    let size = 0
    size += 4
    size += 4
    return size
  }

  constructor(glzDataSize: number, dataSize: number, data: ArrayBuffer | undefined) {
    this.glzDataSize = glzDataSize
    this.dataSize = dataSize
    this.data = data
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): ZlibGlzRGBData {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const glzDataSize = view.getUint32(offset, true)
    offset += 4
    const dataSize = view.getUint32(offset, true)
    offset += 4
    const data = buf.slice(offset)
    return new ZlibGlzRGBData(glzDataSize, dataSize, data)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.glzDataSize, true)
    offset += 4
    view.setUint32(offset, this.dataSize, true)
    offset += 4

    return buf
  }
}
export class JPEGAlphaData {
  flags: JpegAlphaFlags
  jpegSize: number
  dataSize: number
  data: ArrayBuffer | undefined

  get messageSize() {
    let size = 0
    size += 1
    size += 4
    size += 4
    return size
  }

  constructor(flags: JpegAlphaFlags, jpegSize: number, dataSize: number, data: ArrayBuffer | undefined) {
    this.flags = flags
    this.jpegSize = jpegSize
    this.dataSize = dataSize
    this.data = data
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): JPEGAlphaData {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const flags = view.getUint8(offset)
    offset += 1
    const jpegSize = view.getUint32(offset, true)
    offset += 4
    const dataSize = view.getUint32(offset, true)
    offset += 4
    const data = buf.slice(offset)
    return new JPEGAlphaData(flags, jpegSize, dataSize, data)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint8(offset, this.flags)
    offset += 1
    view.setUint32(offset, this.jpegSize, true)
    offset += 4
    view.setUint32(offset, this.dataSize, true)
    offset += 4

    return buf
  }
}
export class Surface {
  surfaceId: number

  static messageSize = 4

  constructor(surfaceId: number) {
    this.surfaceId = surfaceId
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): Surface {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const surfaceId = view.getUint32(offset, true)
    offset += 4
    return new Surface(surfaceId)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(Surface.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.surfaceId, true)
    offset += 4

    return buf
  }
}
export class ImageDescriptor {
  id: bigint
  type: ImageType
  flags: ImageFlags
  width: number
  height: number

  static messageSize = 18

  constructor(id: bigint, type: ImageType, flags: ImageFlags, width: number, height: number) {
    this.id = id
    this.type = type
    this.flags = flags
    this.width = width
    this.height = height
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): ImageDescriptor {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const id = view.getBigUint64(offset, true)
    offset += 8
    const type = view.getUint8(offset)
    offset += 1
    const flags = view.getUint8(offset)
    offset += 1
    const width = view.getUint32(offset, true)
    offset += 4
    const height = view.getUint32(offset, true)
    offset += 4
    return new ImageDescriptor(id, type, flags, width, height)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(ImageDescriptor.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setBigUint64(offset, this.id, true)
    offset += 8
    view.setUint8(offset, this.type)
    offset += 1
    view.setUint8(offset, this.flags)
    offset += 1
    view.setUint32(offset, this.width, true)
    offset += 4
    view.setUint32(offset, this.height, true)
    offset += 4

    return buf
  }
}
export class Image {
  descriptor: ImageDescriptor
  bitmap: BitmapData | undefined
  quic: BinaryData | undefined
  lzRgb: BinaryData | undefined
  jpeg: BinaryData | undefined
  lz4: BinaryData | undefined
  lzPlt: LZPLTData | undefined
  zlibGlz: ZlibGlzRGBData | undefined
  jpegAlpha: JPEGAlphaData | undefined
  surface: Surface | undefined

  get messageSize() {
    let size = 0
    size += 18
    switch (this.descriptor.type) {
      case ImageType.ImageTypeBitmap:
        size += this.bitmap!.messageSize
        break
      case ImageType.ImageTypeQuic:
        size += this.quic!.messageSize
        break
      case ImageType.ImageTypeLzRgb:
      case ImageType.ImageTypeGlzRgb:
        size += this.lzRgb!.messageSize
        break
      case ImageType.ImageTypeJpeg:
        size += this.jpeg!.messageSize
        break
      case ImageType.ImageTypeLz4:
        size += this.lz4!.messageSize
        break
      case ImageType.ImageTypeLzPlt:
        size += this.lzPlt!.messageSize
        break
      case ImageType.ImageTypeZlibGlzRgb:
        size += this.zlibGlz!.messageSize
        break
      case ImageType.ImageTypeJpegAlpha:
        size += this.jpegAlpha!.messageSize
        break
      case ImageType.ImageTypeSurface:
        size += 4
        break
    }
    return size
  }

  constructor(descriptor: ImageDescriptor, bitmap: BitmapData | undefined, quic: BinaryData | undefined, lzRgb: BinaryData | undefined, jpeg: BinaryData | undefined, lz4: BinaryData | undefined, lzPlt: LZPLTData | undefined, zlibGlz: ZlibGlzRGBData | undefined, jpegAlpha: JPEGAlphaData | undefined, surface: Surface | undefined) {
    this.descriptor = descriptor
    this.bitmap = bitmap
    this.quic = quic
    this.lzRgb = lzRgb
    this.jpeg = jpeg
    this.lz4 = lz4
    this.lzPlt = lzPlt
    this.zlibGlz = zlibGlz
    this.jpegAlpha = jpegAlpha
    this.surface = surface
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): Image {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const descriptor = ImageDescriptor.unmarshal(buf, offset)
    offset += ImageDescriptor.messageSize
    let bitmap
    let quic
    let lzRgb
    let jpeg
    let lz4
    let lzPlt
    let zlibGlz
    let jpegAlpha
    let surface
    switch (descriptor.type) {
      case ImageType.ImageTypeBitmap:
    bitmap = BitmapData.unmarshal(buf, offset)
    offset += bitmap.messageSize
        break
      case ImageType.ImageTypeQuic:
    quic = BinaryData.unmarshal(buf, offset)
    offset += quic.messageSize
        break
      case ImageType.ImageTypeLzRgb:
      case ImageType.ImageTypeGlzRgb:
    lzRgb = BinaryData.unmarshal(buf, offset)
    offset += lzRgb.messageSize
        break
      case ImageType.ImageTypeJpeg:
    jpeg = BinaryData.unmarshal(buf, offset)
    offset += jpeg.messageSize
        break
      case ImageType.ImageTypeLz4:
    lz4 = BinaryData.unmarshal(buf, offset)
    offset += lz4.messageSize
        break
      case ImageType.ImageTypeLzPlt:
    lzPlt = LZPLTData.unmarshal(buf, offset)
    offset += lzPlt.messageSize
        break
      case ImageType.ImageTypeZlibGlzRgb:
    zlibGlz = ZlibGlzRGBData.unmarshal(buf, offset)
    offset += zlibGlz.messageSize
        break
      case ImageType.ImageTypeJpegAlpha:
    jpegAlpha = JPEGAlphaData.unmarshal(buf, offset)
    offset += jpegAlpha.messageSize
        break
      case ImageType.ImageTypeSurface:
    const surface = Surface.unmarshal(buf, offset)
    offset += Surface.messageSize
        break
    }
    return new Image(descriptor, bitmap, quic, lzRgb, jpeg, lz4, lzPlt, zlibGlz, jpegAlpha, surface)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    slice.set(new Uint8Array(this.descriptor.marshal()), offset)
    offset += ImageDescriptor.messageSize
    switch (this.descriptor.type) {
      case ImageType.ImageTypeBitmap:
    slice.set(new Uint8Array(this.bitmap!.marshal()), offset)
    offset += this.bitmap!.messageSize
        break
      case ImageType.ImageTypeQuic:
    slice.set(new Uint8Array(this.quic!.marshal()), offset)
    offset += this.quic!.messageSize
        break
      case ImageType.ImageTypeLzRgb:
      case ImageType.ImageTypeGlzRgb:
    slice.set(new Uint8Array(this.lzRgb!.marshal()), offset)
    offset += this.lzRgb!.messageSize
        break
      case ImageType.ImageTypeJpeg:
    slice.set(new Uint8Array(this.jpeg!.marshal()), offset)
    offset += this.jpeg!.messageSize
        break
      case ImageType.ImageTypeLz4:
    slice.set(new Uint8Array(this.lz4!.marshal()), offset)
    offset += this.lz4!.messageSize
        break
      case ImageType.ImageTypeLzPlt:
    slice.set(new Uint8Array(this.lzPlt!.marshal()), offset)
    offset += this.lzPlt!.messageSize
        break
      case ImageType.ImageTypeZlibGlzRgb:
    slice.set(new Uint8Array(this.zlibGlz!.marshal()), offset)
    offset += this.zlibGlz!.messageSize
        break
      case ImageType.ImageTypeJpegAlpha:
    slice.set(new Uint8Array(this.jpegAlpha!.marshal()), offset)
    offset += this.jpegAlpha!.messageSize
        break
      case ImageType.ImageTypeSurface:
    slice.set(new Uint8Array(this.surface!.marshal()), offset)
    offset += Surface.messageSize
        break
    }

    return buf
  }
}
export class Pattern {
  pat: Image | undefined
  pos: Point

  static messageSize = 12

  constructor(pat: Image | undefined, pos: Point) {
    this.pat = pat
    this.pos = pos
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): Pattern {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const patPtr = view.getUint32(offset, true)
    offset += 4
    let pat: Image | undefined = undefined
    if (patPtr !== 0) {
        pat = Image.unmarshal(buf, Number(patPtr))
    }
    const pos = Point.unmarshal(buf, offset)
    offset += Point.messageSize
    return new Pattern(pat, pos)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(Pattern.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, 0, true) // TODO: This!
    offset += 8
    slice.set(new Uint8Array(this.pos.marshal()), offset)
    offset += Point.messageSize

    return buf
  }
}
export class Brush {
  type: BrushType
  color: number | undefined
  pattern: Pattern | undefined

  get messageSize() {
    let size = 0
    size += 1
    switch (this.type) {
      case BrushType.BrushTypeSolid:
        size += 4
        break
      case BrushType.BrushTypePattern:
        size += 12
        break
    }
    return size
  }

  constructor(type: BrushType, color: number | undefined, pattern: Pattern | undefined) {
    this.type = type
    this.color = color
    this.pattern = pattern
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): Brush {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const type = view.getUint8(offset)
    offset += 1
    let color
    let pattern
    switch (type) {
      case BrushType.BrushTypeSolid:
    color = view.getUint32(offset, true)
    offset += 4
        break
      case BrushType.BrushTypePattern:
    const pattern = Pattern.unmarshal(buf, offset)
    offset += Pattern.messageSize
        break
    }
    return new Brush(type, color, pattern)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint8(offset, this.type)
    offset += 1
    switch (this.type) {
      case BrushType.BrushTypeSolid:
    view.setUint32(offset, this.color!, true)
    offset += 4
        break
      case BrushType.BrushTypePattern:
    slice.set(new Uint8Array(this.pattern!.marshal()), offset)
    offset += Pattern.messageSize
        break
    }

    return buf
  }
}
export class QMask {
  flags: MaskFlags
  pos: Point
  bitmap: Image | undefined

  static messageSize = 13

  constructor(flags: MaskFlags, pos: Point, bitmap: Image | undefined) {
    this.flags = flags
    this.pos = pos
    this.bitmap = bitmap
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): QMask {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const flags = view.getUint8(offset)
    offset += 1
    const pos = Point.unmarshal(buf, offset)
    offset += Point.messageSize
    const bitmapPtr = view.getUint32(offset, true)
    offset += 4
    let bitmap: Image | undefined = undefined
    if (bitmapPtr !== 0) {
        bitmap = Image.unmarshal(buf, Number(bitmapPtr))
    }
    return new QMask(flags, pos, bitmap)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(QMask.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint8(offset, this.flags)
    offset += 1
    slice.set(new Uint8Array(this.pos.marshal()), offset)
    offset += Point.messageSize
    view.setUint32(offset, 0, true) // TODO: This!
    offset += 8

    return buf
  }
}
export class LineAttr {
  flags: LineFlags
  styleNseg: number | undefined
  style: Fixed284[] | undefined

  get messageSize() {
    let size = 0
    size += 1
    switch (this.flags) {
      case LineFlags.LineFlagsStyled:
        size += 1
        break
    }
    switch (this.flags) {
      case LineFlags.LineFlagsStyled:
        size += 4
        break
    }
    return size
  }

  constructor(flags: LineFlags, styleNseg: number | undefined, style: Fixed284[] | undefined) {
    this.flags = flags
    this.styleNseg = styleNseg
    this.style = style
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): LineAttr {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const flags = view.getUint8(offset)
    offset += 1
    let styleNseg
    switch (flags) {
      case LineFlags.LineFlagsStyled:
    styleNseg = view.getUint8(offset)
    offset += 1
        break
    }
    let style
    switch (flags) {
      case LineFlags.LineFlagsStyled:
    const stylePtr = view.getUint32(offset, true)
    offset += 4
    let style: Fixed284 | undefined = undefined
    if (stylePtr !== 0) {
        style = Fixed284.unmarshal(buf, Number(stylePtr))
    }
        break
    }
    return new LineAttr(flags, styleNseg, style)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint8(offset, this.flags)
    offset += 1
    switch (this.flags) {
      case LineFlags.LineFlagsStyled:
    view.setUint8(offset, this.styleNseg!)
    offset += 1
        break
    }
    switch (this.flags) {
      case LineFlags.LineFlagsStyled:
    view.setUint32(offset, 0, true) // TODO: This!
    offset += 8
        break
    }

    return buf
  }
}
export class RasterGlyphA1 {
  renderPos: Point
  glyphOrigin: Point
  width: number
  height: number
  data: number[]

  static messageSize = 16

  constructor(renderPos: Point, glyphOrigin: Point, width: number, height: number, data: number[]) {
    this.renderPos = renderPos
    this.glyphOrigin = glyphOrigin
    this.width = width
    this.height = height
    this.data = data
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): RasterGlyphA1 {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const renderPos = Point.unmarshal(buf, offset)
    offset += Point.messageSize
    const glyphOrigin = Point.unmarshal(buf, offset)
    offset += Point.messageSize
    const width = view.getUint16(offset, true)
    offset += 2
    const height = view.getUint16(offset, true)
    offset += 2
    let data: number[] = []
    for (let i = 0; i < height * Math.ceil(width * 1 / 8); i++) {
    const uint8 = view.getUint8(offset)
    offset += 1
    data.push(uint8)
    }
    return new RasterGlyphA1(renderPos, glyphOrigin, width, height, data)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(RasterGlyphA1.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    slice.set(new Uint8Array(this.renderPos.marshal()), offset)
    offset += Point.messageSize
    slice.set(new Uint8Array(this.glyphOrigin.marshal()), offset)
    offset += Point.messageSize
    view.setUint16(offset, this.width, true)
    offset += 2
    view.setUint16(offset, this.height, true)
    offset += 2

    return buf
  }
}
export class RasterGlyphA4 {
  renderPos: Point
  glyphOrigin: Point
  width: number
  height: number
  data: number[]

  static messageSize = 16

  constructor(renderPos: Point, glyphOrigin: Point, width: number, height: number, data: number[]) {
    this.renderPos = renderPos
    this.glyphOrigin = glyphOrigin
    this.width = width
    this.height = height
    this.data = data
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): RasterGlyphA4 {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const renderPos = Point.unmarshal(buf, offset)
    offset += Point.messageSize
    const glyphOrigin = Point.unmarshal(buf, offset)
    offset += Point.messageSize
    const width = view.getUint16(offset, true)
    offset += 2
    const height = view.getUint16(offset, true)
    offset += 2
    let data: number[] = []
    for (let i = 0; i < height * Math.ceil(width * 4 / 8); i++) {
    const uint8 = view.getUint8(offset)
    offset += 1
    data.push(uint8)
    }
    return new RasterGlyphA4(renderPos, glyphOrigin, width, height, data)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(RasterGlyphA4.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    slice.set(new Uint8Array(this.renderPos.marshal()), offset)
    offset += Point.messageSize
    slice.set(new Uint8Array(this.glyphOrigin.marshal()), offset)
    offset += Point.messageSize
    view.setUint16(offset, this.width, true)
    offset += 2
    view.setUint16(offset, this.height, true)
    offset += 2

    return buf
  }
}
export class RasterGlyphA8 {
  renderPos: Point
  glyphOrigin: Point
  width: number
  height: number
  data: number[]

  static messageSize = 16

  constructor(renderPos: Point, glyphOrigin: Point, width: number, height: number, data: number[]) {
    this.renderPos = renderPos
    this.glyphOrigin = glyphOrigin
    this.width = width
    this.height = height
    this.data = data
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): RasterGlyphA8 {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const renderPos = Point.unmarshal(buf, offset)
    offset += Point.messageSize
    const glyphOrigin = Point.unmarshal(buf, offset)
    offset += Point.messageSize
    const width = view.getUint16(offset, true)
    offset += 2
    const height = view.getUint16(offset, true)
    offset += 2
    let data: number[] = []
    for (let i = 0; i < height * Math.ceil(width * 8 / 8); i++) {
    const uint8 = view.getUint8(offset)
    offset += 1
    data.push(uint8)
    }
    return new RasterGlyphA8(renderPos, glyphOrigin, width, height, data)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(RasterGlyphA8.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    slice.set(new Uint8Array(this.renderPos.marshal()), offset)
    offset += Point.messageSize
    slice.set(new Uint8Array(this.glyphOrigin.marshal()), offset)
    offset += Point.messageSize
    view.setUint16(offset, this.width, true)
    offset += 2
    view.setUint16(offset, this.height, true)
    offset += 2

    return buf
  }
}
export class String {
  length: number
  flags: StringFlags
  glyphsRasterGlyphA1: RasterGlyphA1[] | undefined
  glyphsRasterGlyphA4: RasterGlyphA4[] | undefined
  glyphsRasterGlyphA8: RasterGlyphA8[] | undefined

  get messageSize() {
    let size = 0
    size += 2
    size += 1
    switch (this.flags) {
      case StringFlags.StringFlagsRasterA1:
        size += 0 // TODO: FIX THIS!
        break
      case StringFlags.StringFlagsRasterA4:
        size += 0 // TODO: FIX THIS!
        break
      case StringFlags.StringFlagsRasterA8:
        size += 0 // TODO: FIX THIS!
        break
    }
    return size
  }

  constructor(length: number, flags: StringFlags, glyphsRasterGlyphA1: RasterGlyphA1[] | undefined, glyphsRasterGlyphA4: RasterGlyphA4[] | undefined, glyphsRasterGlyphA8: RasterGlyphA8[] | undefined) {
    this.length = length
    this.flags = flags
    this.glyphsRasterGlyphA1 = glyphsRasterGlyphA1
    this.glyphsRasterGlyphA4 = glyphsRasterGlyphA4
    this.glyphsRasterGlyphA8 = glyphsRasterGlyphA8
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): String {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const length = view.getUint16(offset, true)
    offset += 2
    const flags = view.getUint8(offset)
    offset += 1
    let glyphsRasterGlyphA1
    let glyphsRasterGlyphA4
    let glyphsRasterGlyphA8
    switch (flags) {
      case StringFlags.StringFlagsRasterA1:
    let glyphsRasterGlyphA1: RasterGlyphA1[] = []
    for (let i = 0; i < length; i++) {
    const rasterGlyphA1 = RasterGlyphA1.unmarshal(buf, offset)
    offset += RasterGlyphA1.messageSize
    glyphsRasterGlyphA1.push(rasterGlyphA1)
    }
        break
      case StringFlags.StringFlagsRasterA4:
    let glyphsRasterGlyphA4: RasterGlyphA4[] = []
    for (let i = 0; i < length; i++) {
    const rasterGlyphA4 = RasterGlyphA4.unmarshal(buf, offset)
    offset += RasterGlyphA4.messageSize
    glyphsRasterGlyphA4.push(rasterGlyphA4)
    }
        break
      case StringFlags.StringFlagsRasterA8:
    let glyphsRasterGlyphA8: RasterGlyphA8[] = []
    for (let i = 0; i < length; i++) {
    const rasterGlyphA8 = RasterGlyphA8.unmarshal(buf, offset)
    offset += RasterGlyphA8.messageSize
    glyphsRasterGlyphA8.push(rasterGlyphA8)
    }
        break
    }
    return new String(length, flags, glyphsRasterGlyphA1, glyphsRasterGlyphA4, glyphsRasterGlyphA8)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint16(offset, this.length, true)
    offset += 2
    view.setUint8(offset, this.flags)
    offset += 1
    switch (this.flags) {
      case StringFlags.StringFlagsRasterA1:
    this.glyphsRasterGlyphA1!.forEach((rasterGlyphA1) => {
    slice.set(new Uint8Array(rasterGlyphA1.marshal()), offset)
    offset += RasterGlyphA1.messageSize
    })
        break
      case StringFlags.StringFlagsRasterA4:
    this.glyphsRasterGlyphA4!.forEach((rasterGlyphA4) => {
    slice.set(new Uint8Array(rasterGlyphA4.marshal()), offset)
    offset += RasterGlyphA4.messageSize
    })
        break
      case StringFlags.StringFlagsRasterA8:
    this.glyphsRasterGlyphA8!.forEach((rasterGlyphA8) => {
    slice.set(new Uint8Array(rasterGlyphA8.marshal()), offset)
    offset += RasterGlyphA8.messageSize
    })
        break
    }

    return buf
  }
}
export class StreamDataHeader {
  id: StreamIdT
  multiMediaTime: number

  static messageSize = 8

  constructor(id: StreamIdT, multiMediaTime: number) {
    this.id = id
    this.multiMediaTime = multiMediaTime
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): StreamDataHeader {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const id = StreamIdT.unmarshal(buf, offset)
    offset += StreamIdT.messageSize
    const multiMediaTime = view.getUint32(offset, true)
    offset += 4
    return new StreamDataHeader(id, multiMediaTime)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(StreamDataHeader.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    slice.set(new Uint8Array(this.id.marshal()), offset)
    offset += StreamIdT.messageSize
    view.setUint32(offset, this.multiMediaTime, true)
    offset += 4

    return buf
  }
}
export class Head {
  monitorId: number
  surfaceId: number
  width: number
  height: number
  x: number
  y: number
  flags: number

  static messageSize = 28

  constructor(monitorId: number, surfaceId: number, width: number, height: number, x: number, y: number, flags: number) {
    this.monitorId = monitorId
    this.surfaceId = surfaceId
    this.width = width
    this.height = height
    this.x = x
    this.y = y
    this.flags = flags
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): Head {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const monitorId = view.getUint32(offset, true)
    offset += 4
    const surfaceId = view.getUint32(offset, true)
    offset += 4
    const width = view.getUint32(offset, true)
    offset += 4
    const height = view.getUint32(offset, true)
    offset += 4
    const x = view.getUint32(offset, true)
    offset += 4
    const y = view.getUint32(offset, true)
    offset += 4
    const flags = view.getUint32(offset, true)
    offset += 4
    return new Head(monitorId, surfaceId, width, height, x, y, flags)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(Head.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.monitorId, true)
    offset += 4
    view.setUint32(offset, this.surfaceId, true)
    offset += 4
    view.setUint32(offset, this.width, true)
    offset += 4
    view.setUint32(offset, this.height, true)
    offset += 4
    view.setUint32(offset, this.x, true)
    offset += 4
    view.setUint32(offset, this.y, true)
    offset += 4
    view.setUint32(offset, this.flags, true)
    offset += 4

    return buf
  }
}
export enum GlScanoutFlags {
  Empty,
  GlScanoutFlagsY0Top = 1 << 0,
}
export class MsgDisplayMode {
  xRes: number
  yRes: number
  bits: number

  static messageSize = 12

  constructor(xRes: number, yRes: number, bits: number) {
    this.xRes = xRes
    this.yRes = yRes
    this.bits = bits
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgDisplayMode {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const xRes = view.getUint32(offset, true)
    offset += 4
    const yRes = view.getUint32(offset, true)
    offset += 4
    const bits = view.getUint32(offset, true)
    offset += 4
    return new MsgDisplayMode(xRes, yRes, bits)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgDisplayMode.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.xRes, true)
    offset += 4
    view.setUint32(offset, this.yRes, true)
    offset += 4
    view.setUint32(offset, this.bits, true)
    offset += 4

    return buf
  }
}
export class MsgDisplayMark {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgDisplayMark {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new MsgDisplayMark()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgDisplayMark.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export class MsgDisplayReset {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgDisplayReset {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new MsgDisplayReset()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgDisplayReset.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export class MsgDisplayCopyBits {
  base: DisplayBase
  srcPos: Point

  get messageSize() {
    let size = 0
    size += this.base.messageSize
    size += 8
    return size
  }

  constructor(base: DisplayBase, srcPos: Point) {
    this.base = base
    this.srcPos = srcPos
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): MsgDisplayCopyBits {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const base = DisplayBase.unmarshal(buf, offset)
    offset += base.messageSize
    const srcPos = Point.unmarshal(buf, offset)
    offset += Point.messageSize
    return new MsgDisplayCopyBits(base, srcPos)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    slice.set(new Uint8Array(this.base.marshal()), offset)
    offset += this.base.messageSize
    slice.set(new Uint8Array(this.srcPos.marshal()), offset)
    offset += Point.messageSize

    return buf
  }
}
export class MsgDisplayInvalList {
  count: number
  resources: ResourceID[]

  get messageSize() {
    let size = 0
    size += 2
    size += 9 * this.count
    return size
  }

  constructor(count: number, resources: ResourceID[]) {
    this.count = count
    this.resources = resources
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): MsgDisplayInvalList {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const count = view.getUint16(offset, true)
    offset += 2
    let resources: ResourceID[] = []
    for (let i = 0; i < count; i++) {
    const resourceID = ResourceID.unmarshal(buf, offset)
    offset += ResourceID.messageSize
    resources.push(resourceID)
    }
    return new MsgDisplayInvalList(count, resources)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint16(offset, this.count, true)
    offset += 2
    this.resources.forEach((resourceID) => {
    slice.set(new Uint8Array(resourceID.marshal()), offset)
    offset += ResourceID.messageSize
    })

    return buf
  }
}
export class MsgDisplayInvalAllPixmaps {
  waitCount: number
  waitList: WaitForChannel[]

  get messageSize() {
    let size = 0
    size += 1
    size += 10 * this.waitCount
    return size
  }

  constructor(waitCount: number, waitList: WaitForChannel[]) {
    this.waitCount = waitCount
    this.waitList = waitList
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): MsgDisplayInvalAllPixmaps {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const waitCount = view.getUint8(offset)
    offset += 1
    let waitList: WaitForChannel[] = []
    for (let i = 0; i < waitCount; i++) {
    const waitForChannel = WaitForChannel.unmarshal(buf, offset)
    offset += WaitForChannel.messageSize
    waitList.push(waitForChannel)
    }
    return new MsgDisplayInvalAllPixmaps(waitCount, waitList)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint8(offset, this.waitCount)
    offset += 1
    this.waitList.forEach((waitForChannel) => {
    slice.set(new Uint8Array(waitForChannel.marshal()), offset)
    offset += WaitForChannel.messageSize
    })

    return buf
  }
}
export class MsgDisplayInvalPalette {
  id: bigint

  static messageSize = 8

  constructor(id: bigint) {
    this.id = id
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgDisplayInvalPalette {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const id = view.getBigUint64(offset, true)
    offset += 8
    return new MsgDisplayInvalPalette(id)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgDisplayInvalPalette.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setBigUint64(offset, this.id, true)
    offset += 8

    return buf
  }
}
export class MsgDisplayInvalAllPalettes {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgDisplayInvalAllPalettes {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new MsgDisplayInvalAllPalettes()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgDisplayInvalAllPalettes.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export class MsgDisplayStreamCreate {
  surfaceId: number
  id: StreamIdT
  flags: StreamFlags
  codecType: VideoCodecType
  stamp: bigint
  streamWidth: number
  streamHeight: number
  srcWidth: number
  srcHeight: number
  dest: Rect
  clip: Clip

  get messageSize() {
    let size = 0
    size += 4
    size += 4
    size += 1
    size += 1
    size += 8
    size += 4
    size += 4
    size += 4
    size += 4
    size += 16
    size += this.clip.messageSize
    return size
  }

  constructor(surfaceId: number, id: StreamIdT, flags: StreamFlags, codecType: VideoCodecType, stamp: bigint, streamWidth: number, streamHeight: number, srcWidth: number, srcHeight: number, dest: Rect, clip: Clip) {
    this.surfaceId = surfaceId
    this.id = id
    this.flags = flags
    this.codecType = codecType
    this.stamp = stamp
    this.streamWidth = streamWidth
    this.streamHeight = streamHeight
    this.srcWidth = srcWidth
    this.srcHeight = srcHeight
    this.dest = dest
    this.clip = clip
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): MsgDisplayStreamCreate {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const surfaceId = view.getUint32(offset, true)
    offset += 4
    const id = StreamIdT.unmarshal(buf, offset)
    offset += StreamIdT.messageSize
    const flags = view.getUint8(offset)
    offset += 1
    const codecType = view.getUint8(offset)
    offset += 1
    const stamp = view.getBigUint64(offset, true)
    offset += 8
    const streamWidth = view.getUint32(offset, true)
    offset += 4
    const streamHeight = view.getUint32(offset, true)
    offset += 4
    const srcWidth = view.getUint32(offset, true)
    offset += 4
    const srcHeight = view.getUint32(offset, true)
    offset += 4
    const dest = Rect.unmarshal(buf, offset)
    offset += Rect.messageSize
    const clip = Clip.unmarshal(buf, offset)
    offset += clip.messageSize
    return new MsgDisplayStreamCreate(surfaceId, id, flags, codecType, stamp, streamWidth, streamHeight, srcWidth, srcHeight, dest, clip)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.surfaceId, true)
    offset += 4
    slice.set(new Uint8Array(this.id.marshal()), offset)
    offset += StreamIdT.messageSize
    view.setUint8(offset, this.flags)
    offset += 1
    view.setUint8(offset, this.codecType)
    offset += 1
    view.setBigUint64(offset, this.stamp, true)
    offset += 8
    view.setUint32(offset, this.streamWidth, true)
    offset += 4
    view.setUint32(offset, this.streamHeight, true)
    offset += 4
    view.setUint32(offset, this.srcWidth, true)
    offset += 4
    view.setUint32(offset, this.srcHeight, true)
    offset += 4
    slice.set(new Uint8Array(this.dest.marshal()), offset)
    offset += Rect.messageSize
    slice.set(new Uint8Array(this.clip.marshal()), offset)
    offset += this.clip.messageSize

    return buf
  }
}
export class MsgDisplayStreamData {
  base: StreamDataHeader
  dataSize: number
  data: number[]

  get messageSize() {
    let size = 0
    size += 8
    size += 4
    size += 1 * this.dataSize
    return size
  }

  constructor(base: StreamDataHeader, dataSize: number, data: number[]) {
    this.base = base
    this.dataSize = dataSize
    this.data = data
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): MsgDisplayStreamData {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const base = StreamDataHeader.unmarshal(buf, offset)
    offset += StreamDataHeader.messageSize
    const dataSize = view.getUint32(offset, true)
    offset += 4
    let data: number[] = []
    for (let i = 0; i < dataSize; i++) {
    const uint8 = view.getUint8(offset)
    offset += 1
    data.push(uint8)
    }
    return new MsgDisplayStreamData(base, dataSize, data)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    slice.set(new Uint8Array(this.base.marshal()), offset)
    offset += StreamDataHeader.messageSize
    view.setUint32(offset, this.dataSize, true)
    offset += 4
    this.data.forEach((uint8) => {
    view.setUint8(offset, uint8)
    offset += 1
    })

    return buf
  }
}
export class MsgDisplayStreamClip {
  id: StreamIdT
  clip: Clip

  get messageSize() {
    let size = 0
    size += 4
    size += this.clip.messageSize
    return size
  }

  constructor(id: StreamIdT, clip: Clip) {
    this.id = id
    this.clip = clip
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): MsgDisplayStreamClip {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const id = StreamIdT.unmarshal(buf, offset)
    offset += StreamIdT.messageSize
    const clip = Clip.unmarshal(buf, offset)
    offset += clip.messageSize
    return new MsgDisplayStreamClip(id, clip)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    slice.set(new Uint8Array(this.id.marshal()), offset)
    offset += StreamIdT.messageSize
    slice.set(new Uint8Array(this.clip.marshal()), offset)
    offset += this.clip.messageSize

    return buf
  }
}
export class MsgDisplayStreamDestroy {
  id: StreamIdT

  static messageSize = 4

  constructor(id: StreamIdT) {
    this.id = id
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgDisplayStreamDestroy {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const id = StreamIdT.unmarshal(buf, offset)
    offset += StreamIdT.messageSize
    return new MsgDisplayStreamDestroy(id)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgDisplayStreamDestroy.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    slice.set(new Uint8Array(this.id.marshal()), offset)
    offset += StreamIdT.messageSize

    return buf
  }
}
export class MsgDisplayStreamDestroyAll {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgDisplayStreamDestroyAll {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new MsgDisplayStreamDestroyAll()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgDisplayStreamDestroyAll.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export class Fill {
  brush: Brush
  ropDescriptor: Ropd
  mask: QMask

  get messageSize() {
    let size = 0
    size += this.brush.messageSize
    size += 2
    size += 13
    return size
  }

  constructor(brush: Brush, ropDescriptor: Ropd, mask: QMask) {
    this.brush = brush
    this.ropDescriptor = ropDescriptor
    this.mask = mask
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): Fill {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const brush = Brush.unmarshal(buf, offset)
    offset += brush.messageSize
    const ropDescriptor = view.getUint16(offset, true)
    offset += 2
    const mask = QMask.unmarshal(buf, offset)
    offset += QMask.messageSize
    return new Fill(brush, ropDescriptor, mask)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    slice.set(new Uint8Array(this.brush.marshal()), offset)
    offset += this.brush.messageSize
    view.setUint16(offset, this.ropDescriptor, true)
    offset += 2
    slice.set(new Uint8Array(this.mask.marshal()), offset)
    offset += QMask.messageSize

    return buf
  }
}
export class MsgDisplayDrawFill {
  base: DisplayBase
  data: Fill

  get messageSize() {
    let size = 0
    size += this.base.messageSize
    size += this.data.messageSize
    return size
  }

  constructor(base: DisplayBase, data: Fill) {
    this.base = base
    this.data = data
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): MsgDisplayDrawFill {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const base = DisplayBase.unmarshal(buf, offset)
    offset += base.messageSize
    const data = Fill.unmarshal(buf, offset)
    offset += data.messageSize
    return new MsgDisplayDrawFill(base, data)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    slice.set(new Uint8Array(this.base.marshal()), offset)
    offset += this.base.messageSize
    slice.set(new Uint8Array(this.data.marshal()), offset)
    offset += this.data.messageSize

    return buf
  }
}
export class Opaque {
  srcBitmap: Image | undefined
  srcArea: Rect
  brush: Brush
  ropDescriptor: Ropd
  scaleMode: ImageScaleMode
  mask: QMask

  get messageSize() {
    let size = 0
    size += 4
    size += 16
    size += this.brush.messageSize
    size += 2
    size += 1
    size += 13
    return size
  }

  constructor(srcBitmap: Image | undefined, srcArea: Rect, brush: Brush, ropDescriptor: Ropd, scaleMode: ImageScaleMode, mask: QMask) {
    this.srcBitmap = srcBitmap
    this.srcArea = srcArea
    this.brush = brush
    this.ropDescriptor = ropDescriptor
    this.scaleMode = scaleMode
    this.mask = mask
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): Opaque {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const srcBitmapPtr = view.getUint32(offset, true)
    offset += 4
    let srcBitmap: Image | undefined = undefined
    if (srcBitmapPtr !== 0) {
        srcBitmap = Image.unmarshal(buf, Number(srcBitmapPtr))
    }
    const srcArea = Rect.unmarshal(buf, offset)
    offset += Rect.messageSize
    const brush = Brush.unmarshal(buf, offset)
    offset += brush.messageSize
    const ropDescriptor = view.getUint16(offset, true)
    offset += 2
    const scaleMode = view.getUint8(offset)
    offset += 1
    const mask = QMask.unmarshal(buf, offset)
    offset += QMask.messageSize
    return new Opaque(srcBitmap, srcArea, brush, ropDescriptor, scaleMode, mask)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, 0, true) // TODO: This!
    offset += 8
    slice.set(new Uint8Array(this.srcArea.marshal()), offset)
    offset += Rect.messageSize
    slice.set(new Uint8Array(this.brush.marshal()), offset)
    offset += this.brush.messageSize
    view.setUint16(offset, this.ropDescriptor, true)
    offset += 2
    view.setUint8(offset, this.scaleMode)
    offset += 1
    slice.set(new Uint8Array(this.mask.marshal()), offset)
    offset += QMask.messageSize

    return buf
  }
}
export class MsgDisplayDrawOpaque {
  base: DisplayBase
  data: Opaque

  get messageSize() {
    let size = 0
    size += this.base.messageSize
    size += this.data.messageSize
    return size
  }

  constructor(base: DisplayBase, data: Opaque) {
    this.base = base
    this.data = data
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): MsgDisplayDrawOpaque {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const base = DisplayBase.unmarshal(buf, offset)
    offset += base.messageSize
    const data = Opaque.unmarshal(buf, offset)
    offset += data.messageSize
    return new MsgDisplayDrawOpaque(base, data)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    slice.set(new Uint8Array(this.base.marshal()), offset)
    offset += this.base.messageSize
    slice.set(new Uint8Array(this.data.marshal()), offset)
    offset += this.data.messageSize

    return buf
  }
}
export class Copy {
  srcBitmap: Image | undefined
  srcArea: Rect
  ropDescriptor: Ropd
  scaleMode: ImageScaleMode
  mask: QMask

  static messageSize = 36

  constructor(srcBitmap: Image | undefined, srcArea: Rect, ropDescriptor: Ropd, scaleMode: ImageScaleMode, mask: QMask) {
    this.srcBitmap = srcBitmap
    this.srcArea = srcArea
    this.ropDescriptor = ropDescriptor
    this.scaleMode = scaleMode
    this.mask = mask
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): Copy {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const srcBitmapPtr = view.getUint32(offset, true)
    offset += 4
    let srcBitmap: Image | undefined = undefined
    if (srcBitmapPtr !== 0) {
        srcBitmap = Image.unmarshal(buf, Number(srcBitmapPtr))
    }
    const srcArea = Rect.unmarshal(buf, offset)
    offset += Rect.messageSize
    const ropDescriptor = view.getUint16(offset, true)
    offset += 2
    const scaleMode = view.getUint8(offset)
    offset += 1
    const mask = QMask.unmarshal(buf, offset)
    offset += QMask.messageSize
    return new Copy(srcBitmap, srcArea, ropDescriptor, scaleMode, mask)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(Copy.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, 0, true) // TODO: This!
    offset += 8
    slice.set(new Uint8Array(this.srcArea.marshal()), offset)
    offset += Rect.messageSize
    view.setUint16(offset, this.ropDescriptor, true)
    offset += 2
    view.setUint8(offset, this.scaleMode)
    offset += 1
    slice.set(new Uint8Array(this.mask.marshal()), offset)
    offset += QMask.messageSize

    return buf
  }
}
export class MsgDisplayDrawCopy {
  base: DisplayBase
  data: Copy

  get messageSize() {
    let size = 0
    size += this.base.messageSize
    size += 36
    return size
  }

  constructor(base: DisplayBase, data: Copy) {
    this.base = base
    this.data = data
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): MsgDisplayDrawCopy {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const base = DisplayBase.unmarshal(buf, offset)
    offset += base.messageSize
    const data = Copy.unmarshal(buf, offset)
    offset += Copy.messageSize
    return new MsgDisplayDrawCopy(base, data)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    slice.set(new Uint8Array(this.base.marshal()), offset)
    offset += this.base.messageSize
    slice.set(new Uint8Array(this.data.marshal()), offset)
    offset += Copy.messageSize

    return buf
  }
}
export class Blend {
  srcBitmap: Image | undefined
  srcArea: Rect
  ropDescriptor: Ropd
  scaleMode: ImageScaleMode
  mask: QMask

  static messageSize = 36

  constructor(srcBitmap: Image | undefined, srcArea: Rect, ropDescriptor: Ropd, scaleMode: ImageScaleMode, mask: QMask) {
    this.srcBitmap = srcBitmap
    this.srcArea = srcArea
    this.ropDescriptor = ropDescriptor
    this.scaleMode = scaleMode
    this.mask = mask
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): Blend {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const srcBitmapPtr = view.getUint32(offset, true)
    offset += 4
    let srcBitmap: Image | undefined = undefined
    if (srcBitmapPtr !== 0) {
        srcBitmap = Image.unmarshal(buf, Number(srcBitmapPtr))
    }
    const srcArea = Rect.unmarshal(buf, offset)
    offset += Rect.messageSize
    const ropDescriptor = view.getUint16(offset, true)
    offset += 2
    const scaleMode = view.getUint8(offset)
    offset += 1
    const mask = QMask.unmarshal(buf, offset)
    offset += QMask.messageSize
    return new Blend(srcBitmap, srcArea, ropDescriptor, scaleMode, mask)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(Blend.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, 0, true) // TODO: This!
    offset += 8
    slice.set(new Uint8Array(this.srcArea.marshal()), offset)
    offset += Rect.messageSize
    view.setUint16(offset, this.ropDescriptor, true)
    offset += 2
    view.setUint8(offset, this.scaleMode)
    offset += 1
    slice.set(new Uint8Array(this.mask.marshal()), offset)
    offset += QMask.messageSize

    return buf
  }
}
export class MsgDisplayDrawBlend {
  base: DisplayBase
  data: Blend

  get messageSize() {
    let size = 0
    size += this.base.messageSize
    size += 36
    return size
  }

  constructor(base: DisplayBase, data: Blend) {
    this.base = base
    this.data = data
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): MsgDisplayDrawBlend {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const base = DisplayBase.unmarshal(buf, offset)
    offset += base.messageSize
    const data = Blend.unmarshal(buf, offset)
    offset += Blend.messageSize
    return new MsgDisplayDrawBlend(base, data)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    slice.set(new Uint8Array(this.base.marshal()), offset)
    offset += this.base.messageSize
    slice.set(new Uint8Array(this.data.marshal()), offset)
    offset += Blend.messageSize

    return buf
  }
}
export class Blackness {
  mask: QMask

  static messageSize = 13

  constructor(mask: QMask) {
    this.mask = mask
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): Blackness {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const mask = QMask.unmarshal(buf, offset)
    offset += QMask.messageSize
    return new Blackness(mask)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(Blackness.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    slice.set(new Uint8Array(this.mask.marshal()), offset)
    offset += QMask.messageSize

    return buf
  }
}
export class MsgDisplayDrawBlackness {
  base: DisplayBase
  data: Blackness

  get messageSize() {
    let size = 0
    size += this.base.messageSize
    size += 13
    return size
  }

  constructor(base: DisplayBase, data: Blackness) {
    this.base = base
    this.data = data
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): MsgDisplayDrawBlackness {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const base = DisplayBase.unmarshal(buf, offset)
    offset += base.messageSize
    const data = Blackness.unmarshal(buf, offset)
    offset += Blackness.messageSize
    return new MsgDisplayDrawBlackness(base, data)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    slice.set(new Uint8Array(this.base.marshal()), offset)
    offset += this.base.messageSize
    slice.set(new Uint8Array(this.data.marshal()), offset)
    offset += Blackness.messageSize

    return buf
  }
}
export class Whiteness {
  mask: QMask

  static messageSize = 13

  constructor(mask: QMask) {
    this.mask = mask
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): Whiteness {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const mask = QMask.unmarshal(buf, offset)
    offset += QMask.messageSize
    return new Whiteness(mask)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(Whiteness.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    slice.set(new Uint8Array(this.mask.marshal()), offset)
    offset += QMask.messageSize

    return buf
  }
}
export class MsgDisplayDrawWhiteness {
  base: DisplayBase
  data: Whiteness

  get messageSize() {
    let size = 0
    size += this.base.messageSize
    size += 13
    return size
  }

  constructor(base: DisplayBase, data: Whiteness) {
    this.base = base
    this.data = data
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): MsgDisplayDrawWhiteness {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const base = DisplayBase.unmarshal(buf, offset)
    offset += base.messageSize
    const data = Whiteness.unmarshal(buf, offset)
    offset += Whiteness.messageSize
    return new MsgDisplayDrawWhiteness(base, data)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    slice.set(new Uint8Array(this.base.marshal()), offset)
    offset += this.base.messageSize
    slice.set(new Uint8Array(this.data.marshal()), offset)
    offset += Whiteness.messageSize

    return buf
  }
}
export class Invers {
  mask: QMask

  static messageSize = 13

  constructor(mask: QMask) {
    this.mask = mask
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): Invers {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const mask = QMask.unmarshal(buf, offset)
    offset += QMask.messageSize
    return new Invers(mask)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(Invers.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    slice.set(new Uint8Array(this.mask.marshal()), offset)
    offset += QMask.messageSize

    return buf
  }
}
export class MsgDisplayDrawInvers {
  base: DisplayBase
  data: Invers

  get messageSize() {
    let size = 0
    size += this.base.messageSize
    size += 13
    return size
  }

  constructor(base: DisplayBase, data: Invers) {
    this.base = base
    this.data = data
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): MsgDisplayDrawInvers {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const base = DisplayBase.unmarshal(buf, offset)
    offset += base.messageSize
    const data = Invers.unmarshal(buf, offset)
    offset += Invers.messageSize
    return new MsgDisplayDrawInvers(base, data)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    slice.set(new Uint8Array(this.base.marshal()), offset)
    offset += this.base.messageSize
    slice.set(new Uint8Array(this.data.marshal()), offset)
    offset += Invers.messageSize

    return buf
  }
}
export class Rop3 {
  srcBitmap: Image | undefined
  srcArea: Rect
  brush: Brush
  rop3: number
  scaleMode: ImageScaleMode
  mask: QMask

  get messageSize() {
    let size = 0
    size += 4
    size += 16
    size += this.brush.messageSize
    size += 1
    size += 1
    size += 13
    return size
  }

  constructor(srcBitmap: Image | undefined, srcArea: Rect, brush: Brush, rop3: number, scaleMode: ImageScaleMode, mask: QMask) {
    this.srcBitmap = srcBitmap
    this.srcArea = srcArea
    this.brush = brush
    this.rop3 = rop3
    this.scaleMode = scaleMode
    this.mask = mask
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): Rop3 {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const srcBitmapPtr = view.getUint32(offset, true)
    offset += 4
    let srcBitmap: Image | undefined = undefined
    if (srcBitmapPtr !== 0) {
        srcBitmap = Image.unmarshal(buf, Number(srcBitmapPtr))
    }
    const srcArea = Rect.unmarshal(buf, offset)
    offset += Rect.messageSize
    const brush = Brush.unmarshal(buf, offset)
    offset += brush.messageSize
    const rop3 = view.getUint8(offset)
    offset += 1
    const scaleMode = view.getUint8(offset)
    offset += 1
    const mask = QMask.unmarshal(buf, offset)
    offset += QMask.messageSize
    return new Rop3(srcBitmap, srcArea, brush, rop3, scaleMode, mask)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, 0, true) // TODO: This!
    offset += 8
    slice.set(new Uint8Array(this.srcArea.marshal()), offset)
    offset += Rect.messageSize
    slice.set(new Uint8Array(this.brush.marshal()), offset)
    offset += this.brush.messageSize
    view.setUint8(offset, this.rop3)
    offset += 1
    view.setUint8(offset, this.scaleMode)
    offset += 1
    slice.set(new Uint8Array(this.mask.marshal()), offset)
    offset += QMask.messageSize

    return buf
  }
}
export class MsgDisplayDrawRop3 {
  base: DisplayBase
  data: Rop3

  get messageSize() {
    let size = 0
    size += this.base.messageSize
    size += this.data.messageSize
    return size
  }

  constructor(base: DisplayBase, data: Rop3) {
    this.base = base
    this.data = data
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): MsgDisplayDrawRop3 {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const base = DisplayBase.unmarshal(buf, offset)
    offset += base.messageSize
    const data = Rop3.unmarshal(buf, offset)
    offset += data.messageSize
    return new MsgDisplayDrawRop3(base, data)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    slice.set(new Uint8Array(this.base.marshal()), offset)
    offset += this.base.messageSize
    slice.set(new Uint8Array(this.data.marshal()), offset)
    offset += this.data.messageSize

    return buf
  }
}
export class Stroke {
  path: Path | undefined
  attr: LineAttr
  brush: Brush
  foreMode: number
  backMode: number

  get messageSize() {
    let size = 0
    size += 4
    size += this.attr.messageSize
    size += this.brush.messageSize
    size += 2
    size += 2
    return size
  }

  constructor(path: Path | undefined, attr: LineAttr, brush: Brush, foreMode: number, backMode: number) {
    this.path = path
    this.attr = attr
    this.brush = brush
    this.foreMode = foreMode
    this.backMode = backMode
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): Stroke {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const pathPtr = view.getUint32(offset, true)
    offset += 4
    let path: Path | undefined = undefined
    if (pathPtr !== 0) {
        path = Path.unmarshal(buf, Number(pathPtr))
    }
    const attr = LineAttr.unmarshal(buf, offset)
    offset += attr.messageSize
    const brush = Brush.unmarshal(buf, offset)
    offset += brush.messageSize
    const foreMode = view.getUint16(offset, true)
    offset += 2
    const backMode = view.getUint16(offset, true)
    offset += 2
    return new Stroke(path, attr, brush, foreMode, backMode)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, 0, true) // TODO: This!
    offset += 8
    slice.set(new Uint8Array(this.attr.marshal()), offset)
    offset += this.attr.messageSize
    slice.set(new Uint8Array(this.brush.marshal()), offset)
    offset += this.brush.messageSize
    view.setUint16(offset, this.foreMode, true)
    offset += 2
    view.setUint16(offset, this.backMode, true)
    offset += 2

    return buf
  }
}
export class MsgDisplayDrawStroke {
  base: DisplayBase
  data: Stroke

  get messageSize() {
    let size = 0
    size += this.base.messageSize
    size += this.data.messageSize
    return size
  }

  constructor(base: DisplayBase, data: Stroke) {
    this.base = base
    this.data = data
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): MsgDisplayDrawStroke {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const base = DisplayBase.unmarshal(buf, offset)
    offset += base.messageSize
    const data = Stroke.unmarshal(buf, offset)
    offset += data.messageSize
    return new MsgDisplayDrawStroke(base, data)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    slice.set(new Uint8Array(this.base.marshal()), offset)
    offset += this.base.messageSize
    slice.set(new Uint8Array(this.data.marshal()), offset)
    offset += this.data.messageSize

    return buf
  }
}
export class Text {
  str: String | undefined
  backArea: Rect
  foreBrush: Brush
  backBrush: Brush
  foreMode: number
  backMode: number

  get messageSize() {
    let size = 0
    size += 4
    size += 16
    size += this.foreBrush.messageSize
    size += this.backBrush.messageSize
    size += 2
    size += 2
    return size
  }

  constructor(str: String | undefined, backArea: Rect, foreBrush: Brush, backBrush: Brush, foreMode: number, backMode: number) {
    this.str = str
    this.backArea = backArea
    this.foreBrush = foreBrush
    this.backBrush = backBrush
    this.foreMode = foreMode
    this.backMode = backMode
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): Text {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const strPtr = view.getUint32(offset, true)
    offset += 4
    let str: String | undefined = undefined
    if (strPtr !== 0) {
        str = String.unmarshal(buf, Number(strPtr))
    }
    const backArea = Rect.unmarshal(buf, offset)
    offset += Rect.messageSize
    const foreBrush = Brush.unmarshal(buf, offset)
    offset += foreBrush.messageSize
    const backBrush = Brush.unmarshal(buf, offset)
    offset += backBrush.messageSize
    const foreMode = view.getUint16(offset, true)
    offset += 2
    const backMode = view.getUint16(offset, true)
    offset += 2
    return new Text(str, backArea, foreBrush, backBrush, foreMode, backMode)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, 0, true) // TODO: This!
    offset += 8
    slice.set(new Uint8Array(this.backArea.marshal()), offset)
    offset += Rect.messageSize
    slice.set(new Uint8Array(this.foreBrush.marshal()), offset)
    offset += this.foreBrush.messageSize
    slice.set(new Uint8Array(this.backBrush.marshal()), offset)
    offset += this.backBrush.messageSize
    view.setUint16(offset, this.foreMode, true)
    offset += 2
    view.setUint16(offset, this.backMode, true)
    offset += 2

    return buf
  }
}
export class MsgDisplayDrawText {
  base: DisplayBase
  data: Text

  get messageSize() {
    let size = 0
    size += this.base.messageSize
    size += this.data.messageSize
    return size
  }

  constructor(base: DisplayBase, data: Text) {
    this.base = base
    this.data = data
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): MsgDisplayDrawText {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const base = DisplayBase.unmarshal(buf, offset)
    offset += base.messageSize
    const data = Text.unmarshal(buf, offset)
    offset += data.messageSize
    return new MsgDisplayDrawText(base, data)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    slice.set(new Uint8Array(this.base.marshal()), offset)
    offset += this.base.messageSize
    slice.set(new Uint8Array(this.data.marshal()), offset)
    offset += this.data.messageSize

    return buf
  }
}
export class Transparent {
  srcBitmap: Image | undefined
  srcArea: Rect
  srcColor: number
  trueColor: number

  static messageSize = 28

  constructor(srcBitmap: Image | undefined, srcArea: Rect, srcColor: number, trueColor: number) {
    this.srcBitmap = srcBitmap
    this.srcArea = srcArea
    this.srcColor = srcColor
    this.trueColor = trueColor
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): Transparent {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const srcBitmapPtr = view.getUint32(offset, true)
    offset += 4
    let srcBitmap: Image | undefined = undefined
    if (srcBitmapPtr !== 0) {
        srcBitmap = Image.unmarshal(buf, Number(srcBitmapPtr))
    }
    const srcArea = Rect.unmarshal(buf, offset)
    offset += Rect.messageSize
    const srcColor = view.getUint32(offset, true)
    offset += 4
    const trueColor = view.getUint32(offset, true)
    offset += 4
    return new Transparent(srcBitmap, srcArea, srcColor, trueColor)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(Transparent.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, 0, true) // TODO: This!
    offset += 8
    slice.set(new Uint8Array(this.srcArea.marshal()), offset)
    offset += Rect.messageSize
    view.setUint32(offset, this.srcColor, true)
    offset += 4
    view.setUint32(offset, this.trueColor, true)
    offset += 4

    return buf
  }
}
export class MsgDisplayDrawTransparent {
  base: DisplayBase
  data: Transparent

  get messageSize() {
    let size = 0
    size += this.base.messageSize
    size += 28
    return size
  }

  constructor(base: DisplayBase, data: Transparent) {
    this.base = base
    this.data = data
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): MsgDisplayDrawTransparent {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const base = DisplayBase.unmarshal(buf, offset)
    offset += base.messageSize
    const data = Transparent.unmarshal(buf, offset)
    offset += Transparent.messageSize
    return new MsgDisplayDrawTransparent(base, data)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    slice.set(new Uint8Array(this.base.marshal()), offset)
    offset += this.base.messageSize
    slice.set(new Uint8Array(this.data.marshal()), offset)
    offset += Transparent.messageSize

    return buf
  }
}
export class AlphaBlend {
  alphaFlags: AlphaFlags
  alpha: number
  srcBitmap: Image | undefined
  srcArea: Rect

  static messageSize = 22

  constructor(alphaFlags: AlphaFlags, alpha: number, srcBitmap: Image | undefined, srcArea: Rect) {
    this.alphaFlags = alphaFlags
    this.alpha = alpha
    this.srcBitmap = srcBitmap
    this.srcArea = srcArea
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): AlphaBlend {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const alphaFlags = view.getUint8(offset)
    offset += 1
    const alpha = view.getUint8(offset)
    offset += 1
    const srcBitmapPtr = view.getUint32(offset, true)
    offset += 4
    let srcBitmap: Image | undefined = undefined
    if (srcBitmapPtr !== 0) {
        srcBitmap = Image.unmarshal(buf, Number(srcBitmapPtr))
    }
    const srcArea = Rect.unmarshal(buf, offset)
    offset += Rect.messageSize
    return new AlphaBlend(alphaFlags, alpha, srcBitmap, srcArea)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(AlphaBlend.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint8(offset, this.alphaFlags)
    offset += 1
    view.setUint8(offset, this.alpha)
    offset += 1
    view.setUint32(offset, 0, true) // TODO: This!
    offset += 8
    slice.set(new Uint8Array(this.srcArea.marshal()), offset)
    offset += Rect.messageSize

    return buf
  }
}
export class MsgDisplayDrawAlphaBlend {
  base: DisplayBase
  data: AlphaBlend

  get messageSize() {
    let size = 0
    size += this.base.messageSize
    size += 22
    return size
  }

  constructor(base: DisplayBase, data: AlphaBlend) {
    this.base = base
    this.data = data
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): MsgDisplayDrawAlphaBlend {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const base = DisplayBase.unmarshal(buf, offset)
    offset += base.messageSize
    const data = AlphaBlend.unmarshal(buf, offset)
    offset += AlphaBlend.messageSize
    return new MsgDisplayDrawAlphaBlend(base, data)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    slice.set(new Uint8Array(this.base.marshal()), offset)
    offset += this.base.messageSize
    slice.set(new Uint8Array(this.data.marshal()), offset)
    offset += AlphaBlend.messageSize

    return buf
  }
}
export class MsgDisplaySurfaceCreate {
  surfaceId: number
  width: number
  height: number
  format: SurfaceFmt
  flags: SurfaceFlags

  static messageSize = 20

  constructor(surfaceId: number, width: number, height: number, format: SurfaceFmt, flags: SurfaceFlags) {
    this.surfaceId = surfaceId
    this.width = width
    this.height = height
    this.format = format
    this.flags = flags
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgDisplaySurfaceCreate {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const surfaceId = view.getUint32(offset, true)
    offset += 4
    const width = view.getUint32(offset, true)
    offset += 4
    const height = view.getUint32(offset, true)
    offset += 4
    const format = view.getUint32(offset, true)
    offset += 4
    const flags = view.getUint32(offset, true)
    offset += 4
    return new MsgDisplaySurfaceCreate(surfaceId, width, height, format, flags)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgDisplaySurfaceCreate.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.surfaceId, true)
    offset += 4
    view.setUint32(offset, this.width, true)
    offset += 4
    view.setUint32(offset, this.height, true)
    offset += 4
    view.setUint32(offset, this.format, true)
    offset += 4
    view.setUint32(offset, this.flags, true)
    offset += 4

    return buf
  }
}
export class MsgDisplaySurfaceDestroy {
  surfaceId: number

  static messageSize = 4

  constructor(surfaceId: number) {
    this.surfaceId = surfaceId
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgDisplaySurfaceDestroy {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const surfaceId = view.getUint32(offset, true)
    offset += 4
    return new MsgDisplaySurfaceDestroy(surfaceId)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgDisplaySurfaceDestroy.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.surfaceId, true)
    offset += 4

    return buf
  }
}
export class MsgDisplayStreamDataSized {
  base: StreamDataHeader
  width: number
  height: number
  dest: Rect
  dataSize: number
  data: number[]

  get messageSize() {
    let size = 0
    size += 8
    size += 4
    size += 4
    size += 16
    size += 4
    size += 1 * this.dataSize
    return size
  }

  constructor(base: StreamDataHeader, width: number, height: number, dest: Rect, dataSize: number, data: number[]) {
    this.base = base
    this.width = width
    this.height = height
    this.dest = dest
    this.dataSize = dataSize
    this.data = data
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): MsgDisplayStreamDataSized {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const base = StreamDataHeader.unmarshal(buf, offset)
    offset += StreamDataHeader.messageSize
    const width = view.getUint32(offset, true)
    offset += 4
    const height = view.getUint32(offset, true)
    offset += 4
    const dest = Rect.unmarshal(buf, offset)
    offset += Rect.messageSize
    const dataSize = view.getUint32(offset, true)
    offset += 4
    let data: number[] = []
    for (let i = 0; i < dataSize; i++) {
    const uint8 = view.getUint8(offset)
    offset += 1
    data.push(uint8)
    }
    return new MsgDisplayStreamDataSized(base, width, height, dest, dataSize, data)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    slice.set(new Uint8Array(this.base.marshal()), offset)
    offset += StreamDataHeader.messageSize
    view.setUint32(offset, this.width, true)
    offset += 4
    view.setUint32(offset, this.height, true)
    offset += 4
    slice.set(new Uint8Array(this.dest.marshal()), offset)
    offset += Rect.messageSize
    view.setUint32(offset, this.dataSize, true)
    offset += 4
    this.data.forEach((uint8) => {
    view.setUint8(offset, uint8)
    offset += 1
    })

    return buf
  }
}
export class MsgDisplayMonitorsConfig {
  count: number
  maxAllowed: number
  heads: Head[]

  get messageSize() {
    let size = 0
    size += 2
    size += 2
    size += 28 * this.count
    return size
  }

  constructor(count: number, maxAllowed: number, heads: Head[]) {
    this.count = count
    this.maxAllowed = maxAllowed
    this.heads = heads
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): MsgDisplayMonitorsConfig {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const count = view.getUint16(offset, true)
    offset += 2
    const maxAllowed = view.getUint16(offset, true)
    offset += 2
    let heads: Head[] = []
    for (let i = 0; i < count; i++) {
    const head = Head.unmarshal(buf, offset)
    offset += Head.messageSize
    heads.push(head)
    }
    return new MsgDisplayMonitorsConfig(count, maxAllowed, heads)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint16(offset, this.count, true)
    offset += 2
    view.setUint16(offset, this.maxAllowed, true)
    offset += 2
    this.heads.forEach((head) => {
    slice.set(new Uint8Array(head.marshal()), offset)
    offset += Head.messageSize
    })

    return buf
  }
}
export class Composite {
  flags: CompositeFlags
  srcBitmap: Image | undefined
  maskBitmap: Image | undefined
  srcTransform: Transform | undefined
  maskTransform: Transform | undefined
  srcOrigin: Point16
  maskOrigin: Point16

  get messageSize() {
    let size = 0
    size += 4
    size += 4
    switch (this.flags) {
      case CompositeFlags.CompositeFlagsHasMask:
        size += 4
        break
    }
    switch (this.flags) {
      case CompositeFlags.CompositeFlagsHasSrcTransform:
        size += 24
        break
    }
    switch (this.flags) {
      case CompositeFlags.CompositeFlagsHasMaskTransform:
        size += 24
        break
    }
    size += 4
    size += 4
    return size
  }

  constructor(flags: CompositeFlags, srcBitmap: Image | undefined, maskBitmap: Image | undefined, srcTransform: Transform | undefined, maskTransform: Transform | undefined, srcOrigin: Point16, maskOrigin: Point16) {
    this.flags = flags
    this.srcBitmap = srcBitmap
    this.maskBitmap = maskBitmap
    this.srcTransform = srcTransform
    this.maskTransform = maskTransform
    this.srcOrigin = srcOrigin
    this.maskOrigin = maskOrigin
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): Composite {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const flags = view.getUint32(offset, true)
    offset += 4
    const srcBitmapPtr = view.getUint32(offset, true)
    offset += 4
    let srcBitmap: Image | undefined = undefined
    if (srcBitmapPtr !== 0) {
        srcBitmap = Image.unmarshal(buf, Number(srcBitmapPtr))
    }
    let maskBitmap
    switch (flags) {
      case CompositeFlags.CompositeFlagsHasMask:
    const maskBitmapPtr = view.getUint32(offset, true)
    offset += 4
    let maskBitmap: Image | undefined = undefined
    if (maskBitmapPtr !== 0) {
        maskBitmap = Image.unmarshal(buf, Number(maskBitmapPtr))
    }
        break
    }
    let srcTransform
    switch (flags) {
      case CompositeFlags.CompositeFlagsHasSrcTransform:
    const srcTransform = Transform.unmarshal(buf, offset)
    offset += Transform.messageSize
        break
    }
    let maskTransform
    switch (flags) {
      case CompositeFlags.CompositeFlagsHasMaskTransform:
    const maskTransform = Transform.unmarshal(buf, offset)
    offset += Transform.messageSize
        break
    }
    const srcOrigin = Point16.unmarshal(buf, offset)
    offset += Point16.messageSize
    const maskOrigin = Point16.unmarshal(buf, offset)
    offset += Point16.messageSize
    return new Composite(flags, srcBitmap, maskBitmap, srcTransform, maskTransform, srcOrigin, maskOrigin)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.flags, true)
    offset += 4
    view.setUint32(offset, 0, true) // TODO: This!
    offset += 8
    switch (this.flags) {
      case CompositeFlags.CompositeFlagsHasMask:
    view.setUint32(offset, 0, true) // TODO: This!
    offset += 8
        break
    }
    switch (this.flags) {
      case CompositeFlags.CompositeFlagsHasSrcTransform:
    slice.set(new Uint8Array(this.srcTransform!.marshal()), offset)
    offset += Transform.messageSize
        break
    }
    switch (this.flags) {
      case CompositeFlags.CompositeFlagsHasMaskTransform:
    slice.set(new Uint8Array(this.maskTransform!.marshal()), offset)
    offset += Transform.messageSize
        break
    }
    slice.set(new Uint8Array(this.srcOrigin.marshal()), offset)
    offset += Point16.messageSize
    slice.set(new Uint8Array(this.maskOrigin.marshal()), offset)
    offset += Point16.messageSize

    return buf
  }
}
export class MsgDisplayDrawComposite {
  base: DisplayBase
  data: Composite

  get messageSize() {
    let size = 0
    size += this.base.messageSize
    size += this.data.messageSize
    return size
  }

  constructor(base: DisplayBase, data: Composite) {
    this.base = base
    this.data = data
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): MsgDisplayDrawComposite {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const base = DisplayBase.unmarshal(buf, offset)
    offset += base.messageSize
    const data = Composite.unmarshal(buf, offset)
    offset += data.messageSize
    return new MsgDisplayDrawComposite(base, data)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    slice.set(new Uint8Array(this.base.marshal()), offset)
    offset += this.base.messageSize
    slice.set(new Uint8Array(this.data.marshal()), offset)
    offset += this.data.messageSize

    return buf
  }
}
export class MsgDisplayStreamActivateReport {
  streamId: StreamIdT
  uniqueId: number
  maxWindowSize: number
  timeoutMs: number

  static messageSize = 16

  constructor(streamId: StreamIdT, uniqueId: number, maxWindowSize: number, timeoutMs: number) {
    this.streamId = streamId
    this.uniqueId = uniqueId
    this.maxWindowSize = maxWindowSize
    this.timeoutMs = timeoutMs
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgDisplayStreamActivateReport {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const streamId = StreamIdT.unmarshal(buf, offset)
    offset += StreamIdT.messageSize
    const uniqueId = view.getUint32(offset, true)
    offset += 4
    const maxWindowSize = view.getUint32(offset, true)
    offset += 4
    const timeoutMs = view.getUint32(offset, true)
    offset += 4
    return new MsgDisplayStreamActivateReport(streamId, uniqueId, maxWindowSize, timeoutMs)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgDisplayStreamActivateReport.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    slice.set(new Uint8Array(this.streamId.marshal()), offset)
    offset += StreamIdT.messageSize
    view.setUint32(offset, this.uniqueId, true)
    offset += 4
    view.setUint32(offset, this.maxWindowSize, true)
    offset += 4
    view.setUint32(offset, this.timeoutMs, true)
    offset += 4

    return buf
  }
}
export class MsgDisplayGlScanoutUnix {
  drmDmaBufFd: number
  width: number
  height: number
  stride: number
  drmFourccFormat: number
  flags: GlScanoutFlags

  static messageSize = 24

  constructor(drmDmaBufFd: number, width: number, height: number, stride: number, drmFourccFormat: number, flags: GlScanoutFlags) {
    this.drmDmaBufFd = drmDmaBufFd
    this.width = width
    this.height = height
    this.stride = stride
    this.drmFourccFormat = drmFourccFormat
    this.flags = flags
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgDisplayGlScanoutUnix {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const drmDmaBufFd = view.getInt32(offset, true)
    offset += 4
    const width = view.getUint32(offset, true)
    offset += 4
    const height = view.getUint32(offset, true)
    offset += 4
    const stride = view.getUint32(offset, true)
    offset += 4
    const drmFourccFormat = view.getUint32(offset, true)
    offset += 4
    const flags = view.getUint32(offset, true)
    offset += 4
    return new MsgDisplayGlScanoutUnix(drmDmaBufFd, width, height, stride, drmFourccFormat, flags)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgDisplayGlScanoutUnix.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setInt32(offset, this.drmDmaBufFd, true)
    offset += 4
    view.setUint32(offset, this.width, true)
    offset += 4
    view.setUint32(offset, this.height, true)
    offset += 4
    view.setUint32(offset, this.stride, true)
    offset += 4
    view.setUint32(offset, this.drmFourccFormat, true)
    offset += 4
    view.setUint32(offset, this.flags, true)
    offset += 4

    return buf
  }
}
export class MsgDisplayGlDraw {
  x: number
  y: number
  w: number
  h: number

  static messageSize = 16

  constructor(x: number, y: number, w: number, h: number) {
    this.x = x
    this.y = y
    this.w = w
    this.h = h
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgDisplayGlDraw {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const x = view.getUint32(offset, true)
    offset += 4
    const y = view.getUint32(offset, true)
    offset += 4
    const w = view.getUint32(offset, true)
    offset += 4
    const h = view.getUint32(offset, true)
    offset += 4
    return new MsgDisplayGlDraw(x, y, w, h)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgDisplayGlDraw.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.x, true)
    offset += 4
    view.setUint32(offset, this.y, true)
    offset += 4
    view.setUint32(offset, this.w, true)
    offset += 4
    view.setUint32(offset, this.h, true)
    offset += 4

    return buf
  }
}
export class MsgDisplayQualityIndicator {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgDisplayQualityIndicator {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new MsgDisplayQualityIndicator()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgDisplayQualityIndicator.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export enum MsgDisplay {
  MsgDisplayMode = 101,
  MsgDisplayMark = 102,
  MsgDisplayReset = 103,
  MsgDisplayCopyBits = 104,
  MsgDisplayInvalList = 105,
  MsgDisplayInvalAllPixmaps = 106,
  MsgDisplayInvalPalette = 107,
  MsgDisplayInvalAllPalettes = 108,
  MsgDisplayStreamCreate = 122,
  MsgDisplayStreamData = 123,
  MsgDisplayStreamClip = 124,
  MsgDisplayStreamDestroy = 125,
  MsgDisplayStreamDestroyAll = 126,
  MsgDisplayDrawFill = 302,
  MsgDisplayDrawOpaque = 303,
  MsgDisplayDrawCopy = 304,
  MsgDisplayDrawBlend = 305,
  MsgDisplayDrawBlackness = 306,
  MsgDisplayDrawWhiteness = 307,
  MsgDisplayDrawInvers = 308,
  MsgDisplayDrawRop3 = 309,
  MsgDisplayDrawStroke = 310,
  MsgDisplayDrawText = 311,
  MsgDisplayDrawTransparent = 312,
  MsgDisplayDrawAlphaBlend = 313,
  MsgDisplaySurfaceCreate = 314,
  MsgDisplaySurfaceDestroy = 315,
  MsgDisplayStreamDataSized = 316,
  MsgDisplayMonitorsConfig = 317,
  MsgDisplayDrawComposite = 318,
  MsgDisplayStreamActivateReport = 319,
  MsgDisplayGlScanoutUnix = 320,
  MsgDisplayGlDraw = 321,
  MsgDisplayQualityIndicator = 322,
}
export class MsgcDisplayInit {
  pixmapCacheId: number
  pixmapCacheSize: bigint
  glzDictionaryId: number
  glzDictionaryWindowSize: number

  static messageSize = 14

  constructor(pixmapCacheId: number, pixmapCacheSize: bigint, glzDictionaryId: number, glzDictionaryWindowSize: number) {
    this.pixmapCacheId = pixmapCacheId
    this.pixmapCacheSize = pixmapCacheSize
    this.glzDictionaryId = glzDictionaryId
    this.glzDictionaryWindowSize = glzDictionaryWindowSize
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgcDisplayInit {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const pixmapCacheId = view.getUint8(offset)
    offset += 1
    const pixmapCacheSize = view.getBigInt64(offset, true)
    offset += 8
    const glzDictionaryId = view.getUint8(offset)
    offset += 1
    const glzDictionaryWindowSize = view.getInt32(offset, true)
    offset += 4
    return new MsgcDisplayInit(pixmapCacheId, pixmapCacheSize, glzDictionaryId, glzDictionaryWindowSize)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgcDisplayInit.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint8(offset, this.pixmapCacheId)
    offset += 1
    view.setBigInt64(offset, this.pixmapCacheSize, true)
    offset += 8
    view.setUint8(offset, this.glzDictionaryId)
    offset += 1
    view.setInt32(offset, this.glzDictionaryWindowSize, true)
    offset += 4

    return buf
  }
}
export class MsgcDisplayStreamReport {
  streamId: StreamIdT
  uniqueId: number
  startFrameMmTime: number
  endFrameMmTime: number
  numFrames: number
  numDrops: number
  lastFrameDelay: number
  audioDelay: number

  static messageSize = 32

  constructor(streamId: StreamIdT, uniqueId: number, startFrameMmTime: number, endFrameMmTime: number, numFrames: number, numDrops: number, lastFrameDelay: number, audioDelay: number) {
    this.streamId = streamId
    this.uniqueId = uniqueId
    this.startFrameMmTime = startFrameMmTime
    this.endFrameMmTime = endFrameMmTime
    this.numFrames = numFrames
    this.numDrops = numDrops
    this.lastFrameDelay = lastFrameDelay
    this.audioDelay = audioDelay
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgcDisplayStreamReport {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const streamId = StreamIdT.unmarshal(buf, offset)
    offset += StreamIdT.messageSize
    const uniqueId = view.getUint32(offset, true)
    offset += 4
    const startFrameMmTime = view.getUint32(offset, true)
    offset += 4
    const endFrameMmTime = view.getUint32(offset, true)
    offset += 4
    const numFrames = view.getUint32(offset, true)
    offset += 4
    const numDrops = view.getUint32(offset, true)
    offset += 4
    const lastFrameDelay = view.getInt32(offset, true)
    offset += 4
    const audioDelay = view.getUint32(offset, true)
    offset += 4
    return new MsgcDisplayStreamReport(streamId, uniqueId, startFrameMmTime, endFrameMmTime, numFrames, numDrops, lastFrameDelay, audioDelay)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgcDisplayStreamReport.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    slice.set(new Uint8Array(this.streamId.marshal()), offset)
    offset += StreamIdT.messageSize
    view.setUint32(offset, this.uniqueId, true)
    offset += 4
    view.setUint32(offset, this.startFrameMmTime, true)
    offset += 4
    view.setUint32(offset, this.endFrameMmTime, true)
    offset += 4
    view.setUint32(offset, this.numFrames, true)
    offset += 4
    view.setUint32(offset, this.numDrops, true)
    offset += 4
    view.setInt32(offset, this.lastFrameDelay, true)
    offset += 4
    view.setUint32(offset, this.audioDelay, true)
    offset += 4

    return buf
  }
}
export class MsgcDisplayPreferredCompression {
  imageCompression: ImageCompression

  static messageSize = 1

  constructor(imageCompression: ImageCompression) {
    this.imageCompression = imageCompression
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgcDisplayPreferredCompression {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const imageCompression = view.getUint8(offset)
    offset += 1
    return new MsgcDisplayPreferredCompression(imageCompression)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgcDisplayPreferredCompression.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint8(offset, this.imageCompression)
    offset += 1

    return buf
  }
}
export class MsgcDisplayGlDrawDone {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgcDisplayGlDrawDone {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new MsgcDisplayGlDrawDone()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgcDisplayGlDrawDone.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export class MsgcDisplayPreferredVideoCodecType {
  numOfCodecs: number
  codecs: VideoCodecType[]

  get messageSize() {
    let size = 0
    size += 1
    size += 1 * this.numOfCodecs
    return size
  }

  constructor(numOfCodecs: number, codecs: VideoCodecType[]) {
    this.numOfCodecs = numOfCodecs
    this.codecs = codecs
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): MsgcDisplayPreferredVideoCodecType {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const numOfCodecs = view.getUint8(offset)
    offset += 1
    let codecs: VideoCodecType[] = []
    for (let i = 0; i < numOfCodecs; i++) {
    const videoCodecType = view.getUint8(offset)
    offset += 1
    codecs.push(videoCodecType)
    }
    return new MsgcDisplayPreferredVideoCodecType(numOfCodecs, codecs)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint8(offset, this.numOfCodecs)
    offset += 1
    this.codecs.forEach((videoCodecType) => {
    view.setUint8(offset, videoCodecType)
    offset += 1
    })

    return buf
  }
}
export enum MsgcDisplay {
  MsgcDisplayInit = 101,
  MsgcDisplayStreamReport = 102,
  MsgcDisplayPreferredCompression = 103,
  MsgcDisplayGlDrawDone = 104,
  MsgcDisplayPreferredVideoCodecType = 105,
}
export enum KeyboardModifierFlags {
  Empty,
  KeyboardModifierFlagsScrollLock = 1 << 0,
  KeyboardModifierFlagsNumLock = 1 << 1,
  KeyboardModifierFlagsCapsLock = 1 << 2,
}
export enum MouseButton {
  MouseButtonInvalid = 0,
  MouseButtonLeft = 1,
  MouseButtonMiddle = 2,
  MouseButtonRight = 3,
  MouseButtonUp = 4,
  MouseButtonDown = 5,
}
export enum MouseButtonMask {
  Empty,
  MouseButtonMaskLeft = 1 << 0,
  MouseButtonMaskMiddle = 1 << 1,
  MouseButtonMaskRight = 1 << 2,
}
export class MsgcInputsKeyDown {
  code: number

  static messageSize = 4

  constructor(code: number) {
    this.code = code
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgcInputsKeyDown {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const code = view.getUint32(offset, true)
    offset += 4
    return new MsgcInputsKeyDown(code)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgcInputsKeyDown.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.code, true)
    offset += 4

    return buf
  }
}
export class MsgcInputsKeyUp {
  code: number

  static messageSize = 4

  constructor(code: number) {
    this.code = code
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgcInputsKeyUp {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const code = view.getUint32(offset, true)
    offset += 4
    return new MsgcInputsKeyUp(code)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgcInputsKeyUp.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.code, true)
    offset += 4

    return buf
  }
}
export class MsgcInputsKeyModifiers {
  modifiers: KeyboardModifierFlags

  static messageSize = 2

  constructor(modifiers: KeyboardModifierFlags) {
    this.modifiers = modifiers
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgcInputsKeyModifiers {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const modifiers = view.getUint16(offset, true)
    offset += 2
    return new MsgcInputsKeyModifiers(modifiers)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgcInputsKeyModifiers.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint16(offset, this.modifiers, true)
    offset += 2

    return buf
  }
}
export class MsgcInputsKeyScancode {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgcInputsKeyScancode {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new MsgcInputsKeyScancode()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgcInputsKeyScancode.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export class MsgcInputsMouseMotion {
  dx: number
  dy: number
  buttonsState: MouseButtonMask

  static messageSize = 10

  constructor(dx: number, dy: number, buttonsState: MouseButtonMask) {
    this.dx = dx
    this.dy = dy
    this.buttonsState = buttonsState
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgcInputsMouseMotion {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const dx = view.getInt32(offset, true)
    offset += 4
    const dy = view.getInt32(offset, true)
    offset += 4
    const buttonsState = view.getUint16(offset, true)
    offset += 2
    return new MsgcInputsMouseMotion(dx, dy, buttonsState)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgcInputsMouseMotion.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setInt32(offset, this.dx, true)
    offset += 4
    view.setInt32(offset, this.dy, true)
    offset += 4
    view.setUint16(offset, this.buttonsState, true)
    offset += 2

    return buf
  }
}
export class MsgcInputsMousePosition {
  x: number
  y: number
  buttonsState: MouseButtonMask
  displayId: number

  static messageSize = 11

  constructor(x: number, y: number, buttonsState: MouseButtonMask, displayId: number) {
    this.x = x
    this.y = y
    this.buttonsState = buttonsState
    this.displayId = displayId
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgcInputsMousePosition {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const x = view.getUint32(offset, true)
    offset += 4
    const y = view.getUint32(offset, true)
    offset += 4
    const buttonsState = view.getUint16(offset, true)
    offset += 2
    const displayId = view.getUint8(offset)
    offset += 1
    return new MsgcInputsMousePosition(x, y, buttonsState, displayId)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgcInputsMousePosition.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.x, true)
    offset += 4
    view.setUint32(offset, this.y, true)
    offset += 4
    view.setUint16(offset, this.buttonsState, true)
    offset += 2
    view.setUint8(offset, this.displayId)
    offset += 1

    return buf
  }
}
export class MsgcInputsMousePress {
  button: MouseButton
  buttonsState: MouseButtonMask

  static messageSize = 3

  constructor(button: MouseButton, buttonsState: MouseButtonMask) {
    this.button = button
    this.buttonsState = buttonsState
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgcInputsMousePress {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const button = view.getUint8(offset)
    offset += 1
    const buttonsState = view.getUint16(offset, true)
    offset += 2
    return new MsgcInputsMousePress(button, buttonsState)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgcInputsMousePress.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint8(offset, this.button)
    offset += 1
    view.setUint16(offset, this.buttonsState, true)
    offset += 2

    return buf
  }
}
export class MsgcInputsMouseRelease {
  button: MouseButton
  buttonsState: MouseButtonMask

  static messageSize = 3

  constructor(button: MouseButton, buttonsState: MouseButtonMask) {
    this.button = button
    this.buttonsState = buttonsState
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgcInputsMouseRelease {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const button = view.getUint8(offset)
    offset += 1
    const buttonsState = view.getUint16(offset, true)
    offset += 2
    return new MsgcInputsMouseRelease(button, buttonsState)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgcInputsMouseRelease.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint8(offset, this.button)
    offset += 1
    view.setUint16(offset, this.buttonsState, true)
    offset += 2

    return buf
  }
}
export enum MsgcInputs {
  MsgcInputsKeyDown = 101,
  MsgcInputsKeyUp = 102,
  MsgcInputsKeyModifiers = 103,
  MsgcInputsKeyScancode = 104,
  MsgcInputsMouseMotion = 111,
  MsgcInputsMousePosition = 112,
  MsgcInputsMousePress = 113,
  MsgcInputsMouseRelease = 114,
}
export class MsgInputsInit {
  keyboardModifiers: KeyboardModifierFlags

  static messageSize = 2

  constructor(keyboardModifiers: KeyboardModifierFlags) {
    this.keyboardModifiers = keyboardModifiers
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgInputsInit {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const keyboardModifiers = view.getUint16(offset, true)
    offset += 2
    return new MsgInputsInit(keyboardModifiers)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgInputsInit.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint16(offset, this.keyboardModifiers, true)
    offset += 2

    return buf
  }
}
export class MsgInputsKeyModifiers {
  modifiers: KeyboardModifierFlags

  static messageSize = 2

  constructor(modifiers: KeyboardModifierFlags) {
    this.modifiers = modifiers
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgInputsKeyModifiers {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const modifiers = view.getUint16(offset, true)
    offset += 2
    return new MsgInputsKeyModifiers(modifiers)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgInputsKeyModifiers.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint16(offset, this.modifiers, true)
    offset += 2

    return buf
  }
}
export class MsgInputsMouseMotionAck {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgInputsMouseMotionAck {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new MsgInputsMouseMotionAck()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgInputsMouseMotionAck.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export enum MsgInputs {
  MsgInputsInit = 101,
  MsgInputsKeyModifiers = 102,
  MsgInputsMouseMotionAck = 111,
}
export enum CursorType {
  CursorTypeAlpha = 0,
  CursorTypeMono = 1,
  CursorTypeColor4 = 2,
  CursorTypeColor8 = 3,
  CursorTypeColor16 = 4,
  CursorTypeColor24 = 5,
  CursorTypeColor32 = 6,
}
export enum CursorFlags {
  Empty,
  CursorFlagsNone = 1 << 0,
  CursorFlagsCacheMe = 1 << 1,
  CursorFlagsFromCache = 1 << 2,
}
export class CursorHeader {
  unique: bigint
  type: CursorType
  width: number
  height: number
  hotSpotX: number
  hotSpotY: number

  static messageSize = 17

  constructor(unique: bigint, type: CursorType, width: number, height: number, hotSpotX: number, hotSpotY: number) {
    this.unique = unique
    this.type = type
    this.width = width
    this.height = height
    this.hotSpotX = hotSpotX
    this.hotSpotY = hotSpotY
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): CursorHeader {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const unique = view.getBigUint64(offset, true)
    offset += 8
    const type = view.getUint8(offset)
    offset += 1
    const width = view.getUint16(offset, true)
    offset += 2
    const height = view.getUint16(offset, true)
    offset += 2
    const hotSpotX = view.getUint16(offset, true)
    offset += 2
    const hotSpotY = view.getUint16(offset, true)
    offset += 2
    return new CursorHeader(unique, type, width, height, hotSpotX, hotSpotY)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(CursorHeader.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setBigUint64(offset, this.unique, true)
    offset += 8
    view.setUint8(offset, this.type)
    offset += 1
    view.setUint16(offset, this.width, true)
    offset += 2
    view.setUint16(offset, this.height, true)
    offset += 2
    view.setUint16(offset, this.hotSpotX, true)
    offset += 2
    view.setUint16(offset, this.hotSpotY, true)
    offset += 2

    return buf
  }
}
export class Cursor {
  flags: CursorFlags
  header: CursorHeader | undefined
  data: ArrayBuffer | undefined

  get messageSize() {
    let size = 0
    size += 2
    switch (this.flags) {
      case CursorFlags.CursorFlagsNone:
        size += 17
        break
    }
    return size
  }

  constructor(flags: CursorFlags, header: CursorHeader | undefined, data: ArrayBuffer | undefined) {
    this.flags = flags
    this.header = header
    this.data = data
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): Cursor {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const flags = view.getUint16(offset, true)
    offset += 2
    let header
    switch (flags) {
      case CursorFlags.CursorFlagsNone:
    const header = CursorHeader.unmarshal(buf, offset)
    offset += CursorHeader.messageSize
        break
    }
    const data = buf.slice(origOffset)
    return new Cursor(flags, header, data)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint16(offset, this.flags, true)
    offset += 2
    switch (this.flags) {
      case CursorFlags.CursorFlagsNone:
    slice.set(new Uint8Array(this.header!.marshal()), offset)
    offset += CursorHeader.messageSize
        break
    }

    return buf
  }
}
export class MsgCursorInit {
  position: Point16
  trailLength: number
  trailFrequency: number
  visible: number
  cursor: Cursor

  get messageSize() {
    let size = 0
    size += 4
    size += 2
    size += 2
    size += 1
    size += this.cursor.messageSize
    return size
  }

  constructor(position: Point16, trailLength: number, trailFrequency: number, visible: number, cursor: Cursor) {
    this.position = position
    this.trailLength = trailLength
    this.trailFrequency = trailFrequency
    this.visible = visible
    this.cursor = cursor
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): MsgCursorInit {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const position = Point16.unmarshal(buf, offset)
    offset += Point16.messageSize
    const trailLength = view.getUint16(offset, true)
    offset += 2
    const trailFrequency = view.getUint16(offset, true)
    offset += 2
    const visible = view.getUint8(offset)
    offset += 1
    const cursor = Cursor.unmarshal(buf, offset)
    offset += cursor.messageSize
    return new MsgCursorInit(position, trailLength, trailFrequency, visible, cursor)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    slice.set(new Uint8Array(this.position.marshal()), offset)
    offset += Point16.messageSize
    view.setUint16(offset, this.trailLength, true)
    offset += 2
    view.setUint16(offset, this.trailFrequency, true)
    offset += 2
    view.setUint8(offset, this.visible)
    offset += 1
    slice.set(new Uint8Array(this.cursor.marshal()), offset)
    offset += this.cursor.messageSize

    return buf
  }
}
export class MsgCursorReset {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgCursorReset {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new MsgCursorReset()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgCursorReset.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export class MsgCursorSet {
  position: Point16
  visible: number
  cursor: Cursor

  get messageSize() {
    let size = 0
    size += 4
    size += 1
    size += this.cursor.messageSize
    return size
  }

  constructor(position: Point16, visible: number, cursor: Cursor) {
    this.position = position
    this.visible = visible
    this.cursor = cursor
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): MsgCursorSet {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const position = Point16.unmarshal(buf, offset)
    offset += Point16.messageSize
    const visible = view.getUint8(offset)
    offset += 1
    const cursor = Cursor.unmarshal(buf, offset)
    offset += cursor.messageSize
    return new MsgCursorSet(position, visible, cursor)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    slice.set(new Uint8Array(this.position.marshal()), offset)
    offset += Point16.messageSize
    view.setUint8(offset, this.visible)
    offset += 1
    slice.set(new Uint8Array(this.cursor.marshal()), offset)
    offset += this.cursor.messageSize

    return buf
  }
}
export class MsgCursorMove {
  position: Point16

  static messageSize = 4

  constructor(position: Point16) {
    this.position = position
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgCursorMove {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const position = Point16.unmarshal(buf, offset)
    offset += Point16.messageSize
    return new MsgCursorMove(position)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgCursorMove.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    slice.set(new Uint8Array(this.position.marshal()), offset)
    offset += Point16.messageSize

    return buf
  }
}
export class MsgCursorHide {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgCursorHide {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new MsgCursorHide()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgCursorHide.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export class MsgCursorTrail {
  length: number
  frequency: number

  static messageSize = 4

  constructor(length: number, frequency: number) {
    this.length = length
    this.frequency = frequency
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgCursorTrail {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const length = view.getUint16(offset, true)
    offset += 2
    const frequency = view.getUint16(offset, true)
    offset += 2
    return new MsgCursorTrail(length, frequency)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgCursorTrail.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint16(offset, this.length, true)
    offset += 2
    view.setUint16(offset, this.frequency, true)
    offset += 2

    return buf
  }
}
export class MsgCursorInvalOne {
  id: bigint

  static messageSize = 8

  constructor(id: bigint) {
    this.id = id
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgCursorInvalOne {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const id = view.getBigUint64(offset, true)
    offset += 8
    return new MsgCursorInvalOne(id)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgCursorInvalOne.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setBigUint64(offset, this.id, true)
    offset += 8

    return buf
  }
}
export class MsgCursorInvalAll {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgCursorInvalAll {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new MsgCursorInvalAll()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgCursorInvalAll.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export enum MsgCursor {
  MsgCursorInit = 101,
  MsgCursorReset = 102,
  MsgCursorSet = 103,
  MsgCursorMove = 104,
  MsgCursorHide = 105,
  MsgCursorTrail = 106,
  MsgCursorInvalOne = 107,
  MsgCursorInvalAll = 108,
}
export enum AudioDataMode {
  AudioDataModeInvalid = 0,
  AudioDataModeRaw = 1,
  /**
   * @deprecated AudioDataModeCelt051 is deprecated and kept for backwards compatibility
   */
  AudioDataModeCelt051 = 2,
  AudioDataModeOpus = 3,
}
export enum AudioFmt {
  AudioFmtInvalid = 0,
  AudioFmtS16 = 1,
}
export class AudioVolume {
  nchannels: number
  volume: number[]

  get messageSize() {
    let size = 0
    size += 1
    size += 2 * this.nchannels
    return size
  }

  constructor(nchannels: number, volume: number[]) {
    this.nchannels = nchannels
    this.volume = volume
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): AudioVolume {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const nchannels = view.getUint8(offset)
    offset += 1
    let volume: number[] = []
    for (let i = 0; i < nchannels; i++) {
    const uint16 = view.getUint16(offset, true)
    offset += 2
    volume.push(uint16)
    }
    return new AudioVolume(nchannels, volume)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint8(offset, this.nchannels)
    offset += 1
    this.volume.forEach((uint16) => {
    view.setUint16(offset, uint16, true)
    offset += 2
    })

    return buf
  }
}
export class AudioMute {
  mute: number

  static messageSize = 1

  constructor(mute: number) {
    this.mute = mute
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): AudioMute {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const mute = view.getUint8(offset)
    offset += 1
    return new AudioMute(mute)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(AudioMute.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint8(offset, this.mute)
    offset += 1

    return buf
  }
}
export class MsgPlaybackData {
  time: number
  data: ArrayBuffer | undefined

  get messageSize() {
    let size = 0
    size += 4
    return size
  }

  constructor(time: number, data: ArrayBuffer | undefined) {
    this.time = time
    this.data = data
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): MsgPlaybackData {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const time = view.getUint32(offset, true)
    offset += 4
    const data = buf.slice(origOffset)
    return new MsgPlaybackData(time, data)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.time, true)
    offset += 4

    return buf
  }
}
export class MsgPlaybackMode {
  time: number
  mode: AudioDataMode
  data: ArrayBuffer | undefined

  get messageSize() {
    let size = 0
    size += 4
    size += 2
    return size
  }

  constructor(time: number, mode: AudioDataMode, data: ArrayBuffer | undefined) {
    this.time = time
    this.mode = mode
    this.data = data
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): MsgPlaybackMode {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const time = view.getUint32(offset, true)
    offset += 4
    const mode = view.getUint16(offset, true)
    offset += 2
    const data = buf.slice(origOffset)
    return new MsgPlaybackMode(time, mode, data)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.time, true)
    offset += 4
    view.setUint16(offset, this.mode, true)
    offset += 2

    return buf
  }
}
export class MsgPlaybackStart {
  channels: number
  format: AudioFmt
  frequency: number
  time: number

  static messageSize = 14

  constructor(channels: number, format: AudioFmt, frequency: number, time: number) {
    this.channels = channels
    this.format = format
    this.frequency = frequency
    this.time = time
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgPlaybackStart {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const channels = view.getUint32(offset, true)
    offset += 4
    const format = view.getUint16(offset, true)
    offset += 2
    const frequency = view.getUint32(offset, true)
    offset += 4
    const time = view.getUint32(offset, true)
    offset += 4
    return new MsgPlaybackStart(channels, format, frequency, time)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgPlaybackStart.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.channels, true)
    offset += 4
    view.setUint16(offset, this.format, true)
    offset += 2
    view.setUint32(offset, this.frequency, true)
    offset += 4
    view.setUint32(offset, this.time, true)
    offset += 4

    return buf
  }
}
export class MsgPlaybackStop {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgPlaybackStop {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new MsgPlaybackStop()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgPlaybackStop.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export class MsgPlaybackVolume {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgPlaybackVolume {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new MsgPlaybackVolume()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgPlaybackVolume.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export class MsgPlaybackMute {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgPlaybackMute {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new MsgPlaybackMute()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgPlaybackMute.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export class MsgPlaybackLatency {
  latencyMs: number

  static messageSize = 4

  constructor(latencyMs: number) {
    this.latencyMs = latencyMs
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgPlaybackLatency {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const latencyMs = view.getUint32(offset, true)
    offset += 4
    return new MsgPlaybackLatency(latencyMs)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgPlaybackLatency.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.latencyMs, true)
    offset += 4

    return buf
  }
}
export enum MsgPlayback {
  MsgPlaybackData = 101,
  MsgPlaybackMode = 102,
  MsgPlaybackStart = 103,
  MsgPlaybackStop = 104,
  MsgPlaybackVolume = 105,
  MsgPlaybackMute = 106,
  MsgPlaybackLatency = 107,
}
export class MsgRecordStart {
  channels: number
  format: AudioFmt
  frequency: number

  static messageSize = 10

  constructor(channels: number, format: AudioFmt, frequency: number) {
    this.channels = channels
    this.format = format
    this.frequency = frequency
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgRecordStart {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const channels = view.getUint32(offset, true)
    offset += 4
    const format = view.getUint16(offset, true)
    offset += 2
    const frequency = view.getUint32(offset, true)
    offset += 4
    return new MsgRecordStart(channels, format, frequency)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgRecordStart.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.channels, true)
    offset += 4
    view.setUint16(offset, this.format, true)
    offset += 2
    view.setUint32(offset, this.frequency, true)
    offset += 4

    return buf
  }
}
export class MsgRecordStop {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgRecordStop {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new MsgRecordStop()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgRecordStop.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export class MsgRecordVolume {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgRecordVolume {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new MsgRecordVolume()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgRecordVolume.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export class MsgRecordMute {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgRecordMute {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new MsgRecordMute()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgRecordMute.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export enum MsgRecord {
  MsgRecordStart = 101,
  MsgRecordStop = 102,
  MsgRecordVolume = 103,
  MsgRecordMute = 104,
}
export class MsgcRecordData {
  time: number
  data: ArrayBuffer | undefined

  get messageSize() {
    let size = 0
    size += 4
    return size
  }

  constructor(time: number, data: ArrayBuffer | undefined) {
    this.time = time
    this.data = data
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): MsgcRecordData {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const time = view.getUint32(offset, true)
    offset += 4
    const data = buf.slice(origOffset)
    return new MsgcRecordData(time, data)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.time, true)
    offset += 4

    return buf
  }
}
export class MsgcRecordMode {
  time: number
  mode: AudioDataMode
  data: ArrayBuffer | undefined

  get messageSize() {
    let size = 0
    size += 4
    size += 2
    return size
  }

  constructor(time: number, mode: AudioDataMode, data: ArrayBuffer | undefined) {
    this.time = time
    this.mode = mode
    this.data = data
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): MsgcRecordMode {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const time = view.getUint32(offset, true)
    offset += 4
    const mode = view.getUint16(offset, true)
    offset += 2
    const data = buf.slice(origOffset)
    return new MsgcRecordMode(time, mode, data)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.time, true)
    offset += 4
    view.setUint16(offset, this.mode, true)
    offset += 2

    return buf
  }
}
export class MsgcRecordStartMark {
  time: number

  static messageSize = 4

  constructor(time: number) {
    this.time = time
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgcRecordStartMark {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const time = view.getUint32(offset, true)
    offset += 4
    return new MsgcRecordStartMark(time)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgcRecordStartMark.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.time, true)
    offset += 4

    return buf
  }
}
export enum MsgcRecord {
  MsgcRecordData = 101,
  MsgcRecordMode = 102,
  MsgcRecordStartMark = 103,
}
export enum VscMessageType {
  VscMessageTypeInit = 1,
  VscMessageTypeError = 2,
  VscMessageTypeReaderadd = 3,
  VscMessageTypeReaderremove = 4,
  VscMessageTypeAtr = 5,
  VscMessageTypeCardremove = 6,
  VscMessageTypeApdu = 7,
  VscMessageTypeFlush = 8,
  VscMessageTypeFlushcomplete = 9,
}
export class VscMessageHeader {
  type: VscMessageType
  readerId: number
  length: number

  static messageSize = 12

  constructor(type: VscMessageType, readerId: number, length: number) {
    this.type = type
    this.readerId = readerId
    this.length = length
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): VscMessageHeader {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const type = view.getUint32(offset, true)
    offset += 4
    const readerId = view.getUint32(offset, true)
    offset += 4
    const length = view.getUint32(offset, true)
    offset += 4
    return new VscMessageHeader(type, readerId, length)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(VscMessageHeader.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.type, true)
    offset += 4
    view.setUint32(offset, this.readerId, true)
    offset += 4
    view.setUint32(offset, this.length, true)
    offset += 4

    return buf
  }
}
export class VscMessageError {
  code: number

  static messageSize = 4

  constructor(code: number) {
    this.code = code
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): VscMessageError {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const code = view.getUint32(offset, true)
    offset += 4
    return new VscMessageError(code)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(VscMessageError.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.code, true)
    offset += 4

    return buf
  }
}
export class VscMessageAPDU {
  data: number[]

  constructor(data: number[]) {
    this.data = data
  }

  static unmarshal(buf: ArrayBuffer, expectedSize: number, offset?: number): VscMessageAPDU {
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    let data: number[] = []
    for (let i = 0; i < expectedSize - offset; i++) {
    const uint8 = view.getUint8(offset)
    offset += 1
    data.push(uint8)
    }
    return new VscMessageAPDU(data)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(0) // TODO: This should be the ininite data size
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export class VscMessageATR {
  atr: number[]

  constructor(atr: number[]) {
    this.atr = atr
  }

  static unmarshal(buf: ArrayBuffer, expectedSize: number, offset?: number): VscMessageATR {
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    let atr: number[] = []
    for (let i = 0; i < expectedSize - offset; i++) {
    const uint8 = view.getUint8(offset)
    offset += 1
    atr.push(uint8)
    }
    return new VscMessageATR(atr)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(0) // TODO: This should be the ininite data size
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export class VscMessageReaderAdd {
  name: number[] | undefined

  static messageSize = 4

  constructor(name: number[] | undefined) {
    this.name = name
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): VscMessageReaderAdd {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const namePtr = view.getUint32(offset, true)
    offset += 4
    let name: number | undefined = undefined
    if (namePtr !== 0) {
        name = number.unmarshal(buf, Number(namePtr))
    }
    return new VscMessageReaderAdd(name)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(VscMessageReaderAdd.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, 0, true) // TODO: This!
    offset += 8

    return buf
  }
}
export class MsgSmartcardData {
  type: VscMessageType
  readerId: number
  length: number
  data: number[]

  get messageSize() {
    let size = 0
    size += 4
    size += 4
    size += 4
    size += 1 * this.length
    return size
  }

  constructor(type: VscMessageType, readerId: number, length: number, data: number[]) {
    this.type = type
    this.readerId = readerId
    this.length = length
    this.data = data
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): MsgSmartcardData {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const type = view.getUint32(offset, true)
    offset += 4
    const readerId = view.getUint32(offset, true)
    offset += 4
    const length = view.getUint32(offset, true)
    offset += 4
    let data: number[] = []
    for (let i = 0; i < length; i++) {
    const uint8 = view.getUint8(offset)
    offset += 1
    data.push(uint8)
    }
    return new MsgSmartcardData(type, readerId, length, data)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.type, true)
    offset += 4
    view.setUint32(offset, this.readerId, true)
    offset += 4
    view.setUint32(offset, this.length, true)
    offset += 4
    this.data.forEach((uint8) => {
    view.setUint8(offset, uint8)
    offset += 1
    })

    return buf
  }
}
export enum MsgSmartcard {
  MsgSmartcardData = 101,
}
export class MsgcSmartcardData {
  type: VscMessageType
  readerId: number
  length: number
  data: number[]

  get messageSize() {
    let size = 0
    size += 4
    size += 4
    size += 4
    size += 1 * this.length
    return size
  }

  constructor(type: VscMessageType, readerId: number, length: number, data: number[]) {
    this.type = type
    this.readerId = readerId
    this.length = length
    this.data = data
  }

  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): MsgcSmartcardData {
    const expectedSize = expSize
    if (expectedSize !== undefined && buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const type = view.getUint32(offset, true)
    offset += 4
    const readerId = view.getUint32(offset, true)
    offset += 4
    const length = view.getUint32(offset, true)
    offset += 4
    let data: number[] = []
    for (let i = 0; i < length; i++) {
    const uint8 = view.getUint8(offset)
    offset += 1
    data.push(uint8)
    }
    return new MsgcSmartcardData(type, readerId, length, data)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(this.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.type, true)
    offset += 4
    view.setUint32(offset, this.readerId, true)
    offset += 4
    view.setUint32(offset, this.length, true)
    offset += 4
    this.data.forEach((uint8) => {
    view.setUint8(offset, uint8)
    offset += 1
    })

    return buf
  }
}
export enum MsgcSmartcard {
  MsgcSmartcardData = 101,
}
export class MsgSpicevmcData {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgSpicevmcData {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new MsgSpicevmcData()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgSpicevmcData.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export class MsgSpicevmcCompressedData {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgSpicevmcCompressedData {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new MsgSpicevmcCompressedData()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgSpicevmcCompressedData.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export enum MsgSpicevmc {
  MsgSpicevmcData = 101,
  MsgSpicevmcCompressedData = 102,
}
export class MsgcSpicevmcData {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgcSpicevmcData {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new MsgcSpicevmcData()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgcSpicevmcData.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export class MsgcSpicevmcCompressedData {

  static messageSize = 0

  constructor() {
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgcSpicevmcCompressedData {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    return new MsgcSpicevmcCompressedData()
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgcSpicevmcCompressedData.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0


    return buf
  }
}
export enum MsgcSpicevmc {
  MsgcSpicevmcData = 101,
  MsgcSpicevmcCompressedData = 102,
}
export class MsgcPortEvent {
  event: number

  static messageSize = 1

  constructor(event: number) {
    this.event = event
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgcPortEvent {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const event = view.getUint8(offset)
    offset += 1
    return new MsgcPortEvent(event)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgcPortEvent.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint8(offset, this.event)
    offset += 1

    return buf
  }
}
export enum MsgcPort {
  MsgcPortEvent = 201,
}
export class MsgPortInit {
  nameSize: number
  name: number[] | undefined
  opened: number

  static messageSize = 9

  constructor(nameSize: number, name: number[] | undefined, opened: number) {
    this.nameSize = nameSize
    this.name = name
    this.opened = opened
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgPortInit {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const nameSize = view.getUint32(offset, true)
    offset += 4
    const namePtr = view.getUint32(offset, true)
    offset += 4
    let name: number | undefined = undefined
    if (namePtr !== 0) {
        name = number.unmarshal(buf, Number(namePtr))
    }
    const opened = view.getUint8(offset)
    offset += 1
    return new MsgPortInit(nameSize, name, opened)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgPortInit.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.nameSize, true)
    offset += 4
    view.setUint32(offset, 0, true) // TODO: This!
    offset += 8
    view.setUint8(offset, this.opened)
    offset += 1

    return buf
  }
}
export class MsgPortEvent {
  event: number

  static messageSize = 1

  constructor(event: number) {
    this.event = event
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): MsgPortEvent {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const event = view.getUint8(offset)
    offset += 1
    return new MsgPortEvent(event)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(MsgPortEvent.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint8(offset, this.event)
    offset += 1

    return buf
  }
}
export enum MsgPort {
  MsgPortInit = 201,
  MsgPortEvent = 202,
}
export enum SpiceChannel {
  SpiceChannelMain = 1,
  SpiceChannelDisplay = 2,
  SpiceChannelInputs = 3,
  SpiceChannelCursor = 4,
  SpiceChannelPlayback = 5,
  SpiceChannelRecord = 6,
  SpiceChannelTunnel = 7,
  SpiceChannelSmartcard = 8,
  SpiceChannelUsbredir = 9,
  SpiceChannelPort = 10,
  SpiceChannelWebdav = 11,
}
export const SpiceMagic = 1363428690
export const SpiceVersionMajor = 2
export const SpiceVersionMinor = 2
export const SpiceMaxPasswordLength = 60
export const SpiceTicketKeyPairLength = 1024
export const SpiceTicketPubkeyBytes = 162
export const SpiceMaxNumStreams = 64
export class SpiceLinkHeader {
  magic: number
  majorVersion: number
  minorVersion: number
  size: number

  static messageSize = 16

  constructor(magic: number, majorVersion: number, minorVersion: number, size: number) {
    this.magic = magic
    this.majorVersion = majorVersion
    this.minorVersion = minorVersion
    this.size = size
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): SpiceLinkHeader {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const magic = view.getUint32(offset, true)
    offset += 4
    const majorVersion = view.getUint32(offset, true)
    offset += 4
    const minorVersion = view.getUint32(offset, true)
    offset += 4
    const size = view.getUint32(offset, true)
    offset += 4
    return new SpiceLinkHeader(magic, majorVersion, minorVersion, size)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(SpiceLinkHeader.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.magic, true)
    offset += 4
    view.setUint32(offset, this.majorVersion, true)
    offset += 4
    view.setUint32(offset, this.minorVersion, true)
    offset += 4
    view.setUint32(offset, this.size, true)
    offset += 4

    return buf
  }
}
export enum SpiceCommonCap {
  SpiceCommonCapProtocolAuthSelection = 0,
  SpiceCommonCapAuthSpice = 1,
  SpiceCommonCapAuthSasl = 2,
  SpiceCommonCapMiniHeader = 3,
}
export class SpiceLinkMess {
  connectionId: number
  channelType: number
  channelId: number
  numCommonCaps: number
  numChannelCaps: number
  capsOffset: number

  static messageSize = 18

  constructor(connectionId: number, channelType: number, channelId: number, numCommonCaps: number, numChannelCaps: number, capsOffset: number) {
    this.connectionId = connectionId
    this.channelType = channelType
    this.channelId = channelId
    this.numCommonCaps = numCommonCaps
    this.numChannelCaps = numChannelCaps
    this.capsOffset = capsOffset
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): SpiceLinkMess {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const connectionId = view.getUint32(offset, true)
    offset += 4
    const channelType = view.getUint8(offset)
    offset += 1
    const channelId = view.getUint8(offset)
    offset += 1
    const numCommonCaps = view.getUint32(offset, true)
    offset += 4
    const numChannelCaps = view.getUint32(offset, true)
    offset += 4
    const capsOffset = view.getUint32(offset, true)
    offset += 4
    return new SpiceLinkMess(connectionId, channelType, channelId, numCommonCaps, numChannelCaps, capsOffset)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(SpiceLinkMess.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.connectionId, true)
    offset += 4
    view.setUint8(offset, this.channelType)
    offset += 1
    view.setUint8(offset, this.channelId)
    offset += 1
    view.setUint32(offset, this.numCommonCaps, true)
    offset += 4
    view.setUint32(offset, this.numChannelCaps, true)
    offset += 4
    view.setUint32(offset, this.capsOffset, true)
    offset += 4

    return buf
  }
}
export class SpiceLinkReply {
  error: number
  pubKey: number[]
  numCommonCaps: number
  numChannelCaps: number
  capsOffset: number

  static messageSize = 178

  constructor(error: number, pubKey: number[], numCommonCaps: number, numChannelCaps: number, capsOffset: number) {
    this.error = error
    this.pubKey = pubKey
    this.numCommonCaps = numCommonCaps
    this.numChannelCaps = numChannelCaps
    this.capsOffset = capsOffset
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): SpiceLinkReply {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const error = view.getUint32(offset, true)
    offset += 4
    let pubKey: number[] = []
    for (let i = 0; i < 162; i++) {
    const uint8 = view.getUint8(offset)
    offset += 1
    pubKey.push(uint8)
    }
    const numCommonCaps = view.getUint32(offset, true)
    offset += 4
    const numChannelCaps = view.getUint32(offset, true)
    offset += 4
    const capsOffset = view.getUint32(offset, true)
    offset += 4
    return new SpiceLinkReply(error, pubKey, numCommonCaps, numChannelCaps, capsOffset)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(SpiceLinkReply.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.error, true)
    offset += 4
    this.pubKey.forEach((uint8) => {
    view.setUint8(offset, uint8)
    offset += 1
    })
    view.setUint32(offset, this.numCommonCaps, true)
    offset += 4
    view.setUint32(offset, this.numChannelCaps, true)
    offset += 4
    view.setUint32(offset, this.capsOffset, true)
    offset += 4

    return buf
  }
}
export class SpiceLinkEncryptedTicket {
  encryptedData: number[]

  static messageSize = 128

  constructor(encryptedData: number[]) {
    this.encryptedData = encryptedData
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): SpiceLinkEncryptedTicket {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    let encryptedData: number[] = []
    for (let i = 0; i < 128; i++) {
    const uint8 = view.getUint8(offset)
    offset += 1
    encryptedData.push(uint8)
    }
    return new SpiceLinkEncryptedTicket(encryptedData)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(SpiceLinkEncryptedTicket.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    this.encryptedData.forEach((uint8) => {
    view.setUint8(offset, uint8)
    offset += 1
    })

    return buf
  }
}
export class SpiceLinkAuthMechanism {
  authMechanism: number

  static messageSize = 4

  constructor(authMechanism: number) {
    this.authMechanism = authMechanism
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): SpiceLinkAuthMechanism {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const authMechanism = view.getUint32(offset, true)
    offset += 4
    return new SpiceLinkAuthMechanism(authMechanism)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(SpiceLinkAuthMechanism.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.authMechanism, true)
    offset += 4

    return buf
  }
}
export class SpiceDataHeader {
  serial: bigint
  type: number
  size: number
  subList: number

  static messageSize = 18

  constructor(serial: bigint, type: number, size: number, subList: number) {
    this.serial = serial
    this.type = type
    this.size = size
    this.subList = subList
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): SpiceDataHeader {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const serial = view.getBigUint64(offset, true)
    offset += 8
    const type = view.getUint16(offset, true)
    offset += 2
    const size = view.getUint32(offset, true)
    offset += 4
    const subList = view.getUint32(offset, true)
    offset += 4
    return new SpiceDataHeader(serial, type, size, subList)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(SpiceDataHeader.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setBigUint64(offset, this.serial, true)
    offset += 8
    view.setUint16(offset, this.type, true)
    offset += 2
    view.setUint32(offset, this.size, true)
    offset += 4
    view.setUint32(offset, this.subList, true)
    offset += 4

    return buf
  }
}
export class SpiceMiniDataHeader {
  type: number
  size: number

  static messageSize = 6

  constructor(type: number, size: number) {
    this.type = type
    this.size = size
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): SpiceMiniDataHeader {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const type = view.getUint16(offset, true)
    offset += 2
    const size = view.getUint32(offset, true)
    offset += 4
    return new SpiceMiniDataHeader(type, size)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(SpiceMiniDataHeader.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint16(offset, this.type, true)
    offset += 2
    view.setUint32(offset, this.size, true)
    offset += 4

    return buf
  }
}
export class SpiceSubMessage {
  type: number
  size: number

  static messageSize = 6

  constructor(type: number, size: number) {
    this.type = type
    this.size = size
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): SpiceSubMessage {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const type = view.getUint16(offset, true)
    offset += 2
    const size = view.getUint32(offset, true)
    offset += 4
    return new SpiceSubMessage(type, size)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(SpiceSubMessage.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint16(offset, this.type, true)
    offset += 2
    view.setUint32(offset, this.size, true)
    offset += 4

    return buf
  }
}
export class SpiceSubMessageList {
  size: number
  subMessages: number[]

  static messageSize = 2

  constructor(size: number, subMessages: number[]) {
    this.size = size
    this.subMessages = subMessages
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): SpiceSubMessageList {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const size = view.getUint16(offset, true)
    offset += 2
    let subMessages: number[] = []
    for (let i = 0; i < 0; i++) {
    const uint32 = view.getUint32(offset, true)
    offset += 4
    subMessages.push(uint32)
    }
    return new SpiceSubMessageList(size, subMessages)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(SpiceSubMessageList.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint16(offset, this.size, true)
    offset += 2
    this.subMessages.forEach((uint32) => {
    view.setUint32(offset, uint32, true)
    offset += 4
    })

    return buf
  }
}
export const SpiceInputMotionAckBunch = 4
export enum SpicePlaybackCap {
  SpicePlaybackCapCelt051 = 0,
  SpicePlaybackCapVolume = 1,
  SpicePlaybackCapLatency = 2,
  SpicePlaybackCapOpus = 3,
}
export enum SpiceRecordCap {
  SpiceRecordCapCelt051 = 0,
  SpiceRecordCapVolume = 1,
  SpiceRecordCapOpus = 2,
}
export enum SpiceMainCap {
  SpiceMainCapSemiSeamlessMigrate = 0,
  SpiceMainCapNameAndUuid = 1,
  SpiceMainCapAgentConnectedTokens = 2,
  SpiceMainCapSeamlessMigrate = 3,
}
export enum SpiceDisplayCap {
  SpiceDisplayCapSizedStream = 0,
  SpiceDisplayCapMonitorsConfig = 1,
  SpiceDisplayCapComposite = 2,
  SpiceDisplayCapA8Surface = 3,
  SpiceDisplayCapStreamReport = 4,
  SpiceDisplayCapLz4Compression = 5,
  SpiceDisplayCapPrefCompression = 6,
  SpiceDisplayCapGlScanout = 7,
  SpiceDisplayCapMultiCodec = 8,
  SpiceDisplayCapCodecMjpeg = 9,
  SpiceDisplayCapCodecVp8 = 10,
  SpiceDisplayCapCodecH264 = 11,
  SpiceDisplayCapPrefVideoCodecType = 12,
  SpiceDisplayCapCodecVp9 = 13,
  SpiceDisplayCapCodecH265 = 14,
}
export enum SpiceInputsCap {
  SpiceInputsCapKeyScancode = 0,
}
export enum SpiceSpicevmcCap {
  SpiceSpicevmcCapDataCompressLz4 = 0,
}
export enum SpicePortEvent {
  SpicePortEventOpened = 0,
  SpicePortEventClosed = 1,
  SpicePortEventBreak = 2,
}
export const QuicMagic = 1128879441
export enum QuicImageType {
  QuicImageTypeInvalid = 0,
  QuicImageTypeGray = 1,
  QuicImageTypeRgb16 = 2,
  QuicImageTypeRgb24 = 3,
  QuicImageTypeRgb32 = 4,
  QuicImageTypeRgba = 5,
}
export class Quic {
  magic: number
  version: number
  type: QuicImageType
  width: number
  height: number

  static messageSize = 20

  constructor(magic: number, version: number, type: QuicImageType, width: number, height: number) {
    this.magic = magic
    this.version = version
    this.type = type
    this.width = width
    this.height = height
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): Quic {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const magic = view.getUint32(offset, true)
    offset += 4
    const version = view.getUint32(offset, true)
    offset += 4
    const type = view.getUint32(offset, true)
    offset += 4
    const width = view.getUint32(offset, true)
    offset += 4
    const height = view.getUint32(offset, true)
    offset += 4
    return new Quic(magic, version, type, width, height)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(Quic.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.magic, true)
    offset += 4
    view.setUint32(offset, this.version, true)
    offset += 4
    view.setUint32(offset, this.type, true)
    offset += 4
    view.setUint32(offset, this.width, true)
    offset += 4
    view.setUint32(offset, this.height, true)
    offset += 4

    return buf
  }
}
export class QuicImageParams {
  channels: number
  bpc: number

  static messageSize = 8

  constructor(channels: number, bpc: number) {
    this.channels = channels
    this.bpc = bpc
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): QuicImageParams {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const channels = view.getUint32(offset, true)
    offset += 4
    const bpc = view.getUint32(offset, true)
    offset += 4
    return new QuicImageParams(channels, bpc)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(QuicImageParams.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.channels, true)
    offset += 4
    view.setUint32(offset, this.bpc, true)
    offset += 4

    return buf
  }
}
export const LzMagic = 538991180
export enum LzImageType {
  LzImageTypeInvalid = 0,
  LzImageTypePlt1Le = 1,
  LzImageTypePlt1Be = 2,
  LzImageTypePlt4Le = 3,
  LzImageTypePlt4Be = 4,
  LzImageTypePlt8 = 5,
  LzImageTypeRgb16 = 6,
  LzImageTypeRgb24 = 7,
  LzImageTypeRgb32 = 8,
  LzImageTypeRgba = 9,
  LzImageTypeXxxa = 10,
  LzImageTypeA8 = 11,
}
export class Lz {
  magic: number
  version: number
  type: LzImageType
  width: number
  height: number
  stride: number

  static messageSize = 24

  constructor(magic: number, version: number, type: LzImageType, width: number, height: number, stride: number) {
    this.magic = magic
    this.version = version
    this.type = type
    this.width = width
    this.height = height
    this.stride = stride
  }

  static unmarshal(buf: ArrayBuffer, offset?: number): Lz {
    const expectedSize = this.messageSize
    if (buf.byteLength < expectedSize) {
      console.trace()
      throw "not enough queue to be read"
    }

    const view = new DataView(buf)
    if (offset === undefined) {
      offset = 0
    }
    const origOffset = offset

    const magic = view.getUint32(offset, false)
    offset += 4
    const version = view.getUint32(offset, false)
    offset += 4
    const type = view.getUint32(offset, false)
    offset += 4
    const width = view.getUint32(offset, false)
    offset += 4
    const height = view.getUint32(offset, false)
    offset += 4
    const stride = view.getUint32(offset, false)
    offset += 4
    return new Lz(magic, version, type, width, height, stride)
  }

  marshal(): ArrayBuffer {
    const buf = new ArrayBuffer(Lz.messageSize)
    const view = new DataView(buf)
    const slice = new Uint8Array(buf)
    let offset = 0

    view.setUint32(offset, this.magic, false)
    offset += 4
    view.setUint32(offset, this.version, false)
    offset += 4
    view.setUint32(offset, this.type, false)
    offset += 4
    view.setUint32(offset, this.width, false)
    offset += 4
    view.setUint32(offset, this.height, false)
    offset += 4
    view.setUint32(offset, this.stride, false)
    offset += 4

    return buf
  }
}
