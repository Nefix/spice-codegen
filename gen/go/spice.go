package spice

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
)

type Fixed284 int32
type StreamIdT uint32
type Point struct {
	X int32
	Y int32
}
type Point16 struct {
	X int16
	Y int16
}
type PointFix struct {
	X *Fixed284
	Y *Fixed284
}
type Rect struct {
	Top    int32
	Left   int32
	Bottom int32
	Right  int32
}
type Transform struct {
	T00 uint32
	T01 uint32
	T02 uint32
	T10 uint32
	T11 uint32
	T12 uint32
}
type LinkErr uint32

const (
	LinkErrOk LinkErr = iota
	LinkErrError
	LinkErrInvalidMagic
	LinkErrInvalidData
	LinkErrVersionMismatch
	LinkErrNeedSecured
	LinkErrNeedUnsecured
	LinkErrPermissionDenied
	LinkErrBadConnectionId
	LinkErrChannelNotAvailable
)

type WarnCode uint32

const (
	WarnCodeWarnGeneral WarnCode = iota
)

type InfoCode uint32

const (
	InfoCodeInfoGeneral InfoCode = iota
)

type MigrateFlags uint32

const (
	MigrateFlagsNeedFlush MigrateFlags = 1 << iota
	MigrateFlagsNeedDataTransfer
)

type CompositeFlags uint32

const (
	CompositeFlagsOp0 CompositeFlags = 1 << iota
	CompositeFlagsOp1
	CompositeFlagsOp2
	CompositeFlagsOp3
	CompositeFlagsOp4
	CompositeFlagsOp5
	CompositeFlagsOp6
	CompositeFlagsOp7
	CompositeFlagsSrcFilter0
	CompositeFlagsSrcFilter1
	CompositeFlagsSrcFilter2
	CompositeFlagsMaskFilter0
	CompositeFlagsMaskFitler1
	CompositeFlagsMaskFilter2
	CompositeFlagsSrcRepeat0
	CompositeFlagsSrcRepeat1
	CompositeFlagsMaskRepeat0
	CompositeFlagsMaskRepeat1
	CompositeFlagsComponentAlpha
	CompositeFlagsHasMask
	CompositeFlagsHasSrcTransform
	CompositeFlagsHasMaskTransform
	CompositeFlagsSourceOpaque
	CompositeFlagsMaskOpaque
	CompositeFlagsDestOpaque
)

type NotifySeverity uint32

const (
	NotifySeverityInfo NotifySeverity = iota
	NotifySeverityWarn
	NotifySeverityError
)

type NotifyVisibility uint32

const (
	NotifyVisibilityLow NotifyVisibility = iota
	NotifyVisibilityMedium
	NotifyVisibilityHigh
)

type MouseMode uint16

const (
	MouseModeServer MouseMode = 1 << iota
	MouseModeClient
)

type Empty struct{}
type Data struct {
	Data []uint8
}

func UnmarshalData(r io.Reader) (*Data, error) {
	d := &Data{}
	if err := binary.Read(r, binary.LittleEndian, &d.Data); err != nil {
		return nil, fmt.Errorf("unmarshal Data: %w", err)
	}
	return d, nil
}
func (d *Data) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &d.Data); err != nil {
		return nil, fmt.Errorf("marshal Data: %w", err)
	}
	return buf.Bytes(), nil
}

type DataCompressionType uint8

const (
	DataCompressionTypeNone DataCompressionType = iota
	DataCompressionTypeLz4
)

type EmptyStructure struct{}
type CompressedData struct {
	Type           *DataCompressionType
	CompressedData []uint8
}

func UnmarshalCompressedData(r io.Reader) (*CompressedData, error) {
	c := &CompressedData{}
	if err := binary.Read(r, binary.LittleEndian, &c.Type); err != nil {
		return nil, fmt.Errorf("unmarshal Type: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &c.CompressedData); err != nil {
		return nil, fmt.Errorf("unmarshal CompressedData: %w", err)
	}
	return c, nil
}
func (c *CompressedData) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &c.Type); err != nil {
		return nil, fmt.Errorf("marshal Type: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &c.CompressedData); err != nil {
		return nil, fmt.Errorf("marshal CompressedData: %w", err)
	}
	return buf.Bytes(), nil
}

type ChannelWait struct {
	ChannelType   uint8
	ChannelId     uint8
	MessageSerial uint64
}
type MsgBaseMigrate struct {
	Flags *MigrateFlags
}

func UnmarshalMsgBaseMigrate(r io.Reader) (*MsgBaseMigrate, error) {
	m := &MsgBaseMigrate{}
	if err := binary.Read(r, binary.LittleEndian, &m.Flags); err != nil {
		return nil, fmt.Errorf("unmarshal Flags: %w", err)
	}
	return m, nil
}
func (m *MsgBaseMigrate) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.Flags); err != nil {
		return nil, fmt.Errorf("marshal Flags: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgBaseMigrateData struct{}
type MsgBaseSetAck struct {
	Generation uint32
	Window     uint32
}

func UnmarshalMsgBaseSetAck(r io.Reader) (*MsgBaseSetAck, error) {
	m := &MsgBaseSetAck{}
	if err := binary.Read(r, binary.LittleEndian, &m.Generation); err != nil {
		return nil, fmt.Errorf("unmarshal Generation: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Window); err != nil {
		return nil, fmt.Errorf("unmarshal Window: %w", err)
	}
	return m, nil
}
func (m *MsgBaseSetAck) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.Generation); err != nil {
		return nil, fmt.Errorf("marshal Generation: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Window); err != nil {
		return nil, fmt.Errorf("marshal Window: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgBasePing struct {
	Id        uint32
	Timestamp uint64
	Data      []uint8
}

func UnmarshalMsgBasePing(r io.Reader) (*MsgBasePing, error) {
	m := &MsgBasePing{}
	if err := binary.Read(r, binary.LittleEndian, &m.Id); err != nil {
		return nil, fmt.Errorf("unmarshal Id: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Timestamp); err != nil {
		return nil, fmt.Errorf("unmarshal Timestamp: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Data); err != nil {
		return nil, fmt.Errorf("unmarshal Data: %w", err)
	}
	return m, nil
}
func (m *MsgBasePing) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.Id); err != nil {
		return nil, fmt.Errorf("marshal Id: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Timestamp); err != nil {
		return nil, fmt.Errorf("marshal Timestamp: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Data); err != nil {
		return nil, fmt.Errorf("marshal Data: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgBaseWaitForChannels struct {
	WaitCount uint8
	WaitList  []*ChannelWait
}

func UnmarshalMsgBaseWaitForChannels(r io.Reader) (*MsgBaseWaitForChannels, error) {
	m := &MsgBaseWaitForChannels{}
	if err := binary.Read(r, binary.LittleEndian, &m.WaitCount); err != nil {
		return nil, fmt.Errorf("unmarshal WaitCount: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.WaitList); err != nil {
		return nil, fmt.Errorf("unmarshal WaitList: %w", err)
	}
	return m, nil
}
func (m *MsgBaseWaitForChannels) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.WaitCount); err != nil {
		return nil, fmt.Errorf("marshal WaitCount: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.WaitList); err != nil {
		return nil, fmt.Errorf("marshal WaitList: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgBaseDisconnecting struct {
	TimeStamp uint64
	Reason    *LinkErr
}

func UnmarshalMsgBaseDisconnecting(r io.Reader) (*MsgBaseDisconnecting, error) {
	m := &MsgBaseDisconnecting{}
	if err := binary.Read(r, binary.LittleEndian, &m.TimeStamp); err != nil {
		return nil, fmt.Errorf("unmarshal TimeStamp: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Reason); err != nil {
		return nil, fmt.Errorf("unmarshal Reason: %w", err)
	}
	return m, nil
}
func (m *MsgBaseDisconnecting) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.TimeStamp); err != nil {
		return nil, fmt.Errorf("marshal TimeStamp: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Reason); err != nil {
		return nil, fmt.Errorf("marshal Reason: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgBaseNotify struct {
	TimeStamp  uint64
	Severity   *NotifySeverity
	Visibilty  *NotifyVisibility
	What       uint32
	MessageLen uint32
	Message    []uint8
}

func UnmarshalMsgBaseNotify(r io.Reader) (*MsgBaseNotify, error) {
	m := &MsgBaseNotify{}
	if err := binary.Read(r, binary.LittleEndian, &m.TimeStamp); err != nil {
		return nil, fmt.Errorf("unmarshal TimeStamp: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Severity); err != nil {
		return nil, fmt.Errorf("unmarshal Severity: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Visibilty); err != nil {
		return nil, fmt.Errorf("unmarshal Visibilty: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.What); err != nil {
		return nil, fmt.Errorf("unmarshal What: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.MessageLen); err != nil {
		return nil, fmt.Errorf("unmarshal MessageLen: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Message); err != nil {
		return nil, fmt.Errorf("unmarshal Message: %w", err)
	}
	return m, nil
}
func (m *MsgBaseNotify) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.TimeStamp); err != nil {
		return nil, fmt.Errorf("marshal TimeStamp: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Severity); err != nil {
		return nil, fmt.Errorf("marshal Severity: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Visibilty); err != nil {
		return nil, fmt.Errorf("marshal Visibilty: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.What); err != nil {
		return nil, fmt.Errorf("marshal What: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.MessageLen); err != nil {
		return nil, fmt.Errorf("marshal MessageLen: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Message); err != nil {
		return nil, fmt.Errorf("marshal Message: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgBaseList struct{}
type MsgBaseBaseLast struct{}
type ChannelId struct {
	Type uint8
	Id   uint8
}
type DstInfo struct {
	Port            uint16
	Sport           uint16
	HostSize        uint32
	HostData        []uint8
	CertSubjectSize uint32
	CertSubjectData []uint8
}
type MsgMainMigrateBegin struct {
	DstInfo *DstInfo
}

func UnmarshalMsgMainMigrateBegin(r io.Reader) (*MsgMainMigrateBegin, error) {
	m := &MsgMainMigrateBegin{}
	if err := binary.Read(r, binary.LittleEndian, &m.DstInfo); err != nil {
		return nil, fmt.Errorf("unmarshal DstInfo: %w", err)
	}
	return m, nil
}
func (m *MsgMainMigrateBegin) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.DstInfo); err != nil {
		return nil, fmt.Errorf("marshal DstInfo: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgMainMigrateCancel struct{}
type MsgMainInit struct {
	SessionId           uint32
	DisplayChannelsHint uint32
	SupportedMouseModes uint32
	CurrentMouseMode    uint32
	AgentConnected      uint32
	AgentTokens         uint32
	MultiMediaTime      uint32
	RamHint             uint32
}

func UnmarshalMsgMainInit(r io.Reader) (*MsgMainInit, error) {
	m := &MsgMainInit{}
	if err := binary.Read(r, binary.LittleEndian, &m.SessionId); err != nil {
		return nil, fmt.Errorf("unmarshal SessionId: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.DisplayChannelsHint); err != nil {
		return nil, fmt.Errorf("unmarshal DisplayChannelsHint: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.SupportedMouseModes); err != nil {
		return nil, fmt.Errorf("unmarshal SupportedMouseModes: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.CurrentMouseMode); err != nil {
		return nil, fmt.Errorf("unmarshal CurrentMouseMode: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.AgentConnected); err != nil {
		return nil, fmt.Errorf("unmarshal AgentConnected: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.AgentTokens); err != nil {
		return nil, fmt.Errorf("unmarshal AgentTokens: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.MultiMediaTime); err != nil {
		return nil, fmt.Errorf("unmarshal MultiMediaTime: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.RamHint); err != nil {
		return nil, fmt.Errorf("unmarshal RamHint: %w", err)
	}
	return m, nil
}
func (m *MsgMainInit) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.SessionId); err != nil {
		return nil, fmt.Errorf("marshal SessionId: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.DisplayChannelsHint); err != nil {
		return nil, fmt.Errorf("marshal DisplayChannelsHint: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.SupportedMouseModes); err != nil {
		return nil, fmt.Errorf("marshal SupportedMouseModes: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.CurrentMouseMode); err != nil {
		return nil, fmt.Errorf("marshal CurrentMouseMode: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.AgentConnected); err != nil {
		return nil, fmt.Errorf("marshal AgentConnected: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.AgentTokens); err != nil {
		return nil, fmt.Errorf("marshal AgentTokens: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.MultiMediaTime); err != nil {
		return nil, fmt.Errorf("marshal MultiMediaTime: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.RamHint); err != nil {
		return nil, fmt.Errorf("marshal RamHint: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgMainChannelsList struct {
	NumOfChannels uint32
	Channels      []*ChannelId
}

func UnmarshalMsgMainChannelsList(r io.Reader) (*MsgMainChannelsList, error) {
	m := &MsgMainChannelsList{}
	if err := binary.Read(r, binary.LittleEndian, &m.NumOfChannels); err != nil {
		return nil, fmt.Errorf("unmarshal NumOfChannels: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Channels); err != nil {
		return nil, fmt.Errorf("unmarshal Channels: %w", err)
	}
	return m, nil
}
func (m *MsgMainChannelsList) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.NumOfChannels); err != nil {
		return nil, fmt.Errorf("marshal NumOfChannels: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Channels); err != nil {
		return nil, fmt.Errorf("marshal Channels: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgMainMouseMode struct {
	SupportedModes *MouseMode
	CurrentMode    *MouseMode
}

func UnmarshalMsgMainMouseMode(r io.Reader) (*MsgMainMouseMode, error) {
	m := &MsgMainMouseMode{}
	if err := binary.Read(r, binary.LittleEndian, &m.SupportedModes); err != nil {
		return nil, fmt.Errorf("unmarshal SupportedModes: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.CurrentMode); err != nil {
		return nil, fmt.Errorf("unmarshal CurrentMode: %w", err)
	}
	return m, nil
}
func (m *MsgMainMouseMode) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.SupportedModes); err != nil {
		return nil, fmt.Errorf("marshal SupportedModes: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.CurrentMode); err != nil {
		return nil, fmt.Errorf("marshal CurrentMode: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgMainMultiMediaTime struct {
	Time uint32
}

func UnmarshalMsgMainMultiMediaTime(r io.Reader) (*MsgMainMultiMediaTime, error) {
	m := &MsgMainMultiMediaTime{}
	if err := binary.Read(r, binary.LittleEndian, &m.Time); err != nil {
		return nil, fmt.Errorf("unmarshal Time: %w", err)
	}
	return m, nil
}
func (m *MsgMainMultiMediaTime) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.Time); err != nil {
		return nil, fmt.Errorf("marshal Time: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgMainAgentConnected struct{}
type MsgMainAgentDisconnected struct {
	ErrorCode *LinkErr
}

func UnmarshalMsgMainAgentDisconnected(r io.Reader) (*MsgMainAgentDisconnected, error) {
	m := &MsgMainAgentDisconnected{}
	if err := binary.Read(r, binary.LittleEndian, &m.ErrorCode); err != nil {
		return nil, fmt.Errorf("unmarshal ErrorCode: %w", err)
	}
	return m, nil
}
func (m *MsgMainAgentDisconnected) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.ErrorCode); err != nil {
		return nil, fmt.Errorf("marshal ErrorCode: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgMainAgentData struct{}
type MsgMainAgentToken struct {
	NumTokens uint32
}

func UnmarshalMsgMainAgentToken(r io.Reader) (*MsgMainAgentToken, error) {
	m := &MsgMainAgentToken{}
	if err := binary.Read(r, binary.LittleEndian, &m.NumTokens); err != nil {
		return nil, fmt.Errorf("unmarshal NumTokens: %w", err)
	}
	return m, nil
}
func (m *MsgMainAgentToken) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.NumTokens); err != nil {
		return nil, fmt.Errorf("marshal NumTokens: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgMainMigrateSwitchHost struct {
	Port            uint16
	Sport           uint16
	HostSize        uint32
	HostData        []uint8
	CertSubjectSize uint32
	CertSubjectData []uint8
}

func UnmarshalMsgMainMigrateSwitchHost(r io.Reader) (*MsgMainMigrateSwitchHost, error) {
	m := &MsgMainMigrateSwitchHost{}
	if err := binary.Read(r, binary.LittleEndian, &m.Port); err != nil {
		return nil, fmt.Errorf("unmarshal Port: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Sport); err != nil {
		return nil, fmt.Errorf("unmarshal Sport: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.HostSize); err != nil {
		return nil, fmt.Errorf("unmarshal HostSize: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.HostData); err != nil {
		return nil, fmt.Errorf("unmarshal HostData: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.CertSubjectSize); err != nil {
		return nil, fmt.Errorf("unmarshal CertSubjectSize: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.CertSubjectData); err != nil {
		return nil, fmt.Errorf("unmarshal CertSubjectData: %w", err)
	}
	return m, nil
}
func (m *MsgMainMigrateSwitchHost) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.Port); err != nil {
		return nil, fmt.Errorf("marshal Port: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Sport); err != nil {
		return nil, fmt.Errorf("marshal Sport: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.HostSize); err != nil {
		return nil, fmt.Errorf("marshal HostSize: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.HostData); err != nil {
		return nil, fmt.Errorf("marshal HostData: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.CertSubjectSize); err != nil {
		return nil, fmt.Errorf("marshal CertSubjectSize: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.CertSubjectData); err != nil {
		return nil, fmt.Errorf("marshal CertSubjectData: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgMainMigrateEnd struct{}
type MsgMainName struct {
	NameLen uint32
	Name    []uint8
}

func UnmarshalMsgMainName(r io.Reader) (*MsgMainName, error) {
	m := &MsgMainName{}
	if err := binary.Read(r, binary.LittleEndian, &m.NameLen); err != nil {
		return nil, fmt.Errorf("unmarshal NameLen: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Name); err != nil {
		return nil, fmt.Errorf("unmarshal Name: %w", err)
	}
	return m, nil
}
func (m *MsgMainName) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.NameLen); err != nil {
		return nil, fmt.Errorf("marshal NameLen: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Name); err != nil {
		return nil, fmt.Errorf("marshal Name: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgMainUuid struct {
	Uuid []uint8
}

func UnmarshalMsgMainUuid(r io.Reader) (*MsgMainUuid, error) {
	m := &MsgMainUuid{}
	if err := binary.Read(r, binary.LittleEndian, &m.Uuid); err != nil {
		return nil, fmt.Errorf("unmarshal Uuid: %w", err)
	}
	return m, nil
}
func (m *MsgMainUuid) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.Uuid); err != nil {
		return nil, fmt.Errorf("marshal Uuid: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgMainAgentConnectedTokens struct {
	NumTokens uint32
}

func UnmarshalMsgMainAgentConnectedTokens(r io.Reader) (*MsgMainAgentConnectedTokens, error) {
	m := &MsgMainAgentConnectedTokens{}
	if err := binary.Read(r, binary.LittleEndian, &m.NumTokens); err != nil {
		return nil, fmt.Errorf("unmarshal NumTokens: %w", err)
	}
	return m, nil
}
func (m *MsgMainAgentConnectedTokens) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.NumTokens); err != nil {
		return nil, fmt.Errorf("marshal NumTokens: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgMainMigrateBeginSeamless struct {
	DstInfo       *DstInfo
	SrcMigVersion uint32
}

func UnmarshalMsgMainMigrateBeginSeamless(r io.Reader) (*MsgMainMigrateBeginSeamless, error) {
	m := &MsgMainMigrateBeginSeamless{}
	if err := binary.Read(r, binary.LittleEndian, &m.DstInfo); err != nil {
		return nil, fmt.Errorf("unmarshal DstInfo: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.SrcMigVersion); err != nil {
		return nil, fmt.Errorf("unmarshal SrcMigVersion: %w", err)
	}
	return m, nil
}
func (m *MsgMainMigrateBeginSeamless) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.DstInfo); err != nil {
		return nil, fmt.Errorf("marshal DstInfo: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.SrcMigVersion); err != nil {
		return nil, fmt.Errorf("marshal SrcMigVersion: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgMainMigrateDstSeamlessAck struct{}
type MsgMainMigrateDstSeamlessNack struct{}
type ClipType uint8

const (
	ClipTypeNone ClipType = iota
	ClipTypeRects
)

type PathFlags uint8

const (
	PathFlagsBegin PathFlags = 1 << iota
)
const (
	PathFlagsEnd PathFlags = 1<<iota + 1
)
const (
	PathFlagsClose PathFlags = 1<<iota + 3
)
const (
	PathFlagsBezier PathFlags = 1<<iota + 4
)

type VideoCodecType uint8

const (
	VideoCodecTypeMjpeg VideoCodecType = iota + 1
	VideoCodecTypeVp8
	VideoCodecTypeH264
	VideoCodecTypeVp9
	VideoCodecTypeH265
)

type StreamFlags uint8

const (
	StreamFlagsTopDown StreamFlags = 1 << iota
)

type BrushType uint8

const (
	BrushTypeNone BrushType = iota
	BrushTypeSolid
	BrushTypePattern
)

type MaskFlags uint8

const (
	MaskFlagsInvers MaskFlags = 1 << iota
)

type ImageType uint8

const (
	ImageTypeBitmap ImageType = iota
	ImageTypeQuic
	ImageTypeReserved
)
const (
	ImageTypeLzPlt ImageType = iota + 100
	ImageTypeLzRgb
	ImageTypeGlzRgb
	ImageTypeFromCache
	ImageTypeSurface
	ImageTypeJpeg
	ImageTypeFromCacheLossless
	ImageTypeZlibGlzRgb
	ImageTypeJpegAlpha
	ImageTypeLz4
)

type ImageCompression uint8

const (
	ImageCompressionInvalid ImageCompression = iota
	ImageCompressionOff
	ImageCompressionAutoGlz
	ImageCompressionAutoLz
	ImageCompressionQuic
	ImageCompressionGlz
	ImageCompressionLz
	ImageCompressionLz4
)

type ImageFlags uint8

const (
	ImageFlagsCacheMe ImageFlags = 1 << iota
	ImageFlagsHighBitsSet
	ImageFlagsCacheReplaceMe
)

type BitmapFmt uint8

const (
	BitmapFmtInvalid BitmapFmt = iota
	BitmapFmt1BitLe
	BitmapFmt1BitBe
	BitmapFmt4BitLe
	BitmapFmt4BitBe
	BitmapFmt8Bit
	BitmapFmt16Bit
	BitmapFmt24Bit
	BitmapFmt32Bit
	BitmapFmtRgba
	BitmapFmt8BitA
)

type BitmapFlags uint8

const (
	BitmapFlagsPalCacheMe BitmapFlags = 1 << iota
	BitmapFlagsPalFromCache
	BitmapFlagsTopDown
)

type JpegAlphaFlags uint8

const (
	JpegAlphaFlagsTopDown JpegAlphaFlags = 1 << iota
)

type ImageScaleMode uint8

const (
	ImageScaleModeInterpolate ImageScaleMode = iota
	ImageScaleModeNearest
)

type Ropd uint16

const (
	RopdInversSrc Ropd = 1 << iota
	RopdInversBrush
	RopdInversDest
	RopdOpPut
	RopdOpOr
	RopdOpAnd
	RopdOpXor
	RopdOpBlackness
	RopdOpWhiteness
	RopdOpInvers
	RopdInversRes
)

type LineFlags uint8

const (
	LineFlagsStyled LineFlags = 1<<iota + 3
)
const (
	LineFlagsStartWithGap LineFlags = 1<<iota + 2
)

type StringFlags uint8

const (
	StringFlagsRasterA1 StringFlags = 1 << iota
	StringFlagsRasterA4
	StringFlagsRasterA8
	StringFlagsRasterTopDown
)

type SurfaceFlags uint32

const (
	SurfaceFlagsPrimary SurfaceFlags = 1 << iota
	SurfaceFlagsStreamingMode
)

type SurfaceFmt uint32

const (
	SurfaceFmtInvalid SurfaceFmt = iota
)
const (
	SurfaceFmt1A SurfaceFmt = iota + 1
)
const (
	SurfaceFmt8A SurfaceFmt = iota + 8
)
const (
	SurfaceFmt16555 SurfaceFmt = iota + 16
)
const (
	SurfaceFmt16565 SurfaceFmt = iota + 80
)
const (
	SurfaceFmt32Xrgb SurfaceFmt = iota + 32
)
const (
	SurfaceFmt32Argb SurfaceFmt = iota + 96
)

type AlphaFlags uint8

const (
	AlphaFlagsDestHasAlpha AlphaFlags = 1 << iota
	AlphaFlagsSrcSurfaceHasAlpha
)

type ResourceType uint8

const (
	ResourceTypeInvalid ResourceType = iota
	ResourceTypePixmap
)

type ClipRects struct {
	NumRects uint32
	Rects    []*Rect
}
type PathSegment struct {
	Flags  *PathFlags
	Count  uint32
	Points []*PointFix
}
type Path struct {
	NumSegments uint32
	Segments    []*PathSegment
}
type Clip struct {
	Type *ClipType
}
type DisplayBase struct {
	SurfaceId uint32
	Box       *Rect
	Clip      *Clip
}
type ResourceID struct {
	Type uint8
	Id   uint64
}
type WaitForChannel struct {
	ChannelType   uint8
	ChannelId     uint8
	MessageSerial uint64
}
type Palette struct {
	Unique  uint64
	NumEnts uint16
	Ents    []uint32
}
type BitmapData struct {
	Format *BitmapFmt
	Flags  *BitmapFlags
	X      uint32
	Y      uint32
	Stride uint32
	Data   []uint8
}
type BinaryData struct {
	DataSize uint32
	Data     []uint8
}
type LZPLTData struct {
	Flags    *BitmapFlags
	DataSize uint32
	Data     []uint8
}
type ZlibGlzRGBData struct {
	GlzDataSize uint32
	DataSize    uint32
	Data        []uint8
}
type JPEGAlphaData struct {
	Flags    *JpegAlphaFlags
	JpegSize uint32
	DataSize uint32
	Data     []uint8
}
type Surface struct {
	SurfaceId uint32
}
type Image struct {
	Descriptor *ImageDescriptor
}
type Pattern struct {
	Pat *Image
	Pos *Point
}
type Brush struct {
	Type *BrushType
}
type QMask struct {
	Flags  *MaskFlags
	Pos    *Point
	Bitmap *Image
}
type LineAttr struct {
	Flags *LineFlags
}
type RasterGlyphA1 struct {
	RenderPos   *Point
	GlyphOrigin *Point
	Width       uint16
	Height      uint16
	Data        []uint8
}
type RasterGlyphA4 struct {
	RenderPos   *Point
	GlyphOrigin *Point
	Width       uint16
	Height      uint16
	Data        []uint8
}
type RasterGlyphA8 struct {
	RenderPos   *Point
	GlyphOrigin *Point
	Width       uint16
	Height      uint16
	Data        []uint8
}
type String struct {
	Length uint16
	Flags  *StringFlags
}
type StreamDataHeader struct {
	Id             *StreamIdT
	MultiMediaTime uint32
}
type Head struct {
	MonitorId uint32
	SurfaceId uint32
	Width     uint32
	Height    uint32
	X         uint32
	Y         uint32
	Flags     uint32
}
type GlScanoutFlags uint32

const (
	GlScanoutFlagsY0Top GlScanoutFlags = 1 << iota
)

type MsgDisplayMode struct {
	XRes uint32
	YRes uint32
	Bits uint32
}

func UnmarshalMsgDisplayMode(r io.Reader) (*MsgDisplayMode, error) {
	m := &MsgDisplayMode{}
	if err := binary.Read(r, binary.LittleEndian, &m.XRes); err != nil {
		return nil, fmt.Errorf("unmarshal XRes: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.YRes); err != nil {
		return nil, fmt.Errorf("unmarshal YRes: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Bits); err != nil {
		return nil, fmt.Errorf("unmarshal Bits: %w", err)
	}
	return m, nil
}
func (m *MsgDisplayMode) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.XRes); err != nil {
		return nil, fmt.Errorf("marshal XRes: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.YRes); err != nil {
		return nil, fmt.Errorf("marshal YRes: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Bits); err != nil {
		return nil, fmt.Errorf("marshal Bits: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgDisplayMark struct{}
type MsgDisplayReset struct{}
type MsgDisplayCopyBits struct {
	Base   *DisplayBase
	SrcPos *Point
}

func UnmarshalMsgDisplayCopyBits(r io.Reader) (*MsgDisplayCopyBits, error) {
	m := &MsgDisplayCopyBits{}
	if err := binary.Read(r, binary.LittleEndian, &m.Base); err != nil {
		return nil, fmt.Errorf("unmarshal Base: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.SrcPos); err != nil {
		return nil, fmt.Errorf("unmarshal SrcPos: %w", err)
	}
	return m, nil
}
func (m *MsgDisplayCopyBits) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.Base); err != nil {
		return nil, fmt.Errorf("marshal Base: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.SrcPos); err != nil {
		return nil, fmt.Errorf("marshal SrcPos: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgDisplayInvalList struct {
	Count     uint16
	Resources []*ResourceID
}

func UnmarshalMsgDisplayInvalList(r io.Reader) (*MsgDisplayInvalList, error) {
	m := &MsgDisplayInvalList{}
	if err := binary.Read(r, binary.LittleEndian, &m.Count); err != nil {
		return nil, fmt.Errorf("unmarshal Count: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Resources); err != nil {
		return nil, fmt.Errorf("unmarshal Resources: %w", err)
	}
	return m, nil
}
func (m *MsgDisplayInvalList) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.Count); err != nil {
		return nil, fmt.Errorf("marshal Count: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Resources); err != nil {
		return nil, fmt.Errorf("marshal Resources: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgDisplayInvalAllPixmaps struct {
	WaitCount uint8
	WaitList  []*WaitForChannel
}

func UnmarshalMsgDisplayInvalAllPixmaps(r io.Reader) (*MsgDisplayInvalAllPixmaps, error) {
	m := &MsgDisplayInvalAllPixmaps{}
	if err := binary.Read(r, binary.LittleEndian, &m.WaitCount); err != nil {
		return nil, fmt.Errorf("unmarshal WaitCount: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.WaitList); err != nil {
		return nil, fmt.Errorf("unmarshal WaitList: %w", err)
	}
	return m, nil
}
func (m *MsgDisplayInvalAllPixmaps) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.WaitCount); err != nil {
		return nil, fmt.Errorf("marshal WaitCount: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.WaitList); err != nil {
		return nil, fmt.Errorf("marshal WaitList: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgDisplayInvalPalette struct {
	Id uint64
}

func UnmarshalMsgDisplayInvalPalette(r io.Reader) (*MsgDisplayInvalPalette, error) {
	m := &MsgDisplayInvalPalette{}
	if err := binary.Read(r, binary.LittleEndian, &m.Id); err != nil {
		return nil, fmt.Errorf("unmarshal Id: %w", err)
	}
	return m, nil
}
func (m *MsgDisplayInvalPalette) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.Id); err != nil {
		return nil, fmt.Errorf("marshal Id: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgDisplayInvalAllPalettes struct{}
type MsgDisplayStreamCreate struct {
	SurfaceId    uint32
	Id           *StreamIdT
	Flags        *StreamFlags
	CodecType    *VideoCodecType
	Stamp        uint64
	StreamWidth  uint32
	StreamHeight uint32
	SrcWidth     uint32
	SrcHeight    uint32
	Dest         *Rect
	Clip         *Clip
}

func UnmarshalMsgDisplayStreamCreate(r io.Reader) (*MsgDisplayStreamCreate, error) {
	m := &MsgDisplayStreamCreate{}
	if err := binary.Read(r, binary.LittleEndian, &m.SurfaceId); err != nil {
		return nil, fmt.Errorf("unmarshal SurfaceId: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Id); err != nil {
		return nil, fmt.Errorf("unmarshal Id: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Flags); err != nil {
		return nil, fmt.Errorf("unmarshal Flags: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.CodecType); err != nil {
		return nil, fmt.Errorf("unmarshal CodecType: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Stamp); err != nil {
		return nil, fmt.Errorf("unmarshal Stamp: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.StreamWidth); err != nil {
		return nil, fmt.Errorf("unmarshal StreamWidth: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.StreamHeight); err != nil {
		return nil, fmt.Errorf("unmarshal StreamHeight: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.SrcWidth); err != nil {
		return nil, fmt.Errorf("unmarshal SrcWidth: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.SrcHeight); err != nil {
		return nil, fmt.Errorf("unmarshal SrcHeight: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Dest); err != nil {
		return nil, fmt.Errorf("unmarshal Dest: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Clip); err != nil {
		return nil, fmt.Errorf("unmarshal Clip: %w", err)
	}
	return m, nil
}
func (m *MsgDisplayStreamCreate) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.SurfaceId); err != nil {
		return nil, fmt.Errorf("marshal SurfaceId: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Id); err != nil {
		return nil, fmt.Errorf("marshal Id: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Flags); err != nil {
		return nil, fmt.Errorf("marshal Flags: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.CodecType); err != nil {
		return nil, fmt.Errorf("marshal CodecType: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Stamp); err != nil {
		return nil, fmt.Errorf("marshal Stamp: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.StreamWidth); err != nil {
		return nil, fmt.Errorf("marshal StreamWidth: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.StreamHeight); err != nil {
		return nil, fmt.Errorf("marshal StreamHeight: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.SrcWidth); err != nil {
		return nil, fmt.Errorf("marshal SrcWidth: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.SrcHeight); err != nil {
		return nil, fmt.Errorf("marshal SrcHeight: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Dest); err != nil {
		return nil, fmt.Errorf("marshal Dest: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Clip); err != nil {
		return nil, fmt.Errorf("marshal Clip: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgDisplayStreamData struct {
	Base     *StreamDataHeader
	DataSize uint32
	Data     []uint8
}

func UnmarshalMsgDisplayStreamData(r io.Reader) (*MsgDisplayStreamData, error) {
	m := &MsgDisplayStreamData{}
	if err := binary.Read(r, binary.LittleEndian, &m.Base); err != nil {
		return nil, fmt.Errorf("unmarshal Base: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.DataSize); err != nil {
		return nil, fmt.Errorf("unmarshal DataSize: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Data); err != nil {
		return nil, fmt.Errorf("unmarshal Data: %w", err)
	}
	return m, nil
}
func (m *MsgDisplayStreamData) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.Base); err != nil {
		return nil, fmt.Errorf("marshal Base: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.DataSize); err != nil {
		return nil, fmt.Errorf("marshal DataSize: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Data); err != nil {
		return nil, fmt.Errorf("marshal Data: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgDisplayStreamClip struct {
	Id   *StreamIdT
	Clip *Clip
}

func UnmarshalMsgDisplayStreamClip(r io.Reader) (*MsgDisplayStreamClip, error) {
	m := &MsgDisplayStreamClip{}
	if err := binary.Read(r, binary.LittleEndian, &m.Id); err != nil {
		return nil, fmt.Errorf("unmarshal Id: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Clip); err != nil {
		return nil, fmt.Errorf("unmarshal Clip: %w", err)
	}
	return m, nil
}
func (m *MsgDisplayStreamClip) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.Id); err != nil {
		return nil, fmt.Errorf("marshal Id: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Clip); err != nil {
		return nil, fmt.Errorf("marshal Clip: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgDisplayStreamDestroy struct {
	Id *StreamIdT
}

func UnmarshalMsgDisplayStreamDestroy(r io.Reader) (*MsgDisplayStreamDestroy, error) {
	m := &MsgDisplayStreamDestroy{}
	if err := binary.Read(r, binary.LittleEndian, &m.Id); err != nil {
		return nil, fmt.Errorf("unmarshal Id: %w", err)
	}
	return m, nil
}
func (m *MsgDisplayStreamDestroy) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.Id); err != nil {
		return nil, fmt.Errorf("marshal Id: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgDisplayStreamDestroyAll struct{}
type MsgDisplayDrawFill struct {
	Base *DisplayBase
	Data *Fill
}

func UnmarshalMsgDisplayDrawFill(r io.Reader) (*MsgDisplayDrawFill, error) {
	m := &MsgDisplayDrawFill{}
	if err := binary.Read(r, binary.LittleEndian, &m.Base); err != nil {
		return nil, fmt.Errorf("unmarshal Base: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Data); err != nil {
		return nil, fmt.Errorf("unmarshal Data: %w", err)
	}
	return m, nil
}
func (m *MsgDisplayDrawFill) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.Base); err != nil {
		return nil, fmt.Errorf("marshal Base: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Data); err != nil {
		return nil, fmt.Errorf("marshal Data: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgDisplayDrawOpaque struct {
	Base *DisplayBase
	Data *Opaque
}

func UnmarshalMsgDisplayDrawOpaque(r io.Reader) (*MsgDisplayDrawOpaque, error) {
	m := &MsgDisplayDrawOpaque{}
	if err := binary.Read(r, binary.LittleEndian, &m.Base); err != nil {
		return nil, fmt.Errorf("unmarshal Base: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Data); err != nil {
		return nil, fmt.Errorf("unmarshal Data: %w", err)
	}
	return m, nil
}
func (m *MsgDisplayDrawOpaque) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.Base); err != nil {
		return nil, fmt.Errorf("marshal Base: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Data); err != nil {
		return nil, fmt.Errorf("marshal Data: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgDisplayDrawCopy struct {
	Base *DisplayBase
	Data *Copy
}

func UnmarshalMsgDisplayDrawCopy(r io.Reader) (*MsgDisplayDrawCopy, error) {
	m := &MsgDisplayDrawCopy{}
	if err := binary.Read(r, binary.LittleEndian, &m.Base); err != nil {
		return nil, fmt.Errorf("unmarshal Base: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Data); err != nil {
		return nil, fmt.Errorf("unmarshal Data: %w", err)
	}
	return m, nil
}
func (m *MsgDisplayDrawCopy) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.Base); err != nil {
		return nil, fmt.Errorf("marshal Base: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Data); err != nil {
		return nil, fmt.Errorf("marshal Data: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgDisplayDrawBlend struct {
	Base *DisplayBase
	Data *Blend
}

func UnmarshalMsgDisplayDrawBlend(r io.Reader) (*MsgDisplayDrawBlend, error) {
	m := &MsgDisplayDrawBlend{}
	if err := binary.Read(r, binary.LittleEndian, &m.Base); err != nil {
		return nil, fmt.Errorf("unmarshal Base: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Data); err != nil {
		return nil, fmt.Errorf("unmarshal Data: %w", err)
	}
	return m, nil
}
func (m *MsgDisplayDrawBlend) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.Base); err != nil {
		return nil, fmt.Errorf("marshal Base: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Data); err != nil {
		return nil, fmt.Errorf("marshal Data: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgDisplayDrawBlackness struct {
	Base *DisplayBase
	Data *Blackness
}

func UnmarshalMsgDisplayDrawBlackness(r io.Reader) (*MsgDisplayDrawBlackness, error) {
	m := &MsgDisplayDrawBlackness{}
	if err := binary.Read(r, binary.LittleEndian, &m.Base); err != nil {
		return nil, fmt.Errorf("unmarshal Base: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Data); err != nil {
		return nil, fmt.Errorf("unmarshal Data: %w", err)
	}
	return m, nil
}
func (m *MsgDisplayDrawBlackness) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.Base); err != nil {
		return nil, fmt.Errorf("marshal Base: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Data); err != nil {
		return nil, fmt.Errorf("marshal Data: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgDisplayDrawWhiteness struct {
	Base *DisplayBase
	Data *Whiteness
}

func UnmarshalMsgDisplayDrawWhiteness(r io.Reader) (*MsgDisplayDrawWhiteness, error) {
	m := &MsgDisplayDrawWhiteness{}
	if err := binary.Read(r, binary.LittleEndian, &m.Base); err != nil {
		return nil, fmt.Errorf("unmarshal Base: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Data); err != nil {
		return nil, fmt.Errorf("unmarshal Data: %w", err)
	}
	return m, nil
}
func (m *MsgDisplayDrawWhiteness) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.Base); err != nil {
		return nil, fmt.Errorf("marshal Base: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Data); err != nil {
		return nil, fmt.Errorf("marshal Data: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgDisplayDrawInvers struct {
	Base *DisplayBase
	Data *Invers
}

func UnmarshalMsgDisplayDrawInvers(r io.Reader) (*MsgDisplayDrawInvers, error) {
	m := &MsgDisplayDrawInvers{}
	if err := binary.Read(r, binary.LittleEndian, &m.Base); err != nil {
		return nil, fmt.Errorf("unmarshal Base: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Data); err != nil {
		return nil, fmt.Errorf("unmarshal Data: %w", err)
	}
	return m, nil
}
func (m *MsgDisplayDrawInvers) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.Base); err != nil {
		return nil, fmt.Errorf("marshal Base: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Data); err != nil {
		return nil, fmt.Errorf("marshal Data: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgDisplayDrawRop3 struct {
	Base *DisplayBase
	Data *Rop3
}

func UnmarshalMsgDisplayDrawRop3(r io.Reader) (*MsgDisplayDrawRop3, error) {
	m := &MsgDisplayDrawRop3{}
	if err := binary.Read(r, binary.LittleEndian, &m.Base); err != nil {
		return nil, fmt.Errorf("unmarshal Base: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Data); err != nil {
		return nil, fmt.Errorf("unmarshal Data: %w", err)
	}
	return m, nil
}
func (m *MsgDisplayDrawRop3) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.Base); err != nil {
		return nil, fmt.Errorf("marshal Base: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Data); err != nil {
		return nil, fmt.Errorf("marshal Data: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgDisplayDrawStroke struct {
	Base *DisplayBase
	Data *Stroke
}

func UnmarshalMsgDisplayDrawStroke(r io.Reader) (*MsgDisplayDrawStroke, error) {
	m := &MsgDisplayDrawStroke{}
	if err := binary.Read(r, binary.LittleEndian, &m.Base); err != nil {
		return nil, fmt.Errorf("unmarshal Base: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Data); err != nil {
		return nil, fmt.Errorf("unmarshal Data: %w", err)
	}
	return m, nil
}
func (m *MsgDisplayDrawStroke) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.Base); err != nil {
		return nil, fmt.Errorf("marshal Base: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Data); err != nil {
		return nil, fmt.Errorf("marshal Data: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgDisplayDrawText struct {
	Base *DisplayBase
	Data *Text
}

func UnmarshalMsgDisplayDrawText(r io.Reader) (*MsgDisplayDrawText, error) {
	m := &MsgDisplayDrawText{}
	if err := binary.Read(r, binary.LittleEndian, &m.Base); err != nil {
		return nil, fmt.Errorf("unmarshal Base: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Data); err != nil {
		return nil, fmt.Errorf("unmarshal Data: %w", err)
	}
	return m, nil
}
func (m *MsgDisplayDrawText) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.Base); err != nil {
		return nil, fmt.Errorf("marshal Base: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Data); err != nil {
		return nil, fmt.Errorf("marshal Data: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgDisplayDrawTransparent struct {
	Base *DisplayBase
	Data *Transparent
}

func UnmarshalMsgDisplayDrawTransparent(r io.Reader) (*MsgDisplayDrawTransparent, error) {
	m := &MsgDisplayDrawTransparent{}
	if err := binary.Read(r, binary.LittleEndian, &m.Base); err != nil {
		return nil, fmt.Errorf("unmarshal Base: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Data); err != nil {
		return nil, fmt.Errorf("unmarshal Data: %w", err)
	}
	return m, nil
}
func (m *MsgDisplayDrawTransparent) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.Base); err != nil {
		return nil, fmt.Errorf("marshal Base: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Data); err != nil {
		return nil, fmt.Errorf("marshal Data: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgDisplayDrawAlphaBlend struct {
	Base *DisplayBase
	Data *AlphaBlend
}

func UnmarshalMsgDisplayDrawAlphaBlend(r io.Reader) (*MsgDisplayDrawAlphaBlend, error) {
	m := &MsgDisplayDrawAlphaBlend{}
	if err := binary.Read(r, binary.LittleEndian, &m.Base); err != nil {
		return nil, fmt.Errorf("unmarshal Base: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Data); err != nil {
		return nil, fmt.Errorf("unmarshal Data: %w", err)
	}
	return m, nil
}
func (m *MsgDisplayDrawAlphaBlend) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.Base); err != nil {
		return nil, fmt.Errorf("marshal Base: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Data); err != nil {
		return nil, fmt.Errorf("marshal Data: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgDisplaySurfaceCreate struct {
	SurfaceId uint32
	Width     uint32
	Height    uint32
	Format    *SurfaceFmt
	Flags     *SurfaceFlags
}

func UnmarshalMsgDisplaySurfaceCreate(r io.Reader) (*MsgDisplaySurfaceCreate, error) {
	m := &MsgDisplaySurfaceCreate{}
	if err := binary.Read(r, binary.LittleEndian, &m.SurfaceId); err != nil {
		return nil, fmt.Errorf("unmarshal SurfaceId: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Width); err != nil {
		return nil, fmt.Errorf("unmarshal Width: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Height); err != nil {
		return nil, fmt.Errorf("unmarshal Height: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Format); err != nil {
		return nil, fmt.Errorf("unmarshal Format: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Flags); err != nil {
		return nil, fmt.Errorf("unmarshal Flags: %w", err)
	}
	return m, nil
}
func (m *MsgDisplaySurfaceCreate) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.SurfaceId); err != nil {
		return nil, fmt.Errorf("marshal SurfaceId: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Width); err != nil {
		return nil, fmt.Errorf("marshal Width: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Height); err != nil {
		return nil, fmt.Errorf("marshal Height: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Format); err != nil {
		return nil, fmt.Errorf("marshal Format: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Flags); err != nil {
		return nil, fmt.Errorf("marshal Flags: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgDisplaySurfaceDestroy struct {
	SurfaceId uint32
}

func UnmarshalMsgDisplaySurfaceDestroy(r io.Reader) (*MsgDisplaySurfaceDestroy, error) {
	m := &MsgDisplaySurfaceDestroy{}
	if err := binary.Read(r, binary.LittleEndian, &m.SurfaceId); err != nil {
		return nil, fmt.Errorf("unmarshal SurfaceId: %w", err)
	}
	return m, nil
}
func (m *MsgDisplaySurfaceDestroy) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.SurfaceId); err != nil {
		return nil, fmt.Errorf("marshal SurfaceId: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgDisplayStreamDataSized struct {
	Base     *StreamDataHeader
	Width    uint32
	Height   uint32
	Dest     *Rect
	DataSize uint32
	Data     []uint8
}

func UnmarshalMsgDisplayStreamDataSized(r io.Reader) (*MsgDisplayStreamDataSized, error) {
	m := &MsgDisplayStreamDataSized{}
	if err := binary.Read(r, binary.LittleEndian, &m.Base); err != nil {
		return nil, fmt.Errorf("unmarshal Base: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Width); err != nil {
		return nil, fmt.Errorf("unmarshal Width: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Height); err != nil {
		return nil, fmt.Errorf("unmarshal Height: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Dest); err != nil {
		return nil, fmt.Errorf("unmarshal Dest: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.DataSize); err != nil {
		return nil, fmt.Errorf("unmarshal DataSize: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Data); err != nil {
		return nil, fmt.Errorf("unmarshal Data: %w", err)
	}
	return m, nil
}
func (m *MsgDisplayStreamDataSized) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.Base); err != nil {
		return nil, fmt.Errorf("marshal Base: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Width); err != nil {
		return nil, fmt.Errorf("marshal Width: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Height); err != nil {
		return nil, fmt.Errorf("marshal Height: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Dest); err != nil {
		return nil, fmt.Errorf("marshal Dest: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.DataSize); err != nil {
		return nil, fmt.Errorf("marshal DataSize: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Data); err != nil {
		return nil, fmt.Errorf("marshal Data: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgDisplayMonitorsConfig struct {
	Count      uint16
	MaxAllowed uint16
	Heads      []*Head
}

func UnmarshalMsgDisplayMonitorsConfig(r io.Reader) (*MsgDisplayMonitorsConfig, error) {
	m := &MsgDisplayMonitorsConfig{}
	if err := binary.Read(r, binary.LittleEndian, &m.Count); err != nil {
		return nil, fmt.Errorf("unmarshal Count: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.MaxAllowed); err != nil {
		return nil, fmt.Errorf("unmarshal MaxAllowed: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Heads); err != nil {
		return nil, fmt.Errorf("unmarshal Heads: %w", err)
	}
	return m, nil
}
func (m *MsgDisplayMonitorsConfig) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.Count); err != nil {
		return nil, fmt.Errorf("marshal Count: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.MaxAllowed); err != nil {
		return nil, fmt.Errorf("marshal MaxAllowed: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Heads); err != nil {
		return nil, fmt.Errorf("marshal Heads: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgDisplayDrawComposite struct {
	Base *DisplayBase
	Data *Composite
}

func UnmarshalMsgDisplayDrawComposite(r io.Reader) (*MsgDisplayDrawComposite, error) {
	m := &MsgDisplayDrawComposite{}
	if err := binary.Read(r, binary.LittleEndian, &m.Base); err != nil {
		return nil, fmt.Errorf("unmarshal Base: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Data); err != nil {
		return nil, fmt.Errorf("unmarshal Data: %w", err)
	}
	return m, nil
}
func (m *MsgDisplayDrawComposite) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.Base); err != nil {
		return nil, fmt.Errorf("marshal Base: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Data); err != nil {
		return nil, fmt.Errorf("marshal Data: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgDisplayStreamActivateReport struct {
	StreamId      *StreamIdT
	UniqueId      uint32
	MaxWindowSize uint32
	TimeoutMs     uint32
}

func UnmarshalMsgDisplayStreamActivateReport(r io.Reader) (*MsgDisplayStreamActivateReport, error) {
	m := &MsgDisplayStreamActivateReport{}
	if err := binary.Read(r, binary.LittleEndian, &m.StreamId); err != nil {
		return nil, fmt.Errorf("unmarshal StreamId: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.UniqueId); err != nil {
		return nil, fmt.Errorf("unmarshal UniqueId: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.MaxWindowSize); err != nil {
		return nil, fmt.Errorf("unmarshal MaxWindowSize: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.TimeoutMs); err != nil {
		return nil, fmt.Errorf("unmarshal TimeoutMs: %w", err)
	}
	return m, nil
}
func (m *MsgDisplayStreamActivateReport) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.StreamId); err != nil {
		return nil, fmt.Errorf("marshal StreamId: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.UniqueId); err != nil {
		return nil, fmt.Errorf("marshal UniqueId: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.MaxWindowSize); err != nil {
		return nil, fmt.Errorf("marshal MaxWindowSize: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.TimeoutMs); err != nil {
		return nil, fmt.Errorf("marshal TimeoutMs: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgDisplayGlScanoutUnix struct {
	DrmDmaBufFd     uintptr
	Width           uint32
	Height          uint32
	Stride          uint32
	DrmFourccFormat uint32
	Flags           *GlScanoutFlags
}

func UnmarshalMsgDisplayGlScanoutUnix(r io.Reader) (*MsgDisplayGlScanoutUnix, error) {
	m := &MsgDisplayGlScanoutUnix{}
	if err := binary.Read(r, binary.LittleEndian, &m.DrmDmaBufFd); err != nil {
		return nil, fmt.Errorf("unmarshal DrmDmaBufFd: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Width); err != nil {
		return nil, fmt.Errorf("unmarshal Width: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Height); err != nil {
		return nil, fmt.Errorf("unmarshal Height: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Stride); err != nil {
		return nil, fmt.Errorf("unmarshal Stride: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.DrmFourccFormat); err != nil {
		return nil, fmt.Errorf("unmarshal DrmFourccFormat: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Flags); err != nil {
		return nil, fmt.Errorf("unmarshal Flags: %w", err)
	}
	return m, nil
}
func (m *MsgDisplayGlScanoutUnix) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.DrmDmaBufFd); err != nil {
		return nil, fmt.Errorf("marshal DrmDmaBufFd: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Width); err != nil {
		return nil, fmt.Errorf("marshal Width: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Height); err != nil {
		return nil, fmt.Errorf("marshal Height: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Stride); err != nil {
		return nil, fmt.Errorf("marshal Stride: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.DrmFourccFormat); err != nil {
		return nil, fmt.Errorf("marshal DrmFourccFormat: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Flags); err != nil {
		return nil, fmt.Errorf("marshal Flags: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgDisplayGlDraw struct {
	X uint32
	Y uint32
	W uint32
	H uint32
}

func UnmarshalMsgDisplayGlDraw(r io.Reader) (*MsgDisplayGlDraw, error) {
	m := &MsgDisplayGlDraw{}
	if err := binary.Read(r, binary.LittleEndian, &m.X); err != nil {
		return nil, fmt.Errorf("unmarshal X: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Y); err != nil {
		return nil, fmt.Errorf("unmarshal Y: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.W); err != nil {
		return nil, fmt.Errorf("unmarshal W: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.H); err != nil {
		return nil, fmt.Errorf("unmarshal H: %w", err)
	}
	return m, nil
}
func (m *MsgDisplayGlDraw) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.X); err != nil {
		return nil, fmt.Errorf("marshal X: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Y); err != nil {
		return nil, fmt.Errorf("marshal Y: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.W); err != nil {
		return nil, fmt.Errorf("marshal W: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.H); err != nil {
		return nil, fmt.Errorf("marshal H: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgDisplayQualityIndicator struct{}
type KeyboardModifierFlags uint16

const (
	KeyboardModifierFlagsScrollLock KeyboardModifierFlags = 1 << iota
	KeyboardModifierFlagsNumLock
	KeyboardModifierFlagsCapsLock
)

type MouseButton uint8

const (
	MouseButtonInvalid MouseButton = iota
	MouseButtonLeft
	MouseButtonMiddle
	MouseButtonRight
	MouseButtonUp
	MouseButtonDown
)

type MouseButtonMask uint16

const (
	MouseButtonMaskLeft MouseButtonMask = 1 << iota
	MouseButtonMaskMiddle
	MouseButtonMaskRight
)

type MsgInputsInit struct {
	KeyboardModifiers *KeyboardModifierFlags
}

func UnmarshalMsgInputsInit(r io.Reader) (*MsgInputsInit, error) {
	m := &MsgInputsInit{}
	if err := binary.Read(r, binary.LittleEndian, &m.KeyboardModifiers); err != nil {
		return nil, fmt.Errorf("unmarshal KeyboardModifiers: %w", err)
	}
	return m, nil
}
func (m *MsgInputsInit) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.KeyboardModifiers); err != nil {
		return nil, fmt.Errorf("marshal KeyboardModifiers: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgInputsKeyModifiers struct {
	Modifiers *KeyboardModifierFlags
}

func UnmarshalMsgInputsKeyModifiers(r io.Reader) (*MsgInputsKeyModifiers, error) {
	m := &MsgInputsKeyModifiers{}
	if err := binary.Read(r, binary.LittleEndian, &m.Modifiers); err != nil {
		return nil, fmt.Errorf("unmarshal Modifiers: %w", err)
	}
	return m, nil
}
func (m *MsgInputsKeyModifiers) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.Modifiers); err != nil {
		return nil, fmt.Errorf("marshal Modifiers: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgInputsMouseMotionAck struct{}
type CursorType uint8

const (
	CursorTypeAlpha CursorType = iota
	CursorTypeMono
	CursorTypeColor4
	CursorTypeColor8
	CursorTypeColor16
	CursorTypeColor24
	CursorTypeColor32
)

type CursorFlags uint16

const (
	CursorFlagsNone CursorFlags = 1 << iota
	CursorFlagsCacheMe
	CursorFlagsFromCache
)

type CursorHeader struct {
	Unique   uint64
	Type     *CursorType
	Width    uint16
	Height   uint16
	HotSpotX uint16
	HotSpotY uint16
}
type Cursor struct {
	Flags *CursorFlags
	Data  []uint8
}
type MsgCursorInit struct {
	Position       *Point16
	TrailLength    uint16
	TrailFrequency uint16
	Visible        uint8
	Cursor         *Cursor
}

func UnmarshalMsgCursorInit(r io.Reader) (*MsgCursorInit, error) {
	m := &MsgCursorInit{}
	if err := binary.Read(r, binary.LittleEndian, &m.Position); err != nil {
		return nil, fmt.Errorf("unmarshal Position: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.TrailLength); err != nil {
		return nil, fmt.Errorf("unmarshal TrailLength: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.TrailFrequency); err != nil {
		return nil, fmt.Errorf("unmarshal TrailFrequency: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Visible); err != nil {
		return nil, fmt.Errorf("unmarshal Visible: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Cursor); err != nil {
		return nil, fmt.Errorf("unmarshal Cursor: %w", err)
	}
	return m, nil
}
func (m *MsgCursorInit) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.Position); err != nil {
		return nil, fmt.Errorf("marshal Position: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.TrailLength); err != nil {
		return nil, fmt.Errorf("marshal TrailLength: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.TrailFrequency); err != nil {
		return nil, fmt.Errorf("marshal TrailFrequency: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Visible); err != nil {
		return nil, fmt.Errorf("marshal Visible: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Cursor); err != nil {
		return nil, fmt.Errorf("marshal Cursor: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgCursorReset struct{}
type MsgCursorSet struct {
	Position *Point16
	Visible  uint8
	Cursor   *Cursor
}

func UnmarshalMsgCursorSet(r io.Reader) (*MsgCursorSet, error) {
	m := &MsgCursorSet{}
	if err := binary.Read(r, binary.LittleEndian, &m.Position); err != nil {
		return nil, fmt.Errorf("unmarshal Position: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Visible); err != nil {
		return nil, fmt.Errorf("unmarshal Visible: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Cursor); err != nil {
		return nil, fmt.Errorf("unmarshal Cursor: %w", err)
	}
	return m, nil
}
func (m *MsgCursorSet) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.Position); err != nil {
		return nil, fmt.Errorf("marshal Position: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Visible); err != nil {
		return nil, fmt.Errorf("marshal Visible: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Cursor); err != nil {
		return nil, fmt.Errorf("marshal Cursor: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgCursorMove struct {
	Position *Point16
}

func UnmarshalMsgCursorMove(r io.Reader) (*MsgCursorMove, error) {
	m := &MsgCursorMove{}
	if err := binary.Read(r, binary.LittleEndian, &m.Position); err != nil {
		return nil, fmt.Errorf("unmarshal Position: %w", err)
	}
	return m, nil
}
func (m *MsgCursorMove) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.Position); err != nil {
		return nil, fmt.Errorf("marshal Position: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgCursorHide struct{}
type MsgCursorTrail struct {
	Length    uint16
	Frequency uint16
}

func UnmarshalMsgCursorTrail(r io.Reader) (*MsgCursorTrail, error) {
	m := &MsgCursorTrail{}
	if err := binary.Read(r, binary.LittleEndian, &m.Length); err != nil {
		return nil, fmt.Errorf("unmarshal Length: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Frequency); err != nil {
		return nil, fmt.Errorf("unmarshal Frequency: %w", err)
	}
	return m, nil
}
func (m *MsgCursorTrail) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.Length); err != nil {
		return nil, fmt.Errorf("marshal Length: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Frequency); err != nil {
		return nil, fmt.Errorf("marshal Frequency: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgCursorInvalOne struct {
	Id uint64
}

func UnmarshalMsgCursorInvalOne(r io.Reader) (*MsgCursorInvalOne, error) {
	m := &MsgCursorInvalOne{}
	if err := binary.Read(r, binary.LittleEndian, &m.Id); err != nil {
		return nil, fmt.Errorf("unmarshal Id: %w", err)
	}
	return m, nil
}
func (m *MsgCursorInvalOne) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.Id); err != nil {
		return nil, fmt.Errorf("marshal Id: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgCursorInvalAll struct{}
type AudioDataMode uint16

const (
	AudioDataModeInvalid AudioDataMode = iota
	AudioDataModeRaw
	// AudioDataModeCelt051 is deprecated
	//
	// Deprecated: AudioDataModeCelt051 is deprecated and keept for back compatibility
	AudioDataModeCelt051
	AudioDataModeOpus
)

type AudioFmt uint16

const (
	AudioFmtInvalid AudioFmt = iota
	AudioFmtS16
)

type AudioVolume struct {
	Nchannels uint8
	Volume    []uint16
}

func UnmarshalAudioVolume(r io.Reader) (*AudioVolume, error) {
	a := &AudioVolume{}
	if err := binary.Read(r, binary.LittleEndian, &a.Nchannels); err != nil {
		return nil, fmt.Errorf("unmarshal Nchannels: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &a.Volume); err != nil {
		return nil, fmt.Errorf("unmarshal Volume: %w", err)
	}
	return a, nil
}
func (a *AudioVolume) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &a.Nchannels); err != nil {
		return nil, fmt.Errorf("marshal Nchannels: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &a.Volume); err != nil {
		return nil, fmt.Errorf("marshal Volume: %w", err)
	}
	return buf.Bytes(), nil
}

type AudioMute struct {
	Mute uint8
}

func UnmarshalAudioMute(r io.Reader) (*AudioMute, error) {
	a := &AudioMute{}
	if err := binary.Read(r, binary.LittleEndian, &a.Mute); err != nil {
		return nil, fmt.Errorf("unmarshal Mute: %w", err)
	}
	return a, nil
}
func (a *AudioMute) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &a.Mute); err != nil {
		return nil, fmt.Errorf("marshal Mute: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgPlaybackData struct {
	Time uint32
	Data []uint8
}

func UnmarshalMsgPlaybackData(r io.Reader) (*MsgPlaybackData, error) {
	m := &MsgPlaybackData{}
	if err := binary.Read(r, binary.LittleEndian, &m.Time); err != nil {
		return nil, fmt.Errorf("unmarshal Time: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Data); err != nil {
		return nil, fmt.Errorf("unmarshal Data: %w", err)
	}
	return m, nil
}
func (m *MsgPlaybackData) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.Time); err != nil {
		return nil, fmt.Errorf("marshal Time: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Data); err != nil {
		return nil, fmt.Errorf("marshal Data: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgPlaybackMode struct {
	Time uint32
	Mode *AudioDataMode
	Data []uint8
}

func UnmarshalMsgPlaybackMode(r io.Reader) (*MsgPlaybackMode, error) {
	m := &MsgPlaybackMode{}
	if err := binary.Read(r, binary.LittleEndian, &m.Time); err != nil {
		return nil, fmt.Errorf("unmarshal Time: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Mode); err != nil {
		return nil, fmt.Errorf("unmarshal Mode: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Data); err != nil {
		return nil, fmt.Errorf("unmarshal Data: %w", err)
	}
	return m, nil
}
func (m *MsgPlaybackMode) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.Time); err != nil {
		return nil, fmt.Errorf("marshal Time: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Mode); err != nil {
		return nil, fmt.Errorf("marshal Mode: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Data); err != nil {
		return nil, fmt.Errorf("marshal Data: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgPlaybackStart struct {
	Channels  uint32
	Format    *AudioFmt
	Frequency uint32
	Time      uint32
}

func UnmarshalMsgPlaybackStart(r io.Reader) (*MsgPlaybackStart, error) {
	m := &MsgPlaybackStart{}
	if err := binary.Read(r, binary.LittleEndian, &m.Channels); err != nil {
		return nil, fmt.Errorf("unmarshal Channels: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Format); err != nil {
		return nil, fmt.Errorf("unmarshal Format: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Frequency); err != nil {
		return nil, fmt.Errorf("unmarshal Frequency: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Time); err != nil {
		return nil, fmt.Errorf("unmarshal Time: %w", err)
	}
	return m, nil
}
func (m *MsgPlaybackStart) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.Channels); err != nil {
		return nil, fmt.Errorf("marshal Channels: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Format); err != nil {
		return nil, fmt.Errorf("marshal Format: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Frequency); err != nil {
		return nil, fmt.Errorf("marshal Frequency: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Time); err != nil {
		return nil, fmt.Errorf("marshal Time: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgPlaybackStop struct{}
type MsgPlaybackVolume struct{}
type MsgPlaybackMute struct{}
type MsgPlaybackLatency struct {
	LatencyMs uint32
}

func UnmarshalMsgPlaybackLatency(r io.Reader) (*MsgPlaybackLatency, error) {
	m := &MsgPlaybackLatency{}
	if err := binary.Read(r, binary.LittleEndian, &m.LatencyMs); err != nil {
		return nil, fmt.Errorf("unmarshal LatencyMs: %w", err)
	}
	return m, nil
}
func (m *MsgPlaybackLatency) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.LatencyMs); err != nil {
		return nil, fmt.Errorf("marshal LatencyMs: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgRecordStart struct {
	Channels  uint32
	Format    *AudioFmt
	Frequency uint32
}

func UnmarshalMsgRecordStart(r io.Reader) (*MsgRecordStart, error) {
	m := &MsgRecordStart{}
	if err := binary.Read(r, binary.LittleEndian, &m.Channels); err != nil {
		return nil, fmt.Errorf("unmarshal Channels: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Format); err != nil {
		return nil, fmt.Errorf("unmarshal Format: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Frequency); err != nil {
		return nil, fmt.Errorf("unmarshal Frequency: %w", err)
	}
	return m, nil
}
func (m *MsgRecordStart) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.Channels); err != nil {
		return nil, fmt.Errorf("marshal Channels: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Format); err != nil {
		return nil, fmt.Errorf("marshal Format: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Frequency); err != nil {
		return nil, fmt.Errorf("marshal Frequency: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgRecordStop struct{}
type MsgRecordVolume struct{}
type MsgRecordMute struct{}
type VscMessageType uint32

const (
	VscMessageTypeInit VscMessageType = iota + 1
	VscMessageTypeError
	VscMessageTypeReaderadd
	VscMessageTypeReaderremove
	VscMessageTypeAtr
	VscMessageTypeCardremove
	VscMessageTypeApdu
	VscMessageTypeFlush
	VscMessageTypeFlushcomplete
)

type VscMessageHeader struct {
	Type     *VscMessageType
	ReaderId uint32
	Length   uint32
}
type VscMessageError struct {
	Code uint32
}
type VscMessageAPDU struct {
	Data []uint8
}
type VscMessageATR struct {
	Atr []uint8
}
type VscMessageReaderAdd struct {
	Name []int8
}
type MsgSmartcardData struct {
	Type     *VscMessageType
	ReaderId uint32
	Length   uint32
	Data     []uint8
}

func UnmarshalMsgSmartcardData(r io.Reader) (*MsgSmartcardData, error) {
	m := &MsgSmartcardData{}
	if err := binary.Read(r, binary.LittleEndian, &m.Type); err != nil {
		return nil, fmt.Errorf("unmarshal Type: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.ReaderId); err != nil {
		return nil, fmt.Errorf("unmarshal ReaderId: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Length); err != nil {
		return nil, fmt.Errorf("unmarshal Length: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Data); err != nil {
		return nil, fmt.Errorf("unmarshal Data: %w", err)
	}
	return m, nil
}
func (m *MsgSmartcardData) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.Type); err != nil {
		return nil, fmt.Errorf("marshal Type: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.ReaderId); err != nil {
		return nil, fmt.Errorf("marshal ReaderId: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Length); err != nil {
		return nil, fmt.Errorf("marshal Length: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Data); err != nil {
		return nil, fmt.Errorf("marshal Data: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgSpicevmcData struct{}
type MsgSpicevmcCompressedData struct{}
type MsgPortInit struct {
	NameSize uint32
	Name     []uint8
	Opened   uint8
}

func UnmarshalMsgPortInit(r io.Reader) (*MsgPortInit, error) {
	m := &MsgPortInit{}
	if err := binary.Read(r, binary.LittleEndian, &m.NameSize); err != nil {
		return nil, fmt.Errorf("unmarshal NameSize: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Name); err != nil {
		return nil, fmt.Errorf("unmarshal Name: %w", err)
	}
	if err := binary.Read(r, binary.LittleEndian, &m.Opened); err != nil {
		return nil, fmt.Errorf("unmarshal Opened: %w", err)
	}
	return m, nil
}
func (m *MsgPortInit) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.NameSize); err != nil {
		return nil, fmt.Errorf("marshal NameSize: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Name); err != nil {
		return nil, fmt.Errorf("marshal Name: %w", err)
	}
	if err := binary.Write(buf, binary.LittleEndian, &m.Opened); err != nil {
		return nil, fmt.Errorf("marshal Opened: %w", err)
	}
	return buf.Bytes(), nil
}

type MsgPortEvent struct {
	Event uint8
}

func UnmarshalMsgPortEvent(r io.Reader) (*MsgPortEvent, error) {
	m := &MsgPortEvent{}
	if err := binary.Read(r, binary.LittleEndian, &m.Event); err != nil {
		return nil, fmt.Errorf("unmarshal Event: %w", err)
	}
	return m, nil
}
func (m *MsgPortEvent) Marshal() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := binary.Write(buf, binary.LittleEndian, &m.Event); err != nil {
		return nil, fmt.Errorf("marshal Event: %w", err)
	}
	return buf.Bytes(), nil
}

type ImageDescriptor struct {
	Id     uint64
	Type   *ImageType
	Flags  *ImageFlags
	Width  uint32
	Height uint32
}
type Fill struct {
	Brush         *Brush
	RopDescriptor *Ropd
	Mask          *QMask
}
type Opaque struct {
	SrcBitmap     *Image
	SrcArea       *Rect
	Brush         *Brush
	RopDescriptor *Ropd
	ScaleMode     *ImageScaleMode
	Mask          *QMask
}
type Copy struct {
	SrcBitmap     *Image
	SrcArea       *Rect
	RopDescriptor *Ropd
	ScaleMode     *ImageScaleMode
	Mask          *QMask
}
type Blend struct {
	SrcBitmap     *Image
	SrcArea       *Rect
	RopDescriptor *Ropd
	ScaleMode     *ImageScaleMode
	Mask          *QMask
}
type Blackness struct {
	Mask *QMask
}
type Whiteness struct {
	Mask *QMask
}
type Invers struct {
	Mask *QMask
}
type Rop3 struct {
	SrcBitmap *Image
	SrcArea   *Rect
	Brush     *Brush
	Rop3      uint8
	ScaleMode *ImageScaleMode
	Mask      *QMask
}
type Stroke struct {
	Path     *Path
	Attr     *LineAttr
	Brush    *Brush
	ForeMode uint16
	BackMode uint16
}
type Text struct {
	Str       *String
	BackArea  *Rect
	ForeBrush *Brush
	BackBrush *Brush
	ForeMode  uint16
	BackMode  uint16
}
type Transparent struct {
	SrcBitmap *Image
	SrcArea   *Rect
	SrcColor  uint32
	TrueColor uint32
}
type AlphaBlend struct {
	AlphaFlags *AlphaFlags
	Alpha      uint8
	SrcBitmap  *Image
	SrcArea    *Rect
}
type Composite struct {
	Flags      *CompositeFlags
	SrcBitmap  *Image
	SrcOrigin  *Point16
	MaskOrigin *Point16
}
