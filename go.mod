module gitlab.com/nefix/spice-codegen

go 1.19

require (
	github.com/alecthomas/participle v0.7.1
	github.com/dave/jennifer v1.6.1
	github.com/iancoleman/strcase v0.2.0
)

require github.com/stretchr/testify v1.5.1 // indirect
