package generator

// TODO: EMPTY SWITCH CASES JUMP TO THE NEXT!

import (
	"bytes"
	"fmt"
	"strconv"
	"strings"

	"github.com/iancoleman/strcase"
	"gitlab.com/nefix/spice-codegen/parser"
)

const (
	sizeMessageSize = -1
	sizeArrayLength = -2
	sizeByteLength  = -3
	sizeImageSize   = -4
	sizeInfinite    = -5
)

type TypeScript struct {
	buf *bytes.Buffer
}

func NewTypescript() *TypeScript {
	buf := &bytes.Buffer{}
	buf.WriteString("// @ts-ignore\n")
	return &TypeScript{
		buf: buf,
	}
}

var generatedTypes = map[string]sizeS{}

type sizeS struct {
	Size   int
	Type   string
	Struct *parser.Struct
}

func (TypeScript) Directory() string {
	return "ts"
}

func (TypeScript) File() string {
	return "spice.ts"
}
func (t *TypeScript) Render() []byte {
	return t.buf.Bytes()
}

func (TypeScript) parseType(t string) string {
	switch t {
	case "int8", "uint8", "int16", "uint16", "int32", "uint32", "unix_fd":
		return "number"

	case "uint64", "int64":
		return "bigint"

	default:
		return strcase.ToCamel(t)
	}
}

func (TypeScript) typeSize(t string) int {
	switch t {
	case "uint8", "int8", "enum8", "flags8":
		return 1

	case "uint16", "int16", "enum16", "flags16":
		return 2

	case "uint32", "int32", "fixed28_4", "enum32", "flags32", "unix_fd":
		return 4

	case "uint64", "int64":
		return 8

	default:
		if s, ok := generatedTypes[t]; ok {
			return s.Size
		}

		panic("UNSUPPORTED TYPE SIZE: " + t)
	}
}

func (ts *TypeScript) Type(t *parser.Type) {
	ts.Struct(&parser.Struct{
		Name: t.Name,
		Fields: []*parser.Field{{
			Variable: &parser.Variable{
				Name: "value",
				Type: t.Type,
			},
		}},
		Attributes: t.Attributes,
	})
}

func (t *TypeScript) Struct(s *parser.Struct) {
	// Check if the struct declares types
	for _, f := range s.Fields {
		if f.Type() == "struct" {
			t.Struct(f.Struct)
		}
	}

	// Rename duplicated field names
	t.repeatedFields(s.Fields)

	// STRUCT FIELDS
	sName := t.structName(s.Name)
	fmt.Fprintf(t.buf, "export class %s {\n", sName)

	for _, f := range s.Fields {
		t.field(f)
	}

	// MESSAGE SIZE
	size := t.structSize(&parser.Struct{Fields: s.Fields})
	switch size {
	case sizeMessageSize:
		fmt.Fprintln(t.buf, "\n  get messageSize() {")
		fmt.Fprintln(t.buf, "    let size = 0")

	fields:
		for _, f := range s.Fields {
			switch f.Type() {
			case "variable":
				for _, a := range f.Variable.Attributes {
					if a.Name == "as_ptr" || a.Name == "chunk" {
						continue fields
					}
				}

				s := t.variableSize(f.Variable)

				switch s {
				case sizeMessageSize:
					fmt.Fprintf(t.buf, "    size += this.%s.messageSize\n", t.fieldName(f.Variable.Name))

				case sizeArrayLength:
					fmt.Fprintf(t.buf, "    size += %d * this.%s\n", t.typeSize(f.Variable.Type), t.fieldName(f.Variable.Size.Identifier))

				case sizeByteLength:
					fmt.Fprintf(t.buf, "    size += this.%s!.byteLength\n", t.fieldName(f.Variable.Name))

				default:
					fmt.Fprintf(t.buf, "    size += %d\n", s)
				}

			case "struct":
				v := &parser.Variable{
					Name:       f.StructName,
					Type:       f.Struct.Name,
					Attributes: f.Struct.Attributes,
				}
				s := t.variableSize(v)

				switch s {
				case sizeMessageSize:
					fmt.Fprintf(t.buf, "    size += this.%s.messageSize\n", t.fieldName(v.Name))

				case sizeArrayLength:
					fmt.Fprintf(t.buf, "    size += %d * this.%s\n", t.typeSize(v.Type), t.fieldName(v.Size.Identifier))

				case sizeByteLength:
					fmt.Fprintf(t.buf, "    size += %d * this.%s.byteLength\n", t.typeSize(v.Type), t.fieldName(v.Size.Identifier))

				case sizeImageSize:
					fmt.Fprintf(t.buf, "    size += %s\n", t.imageSize(true, v.Size.ImageSize.Height, v.Size.ImageSize.Width, v.Size.ImageSize.Size))

				// TODO: -5

				default:
					fmt.Fprintf(t.buf, "    size += %d\n", s)
				}

			case "switch":
				t.doSwitching(s.Fields, f.Switch.Name, true, func(c *parser.SwitchCase) {
					for _, v := range c.Variables {
						vSize := t.variableSize(v)

						// TODO: Opaque example

						switch vSize {
						case sizeMessageSize:
							fmt.Fprintf(t.buf, "        size += this.%s!.messageSize\n", t.fieldName(v.Name))

						case sizeArrayLength:
							fmt.Fprintf(t.buf, "        size += 0 // TODO: FIX THIS!\n")

						case sizeByteLength:
							fmt.Fprintf(t.buf, "        size += this.%s.byteLength\n", t.fieldName(v.Name))

						// TODO: -5

						default:
							fmt.Fprintf(t.buf, "        size += %d\n", vSize)
						}
					}
				}, func(c *parser.SwitchCase) {})

			default:
				panic("UNSUPPORTED FIELD TYPE: " + f.Type())
			}
		}

		fmt.Fprintln(t.buf, "    return size")
		fmt.Fprintln(t.buf, "  }")

	case sizeInfinite:

	default:
		fmt.Fprintf(t.buf, "\n  static messageSize = %d\n", size)
	}

	// CONSTRUCTOR

	args := ""
	inside := ""

	for _, f := range s.Fields {
		switch f.Type() {
		case "variable":
			vArg, vInside := t.constructorVariables(f.Variable, false)
			args += vArg
			inside += vInside

		case "switch":
			for _, c := range f.Switch.Cases {
				for _, v := range c.Variables {
					vArg, vInside := t.constructorVariables(v, true)
					args += vArg
					inside += vInside
				}
			}

			if f.Switch.Default != nil {
				for _, v := range f.Switch.Default.Variables {
					vArg, vInside := t.constructorVariables(v, true)
					args += vArg
					inside += vInside
				}
			}

		case "struct":
			vArg, vInside := t.constructorVariables(&parser.Variable{
				Name:       f.StructName,
				Type:       f.Struct.Name,
				Attributes: f.Struct.Attributes,
			}, false)
			args += vArg
			inside += vInside

		default:
			panic(fmt.Sprintf("UNSUPPORTED STRUCT TYPE: %s", f.Type()))
		}
	}
	// remove the last ", "
	if len(args) != 0 {
		args = args[:len(args)-2]
	}
	fmt.Fprintf(t.buf, "\n  constructor(%s) {\n", args)
	fmt.Fprint(t.buf, inside)
	fmt.Fprintln(t.buf, "  }")

	// UNMARSHAL
	switch size {
	case sizeMessageSize:
		fmt.Fprintf(t.buf, "\n  static unmarshal(buf: ArrayBuffer, offset?: number, expSize?: number, ): %s {\n", sName)
		fmt.Fprintln(t.buf, `    const expectedSize = expSize`)
		fmt.Fprintln(t.buf, "    if (expectedSize !== undefined && buf.byteLength < expectedSize) {")
		fmt.Fprintln(t.buf, "      console.trace()")
		fmt.Fprintln(t.buf, `      throw "not enough queue to be read"`)
		fmt.Fprintln(t.buf, "    }")

	case sizeInfinite:
		fmt.Fprintf(t.buf, "\n  static unmarshal(buf: ArrayBuffer, expectedSize: number, offset?: number): %s {\n", sName)
		fmt.Fprintln(t.buf, "    if (buf.byteLength < expectedSize) {")
		fmt.Fprintln(t.buf, "      console.trace()")
		fmt.Fprintln(t.buf, `      throw "not enough queue to be read"`)
		fmt.Fprintln(t.buf, "    }")

	default:
		fmt.Fprintf(t.buf, "\n  static unmarshal(buf: ArrayBuffer, offset?: number): %s {\n", sName)
		fmt.Fprintln(t.buf, `    const expectedSize = this.messageSize`)
		fmt.Fprintln(t.buf, "    if (buf.byteLength < expectedSize) {")
		fmt.Fprintln(t.buf, "      console.trace()")
		fmt.Fprintln(t.buf, `      throw "not enough queue to be read"`)
		fmt.Fprintln(t.buf, "    }")
	}
	fmt.Fprintln(t.buf, "\n    const view = new DataView(buf)")
	fmt.Fprintln(t.buf, "    if (offset === undefined) {")
	fmt.Fprintln(t.buf, "      offset = 0")
	fmt.Fprintln(t.buf, "    }")
	fmt.Fprintln(t.buf, "    const origOffset = offset")
	fmt.Fprintln(t.buf)

	for _, f := range s.Fields {
		switch f.Type() {
		case "variable":
			t.unmarshalVariable(f.Variable, false, false)

		case "struct":
			t.unmarshalVariable(&parser.Variable{
				Name:       f.StructName,
				Type:       f.Struct.Name,
				Attributes: f.Struct.Attributes,
			}, false, false)

		case "switch":
			t.doSwitching(s.Fields, f.Switch.Name, false, func(c *parser.SwitchCase) {
				for _, v := range c.Variables {
					t.unmarshalVariable(v, false, true)
				}
			}, func(c *parser.SwitchCase) {
				for _, v := range c.Variables {
					fmt.Fprintf(t.buf, "    let %s\n", t.fieldName(v.Name))
				}
			})

		default:
			panic("UNSUPPORTED MESSAGE TYPE: " + f.Type())
		}
	}

	fNames := []string{}
	for _, f := range s.Fields {
		switch f.Type() {
		case "variable", "struct":
			fNames = append(fNames, t.fieldName(f.Name()))

		case "switch":
			for _, c := range f.Switch.Cases {
				for _, v := range c.Variables {
					fNames = append(fNames, t.fieldName(v.Name))
				}
			}

			if f.Switch.Default != nil {
				for _, v := range f.Switch.Default.Variables {
					fNames = append(fNames, t.fieldName(v.Name))
				}
			}

		default:
			panic("UNSUPPORTED VARIABLE TYPE: " + f.Type())
		}
	}

	fmt.Fprintf(t.buf, "    return new %s(%s)\n", sName, strings.Join(fNames, ", "))
	fmt.Fprintln(t.buf, "  }")

	// MARSHAL
	fmt.Fprintln(t.buf, "\n  marshal(): ArrayBuffer {")

	switch size {
	case sizeMessageSize:
		fmt.Fprintln(t.buf, "    const buf = new ArrayBuffer(this.messageSize)")

	// TODO: -3, -5

	case sizeInfinite:
		fmt.Fprintln(t.buf, "    const buf = new ArrayBuffer(0) // TODO: This should be the ininite data size")

	default:
		fmt.Fprintf(t.buf, "    const buf = new ArrayBuffer(%s.messageSize)\n", sName)
	}

	fmt.Fprintln(t.buf, "    const view = new DataView(buf)")
	fmt.Fprintln(t.buf, "    const slice = new Uint8Array(buf)")
	fmt.Fprintln(t.buf, "    let offset = 0")
	fmt.Fprintln(t.buf)

	for _, f := range s.Fields {
		switch f.Type() {
		case "variable":
			t.marshalVariable(f.Variable, false, true)

		case "struct":
			t.marshalVariable(&parser.Variable{
				Name:       f.StructName,
				Type:       f.Struct.Name,
				Attributes: f.Struct.Attributes,
			}, false, true)

		case "switch":
			t.doSwitching(s.Fields, f.Switch.Name, true, func(c *parser.SwitchCase) {
				for _, v := range c.Variables {
					t.marshalVariable(v, true, true)
				}
			}, func(c *parser.SwitchCase) {})

		default:
			panic("UNSUPPORTED MESSAGE TYPE: " + f.Type())
		}
	}

	fmt.Fprintln(t.buf, "\n    return buf")
	fmt.Fprintln(t.buf, "  }")

	fmt.Fprintln(t.buf, "}")

	generatedTypes[s.Name] = sizeS{
		Size:   size,
		Type:   "struct",
		Struct: s,
	}
}

func (t TypeScript) structSize(s *parser.Struct) int {
	size := 0

	for _, f := range s.Fields {
		s := t.fieldSize(f)
		switch s {
		case sizeMessageSize, sizeArrayLength, sizeByteLength:
			return sizeMessageSize

		case sizeInfinite:
			return sizeInfinite
		}

		size += s
	}

	return size
}

func (t TypeScript) structName(s string) string {
	return strcase.ToCamel(s)
}

func (t TypeScript) hasSwitch(f []*parser.Field) bool {
	for _, field := range f {
		if field.Switch != nil {
			return true

		} else if field.Struct != nil {
			if t.hasSwitch(field.Struct.Fields) {
				return true
			}

		} else if field.Variable != nil {
			if g, ok := generatedTypes[field.Variable.Type]; ok {
				if g.Size == sizeMessageSize {
					return true
				}
			}
		}
	}

	return false
}

func (t TypeScript) repeatedFields(fields []*parser.Field) {
	iteratedFields := []string{}
	repeatedFields := []string{}

	// Find repeated fields
	for _, f := range fields {
		for _, eF := range iteratedFields {
			if f.Name() == eF {
				repeatedFields = append(repeatedFields, f.Name())
			}
		}

		if f.Type() == "switch" {
			for _, c := range f.Switch.Cases {
				for _, v := range c.Variables {
					for _, eF := range iteratedFields {
						if v.Name == eF {
							repeatedFields = append(repeatedFields, v.Name)
						}
					}

					iteratedFields = append(iteratedFields, v.Name)
				}
			}

			if f.Switch.Default != nil {
				for _, v := range f.Switch.Default.Variables {
					for _, eF := range iteratedFields {
						if v.Name == eF {
							repeatedFields = append(repeatedFields, v.Name)
						}
					}

					iteratedFields = append(iteratedFields, v.Name)
				}
			}
		}

		iteratedFields = append(iteratedFields, f.Name())
	}

	// Rename them
	for _, f := range fields {
		for _, repeated := range repeatedFields {
			if f.Name() == repeated {
				typ := ""
				switch f.Type() {
				case "variable":
					typ = f.Variable.Type

				case "struct":
					if f.StructName != "" {
						typ = f.StructName
						break
					}

					typ = "Struct"

				default:
					typ = f.Type()
				}

				f.SetName(f.Name() + "_" + typ)
			}

			if f.Type() == "switch" {
				for _, c := range f.Switch.Cases {
					for _, v := range c.Variables {
						for _, repeated := range repeatedFields {
							if v.Name == repeated {
								v.Name = v.Name + "_" + v.Type
							}
						}
					}
				}

				if f.Switch.Default != nil {
					for _, v := range f.Switch.Default.Variables {
						for _, repeated := range repeatedFields {
							if v.Name == repeated {
								v.Name = v.Name + "_" + v.Type
							}
						}
					}
				}
			}
		}
	}
}

func (t TypeScript) avoidRepeatedFields(fields []string) []string {
	iteratedFields := []string{}
	repeatedFields := []string{}

	for _, f := range fields {
		for _, eF := range iteratedFields {
			if f == eF {
				repeatedFields = append(repeatedFields, f)
			}
		}

		iteratedFields = append(iteratedFields, f)
	}

	return repeatedFields
}

func (t *TypeScript) constructorVariables(v *parser.Variable, optional bool) (string, string) {
	if v.Pointer {
		optional = true
	}

	for _, a := range v.Attributes {
		switch a.Name {
		case "as_ptr", "chunk":
			if v.Type != "uint8" && v.Type != "ArrayBuffer" {
				panic("UNSUPPORTED " + strings.ToUpper(a.Name) + " TYPE: " + v.Type)
			}

			optional = true
		}
	}

	fName := t.fieldName(v.Name)

	optStr := ""
	if optional {
		optStr = " | undefined"
	}

	arg := fmt.Sprintf("%s: %s%s, ", fName, t.variableType(v, false), optStr)
	inside := fmt.Sprintf("    this.%s = %s\n", fName, fName)

	return arg, inside
}

func (t *TypeScript) unmarshalVariable(v *parser.Variable, nullable bool, predefined bool) {
	asPtr := false
	chunk := false
	littleE := true
	for _, a := range v.Attributes {
		switch a.Name {
		case "as_ptr":
			asPtr = true

		case "chunk":
			chunk = true

		case "big_endian":
			littleE = false
		}
	}

	fName := t.fieldName(v.Name)
	tName := t.parseType(v.Type)

	// TODO: I THINK THIS IS NOT OK!
	if nullable {
		fName += "!"
	}

	if asPtr {
		if v.Type != "uint8" && v.Type != "ArrayBuffer" {
			panic("UNSUPPORTED AS_PTR TYPE: " + v.Type)
		}
		fmt.Fprintf(t.buf, "    const %s = buf.slice(origOffset)\n", fName)

	} else if chunk {
		if v.Type != "uint8" && v.Type != "ArrayBuffer" {
			panic("UNSUPPORTED CHUNK TYPE: " + v.Type)
		}

		fmt.Fprintf(t.buf, "    const %s = buf.slice(offset)\n", fName)

	} else if v.Pointer {
		pointName := t.fieldName(v.Name + "_" + "Ptr")
		t.unmarshalVariable(&parser.Variable{
			Type:       "uint32",
			Pointer:    false,
			Name:       pointName,
			Size:       nil,
			Attributes: nil,
		}, false, false)
		fmt.Fprintf(t.buf, "    let %s: %s | undefined = undefined\n", fName, tName)
		fmt.Fprintf(t.buf, "    if (%s !== 0) {\n", pointName)
		fmt.Fprintf(t.buf, "        %s = %s.unmarshal(buf, Number(%s))\n", fName, tName, pointName)
		fmt.Fprintln(t.buf, "    }")

	} else if v.Size != nil {
		length := ""
		if v.Size.Int != nil {
			length = strconv.Itoa(*v.Size.Int)

		} else if v.Size.Identifier != "" {
			length = t.fieldName(v.Size.Identifier)

		} else if v.Size.ImageSize != nil {
			length = t.imageSize(false, v.Size.ImageSize.Height, v.Size.ImageSize.Width, v.Size.ImageSize.Size)

		} else {
			length = "expectedSize - offset"
		}

		fmt.Fprintf(t.buf, "    let %s: %s[] = []\n", fName, t.parseType(v.Type))
		fmt.Fprintf(t.buf, "    for (let i = 0; i < %s; i++) {\n", length)

		sName := t.fieldName(v.Type)
		t.unmarshalVariable(&parser.Variable{
			Type: v.Type,
			Name: sName,
			// TODO: Size!
			// Size: s.Size,
		}, false, false)

		// ONLY ON IDENTIFIER!
		// fmt.Fprintf(t.buf, "        %s = %s.unmarshal(buf.slice(offset, %s.messageSize))\n", fName, tName, tName)
		// fmt.Fprintf(t.buf, "        offset += %s.messageSize\n", tName)

		fmt.Fprintf(t.buf, "    %s.push(%s)\n", fName, sName)
		fmt.Fprintln(t.buf, "    }")

	} else {
		switch v.Type {
		case "uint8", "uint16", "uint32", "uint64", "int16", "int32", "int64", "unix_fd":
			t.unmarshalSize(fName, v.Type, predefined, littleE)
			fmt.Fprintf(t.buf, "    offset += %d\n", t.typeSize(v.Type))

		default:
			// If it's an enum or a flag, it's always going to be a uint
			s := generatedTypes[v.Type]
			if s.Type == "enum" {
				eType := "uint" + strconv.Itoa(s.Size*8)
				t.unmarshalSize(fName, eType, predefined, littleE)
				fmt.Fprintf(t.buf, "    offset += %d\n", t.typeSize(eType))
				break
			}

			if s.Type == "struct" {
				if t.structSize(s.Struct) == sizeMessageSize {
					constPrefix := ""
					if !predefined {
						constPrefix = "const "
					}
					fmt.Fprintf(t.buf, "    %s%s = %s.unmarshal(buf, offset)\n", constPrefix, fName, tName)
					fmt.Fprintf(t.buf, "    offset += %s.messageSize\n", fName)
					break
				}
			}

			fmt.Fprintf(t.buf, "    const %s = %s.unmarshal(buf, offset)\n", fName, tName)
			fmt.Fprintf(t.buf, "    offset += %s.messageSize\n", tName)
		}
	}
}

func (ts *TypeScript) unmarshalSize(name, t string, predefined bool, littleEndian bool) {
	constPrefix := ""
	if !predefined {
		constPrefix = "const "
	}

	switch t {
	case "uint8":
		fmt.Fprintf(ts.buf, "    %s%s = view.getUint8(offset)\n", constPrefix, name)

	case "uint16":
		fmt.Fprintf(ts.buf, "    %s%s = view.getUint16(offset, %t)\n", constPrefix, name, littleEndian)

	case "uint32":
		fmt.Fprintf(ts.buf, "    %s%s = view.getUint32(offset, %t)\n", constPrefix, name, littleEndian)

	case "uint64":
		fmt.Fprintf(ts.buf, "    %s%s = view.getBigUint64(offset, %t)\n", constPrefix, name, littleEndian)

	case "int16":
		fmt.Fprintf(ts.buf, "    %s%s = view.getInt16(offset, %t)\n", constPrefix, name, littleEndian)

	case "int32", "unix_fd":
		fmt.Fprintf(ts.buf, "    %s%s = view.getInt32(offset, %t)\n", constPrefix, name, littleEndian)

	case "int64":
		fmt.Fprintf(ts.buf, "    %s%s = view.getBigInt64(offset, %t)\n", constPrefix, name, littleEndian)

	default:
		panic("UNKNOWN UNMARSHAL SIZE " + t)
	}
}

func (t *TypeScript) marshalVariable(v *parser.Variable, optional bool, this bool) {
	fName := t.fieldName(v.Name)
	if optional {
		fName += "!"
	}

	thisPrefix := ""
	if this {
		thisPrefix = "this."
	}

	if v.Pointer {
		fmt.Fprintln(t.buf, "    view.setUint32(offset, 0, true) // TODO: This!")
		fmt.Fprintln(t.buf, "    offset += 8")
		return
	}

	chunk := false
	littleE := true
	for _, a := range v.Attributes {
		switch a.Name {
		case "as_ptr", "chunk":
			// fmt.Fprintf(t.buf, "    slice.set(%s%s!, offset)\n", thisPrefix, fName)
			return

		case "big_endian":
			littleE = false

			// case "chunk":
			// 	// chunk = true
		}
	}

	if chunk {
		// TODO: THIS!

	} else if v.Size != nil {
		if v.Size.Int != nil || v.Size.Identifier != "" {
			sName := t.fieldName(v.Type)
			fmt.Fprintf(t.buf, "    %s%s.forEach((%s) => {\n", thisPrefix, fName, sName)
			t.marshalVariable(&parser.Variable{
				Type: v.Type,
				Name: sName,
				// TODO: Size!
				// Size: s.Size,
			}, false, false)
			fmt.Fprintln(t.buf, "    })")

			// fmt.Fprintf(t.buf, "        %s = %s.unmarshal(buf.slice(offset, %s.messageSize))\n", fName, tName, tName)
			// fmt.Fprintf(t.buf, "        offset += %s.messageSize\n", tName)
		} else {
			// TODO: Image Size!
		}
		// TODO: THIS!
		// switch f.Variable.Type {
		// case "uint8":
		// 	fmt.Fprintf("    slice.set(this.%s, %d)\n", strcase.ToLowerCamel(f.Variable.Name), offset)

		// case "uint32":
		// 	fmt.Fprintf("    uint32Array.set(this.%s, %d)\n", strcase.ToLowerCamel(f.Variable.Name), offset)

		// default:
		// 	fmt.Fprintln(f.Variable.Type)
		// 	fmt.Fprintf("    slice.set(new Uint8Array(this.%s.marshal()), %d)\n", strcase.ToLowerCamel(f.Variable.Name), offset)
		// }

	} else {
		switch v.Type {
		case "uint8", "uint16", "uint32", "uint64", "int16", "int32", "int64", "unix_fd":
			t.marshalSize(fName, v.Type, this, littleE)
			fmt.Fprintf(t.buf, "    offset += %d\n", t.typeSize(v.Type))

		default:
			// If it's an enum or a flag, it's always going to be a uint
			s := generatedTypes[v.Type]
			if s.Type == "enum" {
				eType := "uint" + strconv.Itoa(s.Size*8)
				t.marshalSize(fName, eType, this, littleE)
				fmt.Fprintf(t.buf, "    offset += %d\n", t.typeSize(eType))
				break
			}

			fmt.Fprintf(t.buf, "    slice.set(new Uint8Array(%s%s.marshal()), offset)\n", thisPrefix, fName)
			if s.Size == sizeMessageSize {
				fmt.Fprintf(t.buf, "    offset += %s%s.messageSize\n", thisPrefix, fName)

			} else {
				fmt.Fprintf(t.buf, "    offset += %s.messageSize\n", t.parseType(v.Type))
			}
		}
	}
}

func (ts *TypeScript) marshalSize(name, t string, this bool, littleEndian bool) {
	thisPrefix := ""
	if this {
		thisPrefix = "this."
	}

	switch t {
	case "uint8":
		fmt.Fprintf(ts.buf, "    view.setUint8(offset, %s%s)\n", thisPrefix, name)

	case "uint16":
		fmt.Fprintf(ts.buf, "    view.setUint16(offset, %s%s, %t)\n", thisPrefix, name, littleEndian)

	case "uint32":
		fmt.Fprintf(ts.buf, "    view.setUint32(offset, %s%s, %t)\n", thisPrefix, name, littleEndian)

	case "uint64":
		fmt.Fprintf(ts.buf, "    view.setBigUint64(offset, %s%s, %t)\n", thisPrefix, name, littleEndian)

	case "int16":
		fmt.Fprintf(ts.buf, "    view.setInt16(offset, %s%s, %t)\n", thisPrefix, name, littleEndian)

	case "int32", "unix_fd":
		fmt.Fprintf(ts.buf, "    view.setInt32(offset, %s%s, %t)\n", thisPrefix, name, littleEndian)

	case "int64":
		fmt.Fprintf(ts.buf, "    view.setBigInt64(offset, %s%s, %t)\n", thisPrefix, name, littleEndian)

	default:
		panic("UNKNOWN MARSHAL SIZE " + t)
	}
}

func (t TypeScript) imageSize(this bool, height, width string, size int) string {
	height = t.fieldName(height)
	width = t.fieldName(width)

	if this {
		height = "this." + height
		width = "this." + width
	}

	return fmt.Sprintf("%s * Math.ceil(%s * %d / 8)", height, width, size)
}

func (t TypeScript) field(f *parser.Field) {
	switch f.Type() {
	case "variable":
		t.variable(f.Variable, false)

	case "struct":
		t.variable(&parser.Variable{
			Type:       t.structName(f.Struct.Name),
			Name:       f.StructName,
			Attributes: f.Struct.Attributes,
		}, false)

	case "switch":
		t.sswitch(f.Switch)

	default:
		panic("UNSUPPORTED FIELD TYPE " + f.Type())
	}
}

func (t TypeScript) fieldSize(f *parser.Field) int {
	switch f.Type() {
	case "variable":
		return t.variableSize(f.Variable)

	case "struct":
		return t.structSize(f.Struct)

	case "switch":
		return sizeMessageSize

	default:
		panic("UNSUPPORTED FIELD TYPE SIZE " + f.Type())
	}
}

func (t TypeScript) fieldName(f string) string {
	return strcase.ToLowerCamel(f)
}

func (t *TypeScript) variable(v *parser.Variable, optional bool) {
	if v.Pointer {
		optional = true
	}

	for _, a := range v.Attributes {
		switch a.Name {
		case "as_ptr", "chunk":
			if v.Type != "uint8" && v.Type != "ArrayBuffer" {
				panic("UNSUPPORTED AS_PTR TYPE: " + v.Type)
			}

			optional = true
			v.Type = "ArrayBuffer"
			v.Size = nil
		}
	}

	fmt.Fprintf(t.buf, "  %s: %s\n", t.fieldName(v.Name), t.variableType(v, optional))
}

func (t TypeScript) variableSize(v *parser.Variable) int {
	if v.Pointer {
		return t.typeSize("uint32")
	}

	for _, a := range v.Attributes {
		switch a.Name {
		// TODO: This is not true, chunk is not 0
		case "as_ptr", "chunk":
			return sizeByteLength
		}
	}

	s := t.typeSize(v.Type)
	if v.Size != nil {
		if v.Size.Int != nil {
			return s * *v.Size.Int
		}

		if v.Size.Identifier != "" {
			return sizeArrayLength
		}

		// TODO: IMAGE
		if v.Size.ImageSize != nil {
			return sizeImageSize
		}

		return sizeInfinite
	}

	return s
}

func (ts TypeScript) variableType(v *parser.Variable, nullable bool) string {
	t := ""
	if v.Size != nil {
		t += fmt.Sprintf("%s[", ts.parseType(v.Type))
		// TODO: ALL THIS!!!
		// fmt.Fprintf("Tuple<%s, ", t.parseType(v.Type))

		// if v.Size.Int != nil {
		// 	fmt.Fprint(*v.Size.Int)
		// } else {
		// 	fmt.Fprint("???")
		// }
		t += "]"

		// fmt.Fprint(">")

	} else {
		t += ts.parseType(v.Type)
	}

	if nullable {
		t += " | undefined"
	}

	return t
}

func (t TypeScript) sswitch(s *parser.Switch) {
	for _, c := range s.Cases {
		for _, v := range c.Variables {
			t.variable(v, true)
		}
	}

	if s.Default != nil {
		for _, v := range s.Default.Variables {
			t.variable(v, true)
		}
	}
}

func (t *TypeScript) doSwitching(f []*parser.Field, identifier string, this bool, callback func(*parser.SwitchCase), before func(*parser.SwitchCase)) {
	fieldsByName := map[string]*parser.Field{}
	for _, field := range f {
		fieldsByName[t.fieldName(field.Name())] = field

		// CHECK IF HAS SWITCH
		if field.Switch != nil && field.Switch.Name == identifier {
			for _, c := range field.Switch.Cases {
				before(c)
			}

			if field.Switch.Default != nil {
				before(&parser.SwitchCase{Variables: field.Switch.Default.Variables})
			}

			thisStr := "this."
			if !this {
				thisStr = ""
			}

			fmt.Fprintf(t.buf, "    switch (%s%s) {\n", thisStr, field.Switch.Switch)

			tName := t.findFieldType(fieldsByName, field.Switch.Switch)
			for _, c := range field.Switch.Cases {
				// TODO: FALSE!
				fmt.Fprintf(t.buf, "      case %s.%s:\n", tName, t.enumItemName(tName, c.Case))
				callback(c)

				// If the switch is empty, breaktrough
				if len(c.Variables) != 0 {
					fmt.Fprintln(t.buf, "        break")
				}
			}
			if field.Switch.Default != nil {
				fmt.Fprintln(t.buf, "      default:")
				callback(&parser.SwitchCase{Variables: field.Switch.Default.Variables})
				fmt.Fprintln(t.buf, "        break")
			}
			fmt.Fprintln(t.buf, "    }")
		}
	}
}

func (t TypeScript) findFieldType(fieldsByName map[string]*parser.Field, name string) string {
	nameParts := strings.Split(name, ".")
	for i, p := range nameParts {
		field := fieldsByName[p]

		switch field.Type() {
		case "variable":
			return t.parseType(field.Variable.Type)

		case "struct":
			subFieldsByName := map[string]*parser.Field{}
			for _, f := range field.Struct.Fields {
				subFieldsByName[t.fieldName(f.Name())] = f
			}

			return t.findFieldType(subFieldsByName, nameParts[i+1])

		default:
			panic("UNKNOWN FIELD TYPE " + field.Type())
		}
	}

	panic("unreachable code")
}

func (t *TypeScript) Enum(e *parser.Enum) {
	fmt.Fprintf(t.buf, "export enum %s {\n", strcase.ToCamel(e.Name))
	for _, i := range e.Items {
		i.Name = t.enumItemName(e.Name, i.Name)
	}
	t.enumItems(e.Items)
	fmt.Fprintln(t.buf, "}")

	if e.Type != "" {
		generatedTypes[e.Name] = sizeS{
			Size: t.typeSize(e.Type),
			Type: "enum",
		}
	}
}

func (t *TypeScript) enumItems(items []*parser.EnumFlagItem) {
	enumVal := 0
	for _, item := range items {
		iName := strcase.ToCamel(item.Name)
		for _, a := range item.Attributes {
			switch a.Name {
			case "deprecated":
				fmt.Fprintln(t.buf, "  /**")
				fmt.Fprintf(t.buf, "   * @deprecated %s is deprecated and kept for backwards compatibility\n", iName)
				fmt.Fprintln(t.buf, "   */")
			}
		}

		if item.Value != 0 {
			enumVal = item.Value
		}
		val := strconv.Itoa(enumVal)

		enumVal++

		fmt.Fprintf(t.buf, "  %s = %s,\n", iName, val)
	}
}

func (t TypeScript) enumItemName(enum, item string) string {
	return enum + strcase.ToCamel(strings.ToLower(item))
}

func (t TypeScript) Flags(f *parser.Flags) {
	fmt.Fprintf(t.buf, "export enum %s {\n", strcase.ToCamel(f.Name))
	fmt.Fprintln(t.buf, "  Empty,")
	for _, i := range f.Items {
		i.Name = t.enumItemName(f.Name, i.Name)
	}
	t.flagItems(f.Items)
	fmt.Fprintln(t.buf, "}")

	if f.Type != "" {
		generatedTypes[f.Name] = sizeS{
			Size: t.typeSize(f.Type),
			Type: "enum",
		}
	}
}

func (t *TypeScript) flagItems(items []*parser.EnumFlagItem) {
	flagVal := 0
	for _, item := range items {
		iName := strcase.ToCamel(item.Name)
		for _, a := range item.Attributes {
			switch a.Name {
			case "deprecated":
				fmt.Fprintln(t.buf, "  /**")
				fmt.Fprintf(t.buf, "   * @deprecated %s is deprecated and kept for backwards compatibility\n", iName)
				fmt.Fprintln(t.buf, "   */")
			}
		}

		if item.Value != 0 {
			flagVal = item.Value
		}
		val := fmt.Sprintf("1 << %d", flagVal)

		flagVal++

		fmt.Fprintf(t.buf, "  %s = %s,\n", iName, val)
	}
}

func (t TypeScript) Message(m *parser.Message) {
	t.Struct(&parser.Struct{
		Name:       m.Name,
		Fields:     m.Fields,
		Attributes: m.Attributes,
	})
}

func (t TypeScript) Channel(c *parser.Channel) {
	cName := strings.TrimSuffix(strcase.ToCamel(c.Name), "Channel")
	if cName == "Base" {
		cName = ""
	}

	for _, s := range c.Sides {
		var sName string
		switch s.Side {
		case "server":
			sName = "Msg" + strcase.ToCamel(cName)
		case "client":
			sName = "Msgc" + strcase.ToCamel(cName)
		}

		declareVal := 1
		items := []*parser.EnumFlagItem{}
		for _, m := range s.Messages {
			mName := sName + strcase.ToCamel(m.Name)
			t.Message(&parser.Message{
				Name:       mName,
				Attributes: m.Attributes,
				Fields:     m.Fields,
			})

			if m.Value != 0 {
				declareVal = m.Value
			}

			items = append(items, &parser.EnumFlagItem{
				Name:  m.Name,
				Value: declareVal,
			})

			declareVal++
		}

		t.Enum(&parser.Enum{
			Name:  sName,
			Items: items,
		})
	}
}

func (t TypeScript) Protocol(p *parser.Protocol) {
	i := []*parser.EnumFlagItem{}
	for _, c := range p.Channels {
		i = append(i, &parser.EnumFlagItem{
			Name:  strcase.ToCamel(c.Name),
			Value: c.ID,
		})
	}

	t.Enum(&parser.Enum{
		Name:  strcase.ToCamel(p.Name) + "Channel",
		Items: i,
	})
}

func (t TypeScript) Definition(d *parser.Definition) {
	fmt.Fprintf(t.buf, "export const %s = %d\n", strcase.ToCamel(strcase.ToSnake(d.Name)), d.Value)

	generatedTypes[d.Name] = sizeS{
		Size: t.typeSize(d.Type),
		Type: d.Type,
	}
}
