package generator

import (
	"bytes"
	"fmt"
	"strconv"
	"strings"

	"github.com/dave/jennifer/jen"
	"github.com/iancoleman/strcase"
	"gitlab.com/nefix/spice-codegen/parser"
)

var toGenerate []*jen.Statement

type Go struct {
	f *jen.File
}

func NewGo() *Go {
	return &Go{
		f: jen.NewFile("spice"),
	}
}

func (g Go) Directory() string {
	return "go"
}

func (g Go) File() string {
	return "spice.go"
}

func (g *Go) Render() []byte {
	for _, s := range toGenerate {
		g.f.Add(s)
	}

	buf := &bytes.Buffer{}
	if err := g.f.Render(buf); err != nil {
		panic("render " + err.Error())
	}

	return buf.Bytes()
}

func (g Go) funcName(name string, exclude []string) string {
	parts := strings.Split(strcase.ToSnake(name), "_")

	fName := ""
	currPart := -1
	found := false
	for !found {
		currPart++
		if currPart == len(parts) {
			panic(fmt.Sprintf("function name: name: %s, exclude: %v", name, exclude))
		}

		s := string(parts[currPart][0])
		if currPart != 0 {
			s = strings.ToUpper(s)
		}
		fName += s

		for _, e := range exclude {
			if fName != e {
				found = true
				break
			}
		}
	}

	return fName
}

func (g Go) Type(t *parser.Type) *jen.Statement {
	return jen.Type().Id(strcase.ToCamel(t.Name)).Op(t.Type)
}

func (g Go) Struct(s *parser.Struct) *jen.Statement {
	fields := []jen.Code{}
	for _, f := range s.Fields {
		fields = append(fields, g.field(f))
	}

	return jen.Type().Id(strcase.ToCamel(s.Name)).Struct(fields...)
}

func (g Go) field(f *parser.Field) *jen.Statement {
	switch f.Type() {
	case "variable":
		return g.variable(f.Variable)

	case "switch":

	case "struct":
		toGenerate = append(toGenerate, g.Struct(f.Struct))

		return g.variable(&parser.Variable{Name: f.StructName, Type: f.Struct.Name})
	default:
		panic("unsupported field type: " + f.Type())
	}

	return nil
}

func (g Go) variable(v *parser.Variable) *jen.Statement {
	j := jen.Id(strcase.ToCamel(v.Name))

	if v.Size != nil {
		j.Op("[]")
	}

	return j.Op(g.parseType(v.Type))
}

func (g Go) parseType(t string) string {
	switch t {
	case "int8", "uint8", "int16", "uint16", "int32", "uint32", "int64", "uint64":
		return t

	case "unix_fd":
		return "uintptr"

	default:
		return "*" + strcase.ToCamel(t)
	}
}

func (g Go) Enum(e *parser.Enum) []*jen.Statement {
	t := g.Type(&parser.Type{
		Name:       e.Name,
		Type:       "uint" + strings.TrimPrefix(e.Type, "enum"),
		Attributes: nil,
	})

	consts := []*jen.Statement{}
	defs := []jen.Code{}
	for i, item := range e.Items {
		iName := strcase.ToCamel(e.Name) + strcase.ToCamel(strings.ToLower(item.Name))
		for _, a := range item.Attributes {
			if a.Name == "deprecated" {
				defs = append(defs,
					jen.Comment(fmt.Sprintf("%s is deprecated", iName)),
					jen.Comment(""),
					jen.Comment(fmt.Sprintf("Deprecated: %s is deprecated and keept for back compatibility", iName)),
				)
			}
		}

		val := jen.Id("iota")

		if item.Value != 0 {
			if i != 0 {
				i = 0
				consts = append(consts, jen.Const())
				consts[len(consts)-1].Defs(defs...)
				defs = []jen.Code{}

			}

			val.Op("+").Id(strconv.Itoa(item.Value))
		}

		n := jen.Id(iName)
		if i == 0 {
			n.Id(strcase.ToCamel(e.Name)).Op("=").Add(val)
		}

		defs = append(defs, n)
	}
	consts = append(consts, jen.Const().Defs(defs...))

	return append([]*jen.Statement{t}, consts...)
}

func (g Go) Flags(f *parser.Flags) []*jen.Statement {
	t := g.Type(&parser.Type{
		Name:       f.Name,
		Type:       "uint" + strings.TrimPrefix(f.Type, "flags"),
		Attributes: nil,
	})

	consts := []*jen.Statement{}
	defs := []jen.Code{}
	for i, item := range f.Items {
		val := jen.Id("iota")

		if item.Value != 0 {
			if i != 0 {
				i = 0
				consts = append(consts, jen.Const())
				consts[len(consts)-1].Defs(defs...)
				defs = []jen.Code{}

			}

			val.Op("+").Id(strconv.Itoa(item.Value))
		}

		n := jen.Id(strcase.ToCamel(f.Name) + strcase.ToCamel(strings.ToLower(item.Name)))
		if i == 0 {
			n.Id(strcase.ToCamel(f.Name)).Op("=").Id("1").Op("<<").Add(val)
		}

		defs = append(defs, n)
	}
	consts = append(consts, jen.Const().Defs(defs...))

	return append([]*jen.Statement{t}, consts...)
}

func (g Go) Message(m *parser.Message) []*jen.Statement {
	j := []*jen.Statement{g.Struct(&parser.Struct{
		Name:       m.Name,
		Fields:     m.Fields,
		Attributes: m.Attributes,
	})}

	if len(m.Fields) != 0 {
		mName := strcase.ToCamel(m.Name)
		smallName := g.funcName(m.Name, []string{"r"})

		j = append(j, &jen.Statement{jen.Func().
			Id("Unmarshal"+mName).
			Params(jen.Id("r").Qual("io", "Reader")).
			Params(jen.Op("*").Id(mName), jen.Error()).
			BlockFunc(func(g *jen.Group) {
				g.Id(smallName).Op(":=").Op("&").Id(m.Name).Values()

				for _, f := range m.Fields {
					// TODO: Switch
					if f.Type() != "switch" {
						g.If(jen.Id("err").Op(":=").Qual("encoding/binary", "Read").Call(jen.Id("r"), jen.Qual("encoding/binary", "LittleEndian"), jen.Op("&").Id(smallName+"."+f.Name())).Op(";").Id("err").Op("!=").Nil().Block(
							jen.Return(jen.Nil(), jen.Qual("fmt", "Errorf").Call(jen.Lit("unmarshal "+f.Name()+": %w"), jen.Id("err"))),
						))
					}
				}

				g.Return(jen.Id(smallName), jen.Nil())
			}),
		})

		j = append(j, &jen.Statement{jen.Func().
			Params(jen.Id(smallName).Op("*").Id(m.Name)).
			Id("Marshal").
			Params().
			Params(jen.Index().Byte(), jen.Error()).
			BlockFunc(func(g *jen.Group) {
				g.Id("buf").Op(":=").Qual("bytes", "NewBuffer").Params(jen.Nil())

				for _, f := range m.Fields {
					// TODO: Switch
					if f.Type() != "switch" {
						g.If(jen.Id("err").Op(":=").Qual("encoding/binary", "Write").Call(jen.Id("buf"), jen.Qual("encoding/binary", "LittleEndian"), jen.Op("&").Id(smallName+"."+f.Name())).Op(";").Id("err").Op("!=").Nil().Block(
							jen.Return(jen.Nil(), jen.Qual("fmt", "Errorf").Call(jen.Lit("marshal "+f.Name()+": %w"), jen.Id("err"))),
						))
					}
				}

				g.Return(jen.Id("buf.Bytes").Call(), jen.Nil())
			}),
		})

	}

	return j
}

func (g Go) Channel(c *parser.Channel) []*jen.Statement {
	j := []*jen.Statement{}

	cName := strings.TrimSuffix(strcase.ToCamel(c.Name), "Channel")
	if cName == "BaseChannel" {
		cName = ""
	}

	for _, s := range c.Sides {
		switch s.Side {
		case "server":
			for _, m := range s.Messages {
				j = append(j, g.Message(&parser.Message{
					Name:       "Msg" + cName + strcase.ToCamel(m.Name),
					Attributes: m.Attributes,
					Fields:     m.Fields,
				})...)
			}
		}
	}

	return j
}
