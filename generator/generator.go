package generator

import (
	"gitlab.com/nefix/spice-codegen/parser"
)

type Generator interface {
	Type(*parser.Type)
	Struct(*parser.Struct)
	Enum(*parser.Enum)
	Flags(*parser.Flags)
	Message(*parser.Message)
	Channel(*parser.Channel)
	Protocol(*parser.Protocol)
	Definition(*parser.Definition)
	Render() []byte
	Directory() string
	File() string
}

func Generate(g Generator, s *parser.Spice) []byte {
	for _, e := range s.Entries {
		switch entryType(e) {
		case "type":
			g.Type(e.Type)

		case "struct":
			g.Struct(e.Struct)

		case "enum":
			g.Enum(e.Enum)

		case "flags":
			g.Flags(e.Flags)

		case "message":
			g.Message(e.Message)

		case "channel":
			g.Channel(e.Channel)

		case "protocol":
			g.Protocol(e.Protocol)

		case "definition":
			g.Definition(e.Definition)

		default:
			panic("unsupported type " + entryType(e))
		}
	}

	return g.Render()
}

func entryType(e *parser.Entry) string {
	if e.Enum != nil {
		return "enum"
	}

	if e.Flags != nil {
		return "flags"
	}

	if e.Struct != nil {
		return "struct"
	}

	if e.Channel != nil {
		return "channel"
	}

	if e.Protocol != nil {
		return "protocol"
	}

	if e.Message != nil {
		return "message"
	}

	if e.Type != nil {
		return "type"
	}

	if e.Variable != nil {
		return "variable"
	}

	if e.Definition != nil {
		return "definition"
	}

	return "unknown"
}
