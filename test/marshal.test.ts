import { expect, test } from 'vitest'
import { BinaryData, Clip, ClipRects, ClipType, Copy, DisplayBase, Image, ImageDescriptor, ImageFlags, ImageScaleMode, ImageType, MaskFlags, MsgDisplayDrawComposite, MsgDisplayDrawCopy, MsgMain, MsgMainInit, MsgPing, Opaque, Pattern, Point, QMask, Rect, Ropd } from "../gen/ts/spice"

function testMsg(msg: any, expectedMsg: Buffer, marshal?: boolean) {
    // Unmarshal
    const unmarshalBuf = new ArrayBuffer(expectedMsg.byteLength)
    const view = new DataView(unmarshalBuf)
    let offset = 0
    expectedMsg.forEach((i) => {
        view.setUint8(offset, i)
        offset += 1
    })

    expect(msg).toStrictEqual(msg.constructor['unmarshal'](unmarshalBuf))

    // Marshal
    if (marshal) {
        const buf = Buffer.from(msg.marshal())
        expect(expectedMsg).toStrictEqual(buf)
    }
}

async function readBin(path: string): Promise<Buffer> {
    const f = await Bun.file(path).text() as String
    const buf: number[] = []
    f.split(",").forEach((c) => {
        buf.push(parseInt(c, 10))
    })

    return Buffer.from(buf)
}

test('MsgMainInit', () => {
    const msg = new MsgMainInit(2055277774, 1, 3, 2, 0, 10, 45817561, 50323456)
    const expectedMsg = Buffer.from([206, 12, 129, 122, 1, 0, 0, 0, 3, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 10, 0, 0, 0, 217, 30, 187, 2, 0, 224, 255, 2])

    testMsg(msg, expectedMsg)
})

test('MsgPing', () => {
    const expectedMsg = Buffer.from([1, 0, 0, 0, 147, 165, 49, 239, 230, 0, 0, 0])
    const msg = new MsgPing(1, 991855486355n, new Uint8Array(expectedMsg).buffer)

    testMsg(msg, expectedMsg)
})

test('Rect', () => {
    const msg = new Rect(1, 2, 3, 4)
    const expectedMsg = Buffer.from([1, 0, 0, 0, 2, 0, 0, 0, 3, 0, 0, 0, 4, 0, 0, 0])

    testMsg(msg, expectedMsg)
})

test('ClipRects', () => {
    const msg = new ClipRects(2, [new Rect(1, 2, 3, 4), new Rect(5, 6, 7, 8)]);
    const expectedMsg = Buffer.from([2, 0, 0, 0,
        1, 0, 0, 0, 2, 0, 0, 0, 3, 0, 0, 0, 4, 0, 0, 0,
        5, 0, 0, 0, 6, 0, 0, 0, 7, 0, 0, 0, 8, 0, 0, 0
    ])

    testMsg(msg, expectedMsg)
})

test('DisplayBase', async () => {
    const expectedMsg = (await readBin("test/bin/msg_display_draw_copy_quic.bin")).slice(0, 21)
    const msg = new DisplayBase(
        0,
        new Rect(0, 0, 1094, 800),
        new Clip(
            ClipType.ClipTypeNone,
            undefined
        )
    )

    testMsg(msg, expectedMsg)
})

test('ImageDescriptor', () => {
    const msg = new ImageDescriptor(212n, ImageType.ImageTypeQuic, ImageFlags.Empty, 800, 1094)
    const expectedMsg = Buffer.from([212, 0, 0, 0, 0, 0, 0, 0, 1, 0, 32, 3, 0, 0, 70, 4, 0, 0])

    testMsg(msg, expectedMsg)
})

test('MsgDisplayDrawCopyQuic', async () => {
    const expectedMsg = await readBin("test/bin/msg_display_draw_copy_quic.bin")
    const msg = new MsgDisplayDrawCopy(
        new DisplayBase(
            0,
            new Rect(0, 0, 1094, 800),
            new Clip(
                ClipType.ClipTypeNone,
                undefined
            )
        ),
        new Copy(
            new Image(
                new ImageDescriptor(
                    77309411330n,
                    ImageType.ImageTypeQuic,
                    ImageFlags.Empty,
                    800,
                    1094
                ),
                undefined,
                new BinaryData(
                    expectedMsg.buffer.slice(57 + 18 + 4).byteLength, // We know it starts at 57, +18 is for the descriptor and +4 is for the dataSize
                    expectedMsg.buffer.slice(57 + 18 + 4)
                ),
                undefined, undefined, undefined, undefined, undefined, undefined, undefined
            ),
            new Rect(0, 0, 1094, 800),
            Ropd.RopdOpPut,
            ImageScaleMode.ImageScaleModeInterpolate,
            new QMask(
                MaskFlags.Empty,
                new Point(0, 0),
                undefined,
            )
        )
    )

    testMsg(msg, expectedMsg, false)
})

test('MsgDisplayDrawCopyLz', async () => {
    const expectedMsg = await readBin("test/bin/msg_display_draw_copy_lz.bin")
    const msg = new MsgDisplayDrawCopy(
        new DisplayBase(
            0,
            new Rect(0, 0, 1088, 1024),
            new Clip(
                ClipType.ClipTypeNone,
                undefined
            )
        ),
        new Copy(
            new Image(
                new ImageDescriptor(
                    38654705666n,
                    ImageType.ImageTypeLzRgb,
                    ImageFlags.Empty,
                    1024,
                    1088
                ),
                undefined,
                undefined,
                new BinaryData(
                    expectedMsg.buffer.slice(57 + 18 + 4).byteLength, // We know it starts at 57, +18 is for the descriptor and +4 is for the dataSize
                    expectedMsg.buffer.slice(57 + 18 + 4)
                ),
                undefined, undefined, undefined, undefined, undefined, undefined
            ),
            new Rect(0, 0, 1088, 1024),
            Ropd.RopdOpPut,
            ImageScaleMode.ImageScaleModeInterpolate,
            new QMask(
                MaskFlags.Empty,
                new Point(0, 0),
                undefined,
            )
        )
    )

    testMsg(msg, expectedMsg, false)
})
