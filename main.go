//go:generate go run main.go

package main

import (
	"io"
	"log"
	"os"
	"path/filepath"

	"gitlab.com/nefix/spice-codegen/generator"
	"gitlab.com/nefix/spice-codegen/parser"
)

func main() {
	protos := []string{
		"proto/spice.proto",
		"proto/protocol.proto",
		"proto/quic.proto",
		"proto/lz.proto",
	}
	readers := []io.Reader{}
	for _, p := range protos {
		f, err := os.Open(p)
		if err != nil {
			log.Fatalf("open file %s: %v", p, err)
		}
		defer f.Close()

		readers = append(readers, f)
	}
	r := io.MultiReader(readers...)

	s, err := parser.Parse(r)
	if err != nil {
		log.Fatal(err)
	}

	generators := []generator.Generator{
		// generator.NewGo(),
		generator.NewTypescript(),
	}

	for _, g := range generators {
		b := generator.Generate(g, s)

		dir := filepath.Join("gen", g.Directory())

		if err := os.MkdirAll(dir, 0755); err != nil {
			log.Fatalf("create directory: %v", err)
		}

		if err := os.WriteFile(filepath.Join(dir, g.File()), b, 0644); err != nil {
			log.Fatalf("write file: %v", err)
		}
	}
}
